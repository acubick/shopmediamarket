var Message = /** @class */ (function () {
    function Message(type, text) {
        this.type = type;
        this.text = text;
    }
    return Message;
}());
export { Message };
//# sourceMappingURL=message.model.js.map