var Product = /** @class */ (function () {
    function Product(name, category, cost, quantity, sold, model, image, id) {
        this.name = name;
        this.category = category;
        this.cost = cost;
        this.quantity = quantity;
        this.sold = sold;
        this.model = model;
        this.image = image;
        this.id = id;
    }
    return Product;
}());
export { Product };
//# sourceMappingURL=product.model.js.map