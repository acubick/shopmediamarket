var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BaseApi } from '../core/base-api';
var UserService = /** @class */ (function (_super) {
    __extends(UserService, _super);
    function UserService(httpClient) {
        var _this = _super.call(this, httpClient) || this;
        _this.httpClient = httpClient;
        return _this;
    }
    // public getUserByEmail(email: string): Observable<User> {
    //   return this.httpClient.get<User[]>(`http://localhost:3000/users?email=${email}`)
    //     .pipe(map((user: User[]) => user[0] ? user[0] : undefined));
    // }
    UserService.prototype.getUserByEmail = function (email) {
        return this.get("users?email=" + email)
            .pipe(map(function (user) { return user[0] ? user[0] : undefined; }));
    };
    // public createNewUser(user: User): Observable<User> {
    //   return this.httpClient.post<User>(`http://localhost:3000/users`, user);
    //
    // }
    UserService.prototype.createNewUser = function (user) {
        return this.post("users", user);
    };
    UserService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient])
    ], UserService);
    return UserService;
}(BaseApi));
export { UserService };
//# sourceMappingURL=user.service.js.map