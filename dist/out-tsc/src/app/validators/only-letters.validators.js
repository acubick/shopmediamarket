export function onlyLetters(control) {
    var field = control.value;
    var onlyLetterRegexp = '^[a-zA-Z]+$';
    var regexp = new RegExp(onlyLetterRegexp);
    // if (regexp.test(field)) {
    //   return null;
    // } else {
    //   return {onlyLetters: true};
    // }
    return regexp.test(field) ? null : { onlyLetters: true };
}
//# sourceMappingURL=only-letters.validators.js.map