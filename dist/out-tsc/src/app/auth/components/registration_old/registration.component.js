var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MIN_LENGTH_PASSWORD } from '../../../shared/constants';
import { User } from '../../../shared/models/user.model';
import { UserService } from '../../../shared/services/user.service';
var RegistrationComponent = /** @class */ (function () {
    function RegistrationComponent(userService, router) {
        this.userService = userService;
        this.router = router;
    }
    RegistrationComponent.prototype.ngOnInit = function () {
        this.form = new FormGroup({
            email: new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmails.bind(this)),
            password: new FormControl(null, [Validators.required, Validators.minLength(MIN_LENGTH_PASSWORD)]),
            name: new FormControl(null, [Validators.required]),
            agree: new FormControl(false, [Validators.requiredTrue]),
        });
    };
    RegistrationComponent.prototype.onSubmit = function () {
        var _this = this;
        var _a = this.form.value, email = _a.email, password = _a.password, name = _a.name;
        var user = new User(email, password, name);
        // console.log(this.form);
        this.userService.createNewUser(user)
            .subscribe(function () {
            // console.log(user);
            _this.router.navigate(['login'], {
                queryParams: {
                    nowCanLogin: true,
                },
            });
        });
    };
    RegistrationComponent.prototype.forbiddenEmails = function (control) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.userService.getUserByEmail(control.value)
                .subscribe(function (user) {
                if (user) {
                    resolve({ forbiddenEmail: true });
                }
                else {
                    resolve(null);
                }
            });
        });
    };
    RegistrationComponent = __decorate([
        Component({
            selector: 'app-registration',
            templateUrl: './registration.component.html',
            styleUrls: ['./registration.component.scss'],
        }),
        __metadata("design:paramtypes", [UserService,
            Router])
    ], RegistrationComponent);
    return RegistrationComponent;
}());
export { RegistrationComponent };
//# sourceMappingURL=registration.component.js.map