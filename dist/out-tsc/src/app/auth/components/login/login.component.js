var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MIN_LENGTH_PASSWORD } from '../../../shared/constants';
import { Message } from '../../../shared/models/message.model';
import { UserService } from '../../../shared/services/user.service';
import { AuthService } from '../../../core/services/auth.service';
var LoginComponent = /** @class */ (function () {
    function LoginComponent(userService, authServise, router, route) {
        this.userService = userService;
        this.authServise = authServise;
        this.router = router;
        this.route = route;
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.message = new Message('danger', ''),
            this.route.queryParams
                .subscribe(function (params) {
                console.log('few', params);
                if (params.nowCanLogin) {
                    _this.showMessage({
                        text: 'Теперь вы можуте войти в систему',
                        type: 'success',
                    });
                }
            }),
            this.form = new FormGroup({
                email: new FormControl(null, [Validators.required, Validators.email]),
                password: new FormControl(null, [Validators.required, Validators.minLength(MIN_LENGTH_PASSWORD)]),
            });
    };
    LoginComponent.prototype.onSubmit = function () {
        var _this = this;
        // console.log('this form value:', this.form.value)
        var formData = this.form.value;
        this.userService.getUserByEmail(formData.email)
            .subscribe(function (user) {
            if (user) {
                if (user.password === formData.password) {
                    _this.message.text = '';
                    window.localStorage.setItem('user', JSON.stringify(user));
                    _this.authServise.login();
                    // this.router.navigate(['']);
                    console.log('вы залогинились');
                }
                else {
                    _this.showMessage({
                        text: 'Пароль неверный',
                        type: 'danger',
                    });
                }
            }
            else {
                _this.showMessage({
                    text: 'Такого пользователя не существует',
                    type: 'danger',
                });
            }
        });
    };
    LoginComponent.prototype.showMessage = function (message) {
        var _this = this;
        // console.log(this, type, text);
        this.message = message;
        window.setTimeout(function () {
            _this.message.text = '';
        }, 3000);
    };
    LoginComponent = __decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.scss'],
        }),
        __metadata("design:paramtypes", [UserService,
            AuthService,
            Router,
            ActivatedRoute])
    ], LoginComponent);
    return LoginComponent;
}());
export { LoginComponent };
//# sourceMappingURL=login.component.js.map