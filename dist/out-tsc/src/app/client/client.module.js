var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppSharedModule } from '../shared/app-shared.module';
import { ClientComponent } from './client.component';
import { ClientRoutingModule } from './client.routing';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
var ClientModule = /** @class */ (function () {
    function ClientModule() {
    }
    ClientModule = __decorate([
        NgModule({
            imports: [CommonModule, ClientRoutingModule, AppSharedModule],
            declarations: [ClientComponent, HeaderComponent, SidebarComponent],
            providers: [],
        })
    ], ClientModule);
    return ClientModule;
}());
export { ClientModule };
//# sourceMappingURL=client.module.js.map