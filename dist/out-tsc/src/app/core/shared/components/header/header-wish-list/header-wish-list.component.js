var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
var HeaderWishListComponent = /** @class */ (function () {
    function HeaderWishListComponent() {
    }
    HeaderWishListComponent.prototype.ngOnInit = function () {
    };
    HeaderWishListComponent = __decorate([
        Component({
            selector: 'app-header-wish-list',
            templateUrl: './header-wish-list.component.html',
            styleUrls: ['./header-wish-list.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], HeaderWishListComponent);
    return HeaderWishListComponent;
}());
export { HeaderWishListComponent };
//# sourceMappingURL=header-wish-list.component.js.map