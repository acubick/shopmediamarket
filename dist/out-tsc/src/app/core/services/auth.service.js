var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
var AuthService = /** @class */ (function () {
    function AuthService() {
    }
    AuthService.prototype.isLoggedIn = function () {
        return this.isAuthentificated;
    };
    AuthService.prototype.login = function () {
        this.isAuthentificated = true;
    };
    AuthService.prototype.logout = function () {
        this.isAuthentificated = false;
        window.localStorage.clear();
    };
    AuthService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], AuthService);
    return AuthService;
}());
export { AuthService };
//# sourceMappingURL=auth.service.js.map