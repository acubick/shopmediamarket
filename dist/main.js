(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"app/auth/auth.module": [
		"./src/app/auth/auth.module.ts"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/* tslint:disable */
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
        var backEndUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].backEndUrl;
        // tslint:disable-next-line
        console.log(backEndUrl);
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")],
        }),
        __metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./core/core.module */ "./src/app/core/core.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





/* tslint:disable */
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _core_core_module__WEBPACK_IMPORTED_MODULE_4__["CoreModule"],
                _app_routing__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]],
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _client_client_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./client/client.component */ "./src/app/client/client.component.ts");
/* harmony import */ var _core_components_home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./core/components/home/home.component */ "./src/app/core/components/home/home.component.ts");
/* harmony import */ var _shared_components_not_found_page_not_found_page_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shared/components/not-found-page/not-found-page.component */ "./src/app/shared/components/not-found-page/not-found-page.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





/* tslint:disable */
var routes = [
    { path: '', component: _core_components_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
    { path: 'client', component: _client_client_component__WEBPACK_IMPORTED_MODULE_2__["ClientComponent"] },
    { path: 'auth', loadChildren: 'app/auth/auth.module#AuthModule' },
    { path: 'not-found-page', component: _shared_components_not_found_page_not_found_page_component__WEBPACK_IMPORTED_MODULE_4__["NotFoundPageComponent"] },
    { path: '**', redirectTo: '/not-found-page' },
];
// const routes: Routes = [
//
//   { path: 'auth', loadChildren: 'app/auth/auth.module#AuthModule' },
//   // {path: 'login', redirectTo: 'auth/login', pathMatch: 'full'},
//   {path: 'administrator', loadChildren: 'app/administrator/administrator.module#AdministratorModule'},
//   {path: 'client/:id', loadChildren: 'app/client/client.module#ClientModule'},
//   { path: '',   redirectTo: '', pathMatch: 'full' },
//   {path: 'not-found-page', component: NotFoundPageComponent},
//   {path: '**', redirectTo: '/not-found-page'},
// ];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/auth/auth.component.html":
/*!******************************************!*\
  !*** ./src/app/auth/auth.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"auth\" >\r\n  <div class=\"auth-container\">\r\n    <div class=\"card\">\r\n      <header class=\"auth-header\">\r\n        <h1 class=\"auth-title\">\r\n          <div class=\"logo\">\r\n           <!--// TODO: Тут сделать логотип-->\r\n          </div>\r\n          Shop Media Market\r\n        </h1>\r\n      </header>\r\n      <div class=\"auth-content\">\r\n         <router-outlet></router-outlet>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/auth/auth.component.scss":
/*!******************************************!*\
  !*** ./src/app/auth/auth.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".auth {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  background-color: #798b9a; }\n\n.auth-container {\n  width: 100%;\n  height: 100%; }\n\n.card {\n  background-color: #ffffffc7;\n  width: 400px;\n  margin: 5% auto; }\n\n.auth-header {\n  border-bottom: 1px solid #798b9a; }\n\n.auth-container .auth-title {\n  color: #7e8794;\n  padding: 20px;\n  line-height: 30px;\n  font-size: 26px;\n  font-weight: 600;\n  margin: 0;\n  text-align: center; }\n\n.auth-container .auth-content {\n  padding: 15px 35px 35px;\n  min-height: 260px; }\n"

/***/ }),

/***/ "./src/app/auth/auth.component.ts":
/*!****************************************!*\
  !*** ./src/app/auth/auth.component.ts ***!
  \****************************************/
/*! exports provided: AuthComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthComponent", function() { return AuthComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/* tslint:disable */
var AuthComponent = /** @class */ (function () {
    function AuthComponent(router) {
        this.router = router;
    }
    AuthComponent.prototype.ngOnInit = function () {
        this.router.navigate(['/login']);
    };
    AuthComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-auth',
            template: __webpack_require__(/*! ./auth.component.html */ "./src/app/auth/auth.component.html"),
            styles: [__webpack_require__(/*! ./auth.component.scss */ "./src/app/auth/auth.component.scss")],
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthComponent);
    return AuthComponent;
}());



/***/ }),

/***/ "./src/app/auth/auth.module.ts":
/*!*************************************!*\
  !*** ./src/app/auth/auth.module.ts ***!
  \*************************************/
/*! exports provided: AuthModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthModule", function() { return AuthModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_app_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/app-shared.module */ "./src/app/shared/app-shared.module.ts");
/* harmony import */ var _auth_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth.component */ "./src/app/auth/auth.component.ts");
/* harmony import */ var _auth_routing__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./auth.routing */ "./src/app/auth/auth.routing.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/auth/components/login/login.component.ts");
/* harmony import */ var _components_registration_registration_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/registration/registration.component */ "./src/app/auth/components/registration/registration.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







/* tslint:disable */
var AuthModule = /** @class */ (function () {
    function AuthModule() {
    }
    AuthModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
                _auth_routing__WEBPACK_IMPORTED_MODULE_4__["AuthRoutingModule"],
                _shared_app_shared_module__WEBPACK_IMPORTED_MODULE_2__["AppSharedModule"]
            ],
            declarations: [_auth_component__WEBPACK_IMPORTED_MODULE_3__["AuthComponent"], _components_login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"], _components_registration_registration_component__WEBPACK_IMPORTED_MODULE_6__["RegistrationComponent"]],
            providers: [],
        })
    ], AuthModule);
    return AuthModule;
}());



/***/ }),

/***/ "./src/app/auth/auth.routing.ts":
/*!**************************************!*\
  !*** ./src/app/auth/auth.routing.ts ***!
  \**************************************/
/*! exports provided: AuthRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthRoutingModule", function() { return AuthRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth.component */ "./src/app/auth/auth.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/auth/components/login/login.component.ts");
/* harmony import */ var _components_registration_registration_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/registration/registration.component */ "./src/app/auth/components/registration/registration.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/* tslint:disable */



var AUTH_ROUTES = [
    // {path: 'login', component: LoginComponent},
    // {path: 'registration', component: RegistrationComponent}
    {
        path: '', component: _auth_component__WEBPACK_IMPORTED_MODULE_2__["AuthComponent"], children: [
            { path: 'login', component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
            { path: 'registration', component: _components_registration_registration_component__WEBPACK_IMPORTED_MODULE_4__["RegistrationComponent"] }
        ]
    }
];
var AuthRoutingModule = /** @class */ (function () {
    function AuthRoutingModule() {
    }
    AuthRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(AUTH_ROUTES)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        })
    ], AuthRoutingModule);
    return AuthRoutingModule;
}());



/***/ }),

/***/ "./src/app/auth/components/login/login.component.html":
/*!************************************************************!*\
  !*** ./src/app/auth/components/login/login.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p class=\"text-xs-center\">Авторизируйтесь для покупки в магазине</p>\r\n<div\r\n  class=\"alert alert-{{ message.type }}\"\r\n  *ngIf=\" message.text\"\r\n>{{ message.text }}</div>\r\n<!-- /.alert -->\r\n<form [formGroup]=\"form\" (ngSubmit)=\"onSubmit()\">\r\n  <div\r\n    class=\"form-group\"\r\n    [ngClass]=\"{'has-error': form.get('email').invalid && form.get('email').touched}\"\r\n  >\r\n    <label for=\"email\">Email</label>\r\n    <input\r\n      type=\"text\"\r\n      class=\"form-control underlined\"\r\n      id=\"email\"\r\n      placeholder=\"Введите ваш email\"\r\n      formControlName=\"email\"\r\n    >\r\n    <span\r\n      class=\"form-help-text\"\r\n      *ngIf=\"form.get('email').invalid && form.get('email').touched\"\r\n    >\r\n      <span *ngIf=\"form.get('email')['errors']['email']\">Введите корректный email. </span>\r\n      <span *ngIf=\"form.get('email')['errors']['required']\">Email не может быть пустым. </span>\r\n    </span>\r\n  </div>\r\n  <div\r\n    class=\"form-group\"\r\n    [ngClass]=\"{'has-error': form.get('password').invalid && form.get('password').touched}\"\r\n  >\r\n    <label for=\"password\">Пароль</label>\r\n    <input\r\n      type=\"password\"\r\n      class=\"form-control underlined\"\r\n      id=\"password\"\r\n      placeholder=\"Пароль\"\r\n      formControlName=\"password\"\r\n    >\r\n    <span\r\n      class=\"form-help-text\"\r\n      *ngIf=\"form.get('password').invalid && form.get('password').touched\"\r\n    >\r\n      <span *ngIf=\"form.get('password')['errors']['required']\">Поле пароля не может быть пустым. </span>\r\n      <span *ngIf=\"form.get('password')['errors']['minlength'] && form.get('password')['errors']['minlength']['requiredLength']\">\r\n        Пароль должен быть бошльше {{ MIN_LENGTH_PASSWORD - 1 }} символов.\r\n        <br>\r\n        Cейчас введено {{ form.get('password')['errors']['minlength']['actualLength'] }}\r\n      </span>\r\n    </span>\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <button\r\n      type=\"submit\"\r\n      class=\"btn btn-block btn-primary\"\r\n      [disabled]=\"form.invalid\"\r\n    >\r\n      Войти\r\n    </button>\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <p class=\"text-muted text-xs-center\">\r\n      Нет аккаунта? <a [routerLink]=\"'/registration'\">Зарегистрироваться!</a>\r\n    </p>\r\n  </div>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/auth/components/login/login.component.scss":
/*!************************************************************!*\
  !*** ./src/app/auth/components/login/login.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".text-xs-center {\n  text-align: center;\n  font-size: 16px;\n  font-family: Poppins;\n  font-weight: 500;\n  color: #000; }\n\n.form-group {\n  margin-bottom: 1rem; }\n\n.form-control.underlined {\n  padding-left: 0;\n  padding-right: 0;\n  border-radius: 0;\n  border: none;\n  box-shadow: none;\n  border-bottom: 1px solid #d7dde4; }\n\n.form-control {\n  display: block;\n  width: 100%;\n  padding: .375rem .75rem;\n  font-size: 1rem;\n  line-height: 1.5;\n  color: #55595c;\n  background-color: #a1a1a1;\n  border: 1px solid #ccc;\n  border-radius: .25rem; }\n\ninput,\ntextarea {\n  outline: 0; }\n"

/***/ }),

/***/ "./src/app/auth/components/login/login.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/auth/components/login/login.component.ts ***!
  \**********************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../shared/constants */ "./src/app/shared/constants.ts");
/* harmony import */ var _shared_models_message_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared/models/message.model */ "./src/app/shared/models/message.model.ts");
/* harmony import */ var _shared_services_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shared/services/user.service */ "./src/app/shared/services/user.service.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/auth/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginComponent = /** @class */ (function () {
    function LoginComponent(userService, authServise, router, route) {
        this.userService = userService;
        this.authServise = authServise;
        this.router = router;
        this.route = route;
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.message = new _shared_models_message_model__WEBPACK_IMPORTED_MODULE_4__["Message"]('danger', ''),
            this.route.queryParams
                .subscribe(function (params) {
                console.log('few', params);
                if (params.nowCanLogin) {
                    _this.showMessage({
                        text: 'Теперь вы можуте войти в систему',
                        type: 'success',
                    });
                }
            }),
            this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
                password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(_shared_constants__WEBPACK_IMPORTED_MODULE_3__["MIN_LENGTH_PASSWORD"])]),
            });
    };
    LoginComponent.prototype.onSubmit = function () {
        var _this = this;
        // console.log('this form value:', this.form.value)
        var formData = this.form.value;
        this.userService.getUserByEmail(formData.email)
            .subscribe(function (user) {
            if (user) {
                if (user.password === formData.password) {
                    _this.message.text = '';
                    window.localStorage.setItem('user', JSON.stringify(user));
                    _this.authServise.login();
                    // this.router.navigate(['']);
                    console.log('вы залогинились');
                }
                else {
                    _this.showMessage({
                        text: 'Пароль неверный',
                        type: 'danger',
                    });
                }
            }
            else {
                _this.showMessage({
                    text: 'Такого пользователя не существует',
                    type: 'danger',
                });
            }
        });
    };
    LoginComponent.prototype.showMessage = function (message) {
        var _this = this;
        // console.log(this, type, text);
        this.message = message;
        window.setTimeout(function () {
            _this.message.text = '';
        }, 3000);
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/auth/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/auth/components/login/login.component.scss")],
        }),
        __metadata("design:paramtypes", [_shared_services_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/auth/components/registration/registration.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/auth/components/registration/registration.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p class=\"text-xs-center\">Регистрация для получения доступа к покупкам</p>\r\n<form [formGroup]=\"form\" (ngSubmit)=\"onSubmit()\">\r\n  <div class=\"form-group\"\r\n       [ngClass]=\"{'has-error': form.get('email').invalid && form.get('email').touched}\"\r\n  >\r\n    <label for=\"email\">Email</label>\r\n    <input\r\n      type=\"text\"\r\n      class=\"form-control underlined\"\r\n      id=\"email\"\r\n      placeholder=\"Введите email\"\r\n      formControlName=\"email\"\r\n    >\r\n    <span\r\n      class=\"form-help-text\"\r\n      *ngIf=\"form.get('email').invalid && form.get('email').touched\"\r\n    >\r\n      <span *ngIf=\"form.get('email')['errors']['email']\">Введите корректный email. </span>\r\n      <span *ngIf=\"form.get('email')['errors']['required']\">Email не может быть пустым. </span>\r\n      <span *ngIf=\"form.get('email')['errors']['forbiddenEmail']\">Email уже занят. </span>\r\n    </span>\r\n  </div>\r\n  <div class=\"form-group\"\r\n       [ngClass]=\"{'has-error': form.get('password').invalid && form.get('password').touched}\"\r\n  >\r\n    <label for=\"password\">Пароль</label>\r\n    <input\r\n      type=\"password\"\r\n      class=\"form-control underlined\"\r\n      id=\"password\"\r\n      placeholder=\"Введите пароль\"\r\n      formControlName=\"password\"\r\n    >\r\n    <span\r\n      class=\"form-help-text\"\r\n      *ngIf=\"form.get('password').invalid && form.get('password').touched\"\r\n    >\r\n      <span *ngIf=\"form.get('password')['errors']['required']\">Поле пароля не может быть пустым. </span>\r\n      <span\r\n        *ngIf=\"form.get('password')['errors']['minlength'] && form.get('password')['errors']['minlength']['requiredLength']\">\r\n        Пароль должен быть бошльше {{ MIN_LENGTH_PASSWORD - 1 }} символов.\r\n        <br>\r\n        Cейчас введено {{ form.get('password')['errors']['minlength']['actualLength'] }}\r\n      </span>\r\n    </span>\r\n  </div>\r\n  <div class=\"form-group\"\r\n       [ngClass]=\"{'has-error': form.get('name').invalid && form.get('name').touched}\"\r\n  >\r\n    <label for=\"name\">Имя</label>\r\n    <input\r\n      type=\"text\"\r\n      class=\"form-control underlined\"\r\n      id=\"name\"\r\n      placeholder=\"Введите имя\"\r\n      formControlName=\"name\"\r\n    >\r\n    <span class=\"form-help-text\"\r\n          *ngIf=\"form.get('name').invalid && form.get('name').touched\"\r\n    >\r\n      Имя не может быть пустым.\r\n    </span>\r\n  </div>\r\n  <div class=\"form-group\"\r\n       [ngClass]=\"{'has-error': form.get('agree').invalid && form.get('agree').touched}\"\r\n  >\r\n    <label for=\"agree\">\r\n      <input\r\n        class=\"checkbox\"\r\n        id=\"agree\"\r\n        type=\"checkbox\"\r\n        formControlName=\"agree\"\r\n      >\r\n      <span>Согласен с правилами</span>\r\n    </label>\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <button type=\"submit\" class=\"btn btn-block btn-primary\"\r\n            [disabled]=\"form.invalid\"\r\n    >\r\n      Зарегистрироваться\r\n    </button>\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <p class=\"text-muted text-xs-center\">\r\n      Уже есть аккаунт?\r\n      <a [routerLink]=\"'/login'\">\r\n        Войти!\r\n      </a>\r\n    </p>\r\n  </div>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/auth/components/registration/registration.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/auth/components/registration/registration.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/auth/components/registration/registration.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/auth/components/registration/registration.component.ts ***!
  \************************************************************************/
/*! exports provided: RegistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationComponent", function() { return RegistrationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../shared/constants */ "./src/app/shared/constants.ts");
/* harmony import */ var _shared_models_user_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared/models/user.model */ "./src/app/shared/models/user.model.ts");
/* harmony import */ var _shared_services_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shared/services/user.service */ "./src/app/shared/services/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RegistrationComponent = /** @class */ (function () {
    function RegistrationComponent(userService, router) {
        this.userService = userService;
        this.router = router;
    }
    RegistrationComponent.prototype.ngOnInit = function () {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email], this.forbiddenEmails.bind(this)),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(_shared_constants__WEBPACK_IMPORTED_MODULE_3__["MIN_LENGTH_PASSWORD"])]),
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            agree: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](false, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].requiredTrue]),
        });
    };
    RegistrationComponent.prototype.onSubmit = function () {
        var _this = this;
        var _a = this.form.value, email = _a.email, password = _a.password, name = _a.name;
        var user = new _shared_models_user_model__WEBPACK_IMPORTED_MODULE_4__["User"](email, password, name);
        // console.log(this.form);
        this.userService.createNewUser(user)
            .subscribe(function () {
            // console.log(user);
            _this.router.navigate(['login'], {
                queryParams: {
                    nowCanLogin: true,
                },
            });
        });
    };
    RegistrationComponent.prototype.forbiddenEmails = function (control) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.userService.getUserByEmail(control.value)
                .subscribe(function (user) {
                if (user) {
                    resolve({ forbiddenEmail: true });
                }
                else {
                    resolve(null);
                }
            });
        });
    };
    RegistrationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-registration',
            template: __webpack_require__(/*! ./registration.component.html */ "./src/app/auth/components/registration/registration.component.html"),
            styles: [__webpack_require__(/*! ./registration.component.scss */ "./src/app/auth/components/registration/registration.component.scss")],
        }),
        __metadata("design:paramtypes", [_shared_services_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], RegistrationComponent);
    return RegistrationComponent;
}());



/***/ }),

/***/ "./src/app/auth/services/auth.service.ts":
/*!***********************************************!*\
  !*** ./src/app/auth/services/auth.service.ts ***!
  \***********************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
var AuthService = /** @class */ (function () {
    function AuthService() {
    }
    AuthService.prototype.isLoggedIn = function () {
        return this.isAuthentificated;
    };
    AuthService.prototype.login = function () {
        this.isAuthentificated = true;
    };
    AuthService.prototype.logout = function () {
        this.isAuthentificated = false;
        window.localStorage.clear();
    };
    return AuthService;
}());



/***/ }),

/***/ "./src/app/client/client.component.html":
/*!**********************************************!*\
  !*** ./src/app/client/client.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-wrapper\">\r\n  <div class=\"app\">\r\n    <!--HEADER-->\r\n<app-header></app-header>\r\n    <!--HEADER-->\r\n    <!--SIDEBAR-->\r\n     <app-sidebar></app-sidebar>\r\n    <!--SIDEBAR-->\r\n    <!--CONTENT-->\r\n    <article class=\"content\">\r\n      <div class=\"title-block\">\r\n        <h3 class=\"title\">\r\n          Страница покупок <span class=\"sparkline bar\"></span>\r\n        </h3>\r\n      </div>\r\n    </article>\r\n    <!--CONTENT-->\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/client/client.component.scss":
/*!**********************************************!*\
  !*** ./src/app/client/client.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n@import url(http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,800,700,600);\n@charset \"UTF-8\";\nbody, html {\n  padding: 0;\n  margin: 0;\n  height: 100%;\n  min-height: 100%;\n  font-family: 'Open Sans',sans-serif;\n  color: #4f5f6f;\n  overflow-x: hidden; }\n.main-wrapper {\n  width: 100%;\n  position: absolute;\n  height: 100%;\n  overflow-y: auto;\n  overflow-x: hidden; }\n#ref .color-primary {\n  color: #52bcd3; }\n#ref .chart .color-primary {\n  color: #52bcd3; }\n#ref .chart .color-secondary {\n  color: #7bccdd; }\n.app {\n  position: relative;\n  width: 100%;\n  padding-left: 230px;\n  min-height: 100vh;\n  margin: 0 auto;\n  left: 0;\n  background-color: #f0f3f6;\n  box-shadow: 0 0 3px #ccc;\n  transition: left .3s ease,padding-left .3s ease;\n  overflow: hidden; }\n.app .content {\n  padding: 110px 30px 90px 30px;\n  min-height: 100vh; }\n@media (min-width: 1200px) {\n  .app .content {\n    padding: 120px 50px 100px 50px; } }\n@media (min-width: 992px) and (max-width: 1199px) {\n  .app .content {\n    padding: 110px 30px 90px 30px; } }\n@media (min-width: 768px) and (max-width: 991px) {\n  .app .content {\n    padding: 95px 20px 75px 20px; } }\n@media (max-width: 767px) {\n  .app .content {\n    padding: 65px 10px 65px 10px; } }\n@media (max-width: 991px) {\n  .app {\n    padding-left: 0; } }\n@media (max-width: 991px) {\n  .app.sidebar-open {\n    left: 0; } }\n.app.blank {\n  background-color: #667380; }\n.auth {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  background-color: #667380;\n  overflow-x: hidden;\n  overflow-y: auto; }\n.auth-container {\n  width: 450px;\n  min-height: 330px;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translateY(-50%) translateX(-50%);\n  transform: translateY(-50%) translateX(-50%); }\n.auth-container .auth-header {\n  text-align: center;\n  border-bottom: 1px solid #52bcd3; }\n.auth-container .auth-title {\n  color: #97a4b1;\n  padding: 20px;\n  line-height: 30px;\n  font-size: 26px;\n  font-weight: 600;\n  margin: 0; }\n.auth-container .auth-content {\n  padding: 30px 50px;\n  min-height: 260px; }\n.auth-container .forgot-btn {\n  line-height: 28px; }\n.auth-container .checkbox label {\n  padding: 0; }\n.auth-container .checkbox a {\n  vertical-align: text-top; }\n.auth-container .checkbox span {\n  color: #4f5f6f; }\n@media (max-width: 767px) {\n  .auth-container {\n    width: 100%;\n    position: relative;\n    left: 0;\n    top: 0;\n    -webkit-transform: inherit;\n    transform: inherit;\n    margin: 0;\n    margin-bottom: 10px; }\n  .auth-container .auth-content {\n    padding: 30px 25px; } }\n.error-card {\n  width: 410px;\n  min-height: 330px;\n  margin: 60px auto; }\n.error-card .error-title {\n  font-size: 150px;\n  line-height: 150px;\n  font-weight: 700;\n  color: #252932;\n  text-align: center;\n  text-shadow: rgba(61, 61, 61, 0.3) 0.5px 0.5px, rgba(61, 61, 61, 0.2) 1px 1px, rgba(61, 61, 61, 0.3) 1.5px 1.5px; }\n.error-card .error-sub-title {\n  font-weight: 100;\n  text-align: center; }\n.error-card .error-container {\n  text-align: center;\n  visibility: hidden; }\n.error-card .error-container.visible {\n  visibility: visible; }\n.error-card.global {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translateY(-50%) translateX(-50%);\n  transform: translateY(-50%) translateX(-50%);\n  margin: 0; }\n.error-card.global .error-title {\n  color: #fff; }\n.error-card.global .error-container, .error-card.global .error-sub-title {\n  color: #fff; }\n@media (min-width: 768px) and (max-width: 991px) {\n  .error-card {\n    width: 50%; }\n  .error-card.global {\n    position: relative;\n    top: 25%;\n    left: 0;\n    -webkit-transform: inherit;\n    transform: inherit;\n    margin: 40px auto; } }\n@media (max-width: 767px) {\n  .error-card {\n    width: 90%; }\n  .error-card.global {\n    position: relative;\n    top: 25%;\n    left: 0;\n    -webkit-transform: inherit;\n    transform: inherit;\n    margin: 40px auto; } }\n.alert {\n  background-image: none; }\n.alert.alert-primary {\n  background-color: #52bcd3;\n  border-color: #52bcd3;\n  color: #fff; }\n.alert.alert-primary hr {\n  border-top-color: #3eb4ce; }\n.alert.alert-primary .alert-link {\n  color: #e6e6e6; }\n.alert.alert-success {\n  background-color: #4bcf99;\n  border-color: #4bcf99;\n  color: #fff; }\n.alert.alert-success hr {\n  border-top-color: #37ca8e; }\n.alert.alert-success .alert-link {\n  color: #e6e6e6; }\n.alert.alert-info {\n  background-color: #76d4f5;\n  border-color: #76d4f5;\n  color: #fff; }\n.alert.alert-info hr {\n  border-top-color: #5ecdf3; }\n.alert.alert-info .alert-link {\n  color: #e6e6e6; }\n.alert.alert-warning {\n  background-color: #fe974b;\n  border-color: #fe974b;\n  color: #fff; }\n.alert.alert-warning hr {\n  border-top-color: #fe8832; }\n.alert.alert-warning .alert-link {\n  color: #e6e6e6; }\n.alert.alert-danger {\n  background-color: #f44;\n  border-color: #f44;\n  color: #fff; }\n.alert.alert-danger hr {\n  border-top-color: #ff2b2b; }\n.alert.alert-danger .alert-link {\n  color: #e6e6e6; }\n.alert.alert-inverse {\n  background-color: #131e26;\n  border-color: #131e26;\n  color: #fff; }\n.alert.alert-inverse hr {\n  border-top-color: #0b1115; }\n.alert.alert-inverse .alert-link {\n  color: #e6e6e6; }\n.animated {\n  -webkit-animation-duration: .5s;\n  animation-duration: .5s;\n  -webkit-animation-delay: .1s;\n  animation-delay: .1s; }\n.btn {\n  background-image: none;\n  border-radius: 0;\n  margin-bottom: 5px; }\n.btn.btn-primary {\n  color: #fff;\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.btn.btn-primary:hover {\n  color: #fff;\n  background-color: #31a7c1;\n  border-color: #2fa0b9; }\n.btn.btn-primary.focus, .btn.btn-primary:focus {\n  color: #fff;\n  background-color: #31a7c1;\n  border-color: #2fa0b9; }\n.btn.btn-primary.active, .btn.btn-primary:active, .open > .btn.btn-primary.dropdown-toggle {\n  color: #fff;\n  background-color: #31a7c1;\n  border-color: #2fa0b9;\n  background-image: none; }\n.btn.btn-primary.active.focus, .btn.btn-primary.active:focus, .btn.btn-primary.active:hover, .btn.btn-primary:active.focus, .btn.btn-primary:active:focus, .btn.btn-primary:active:hover, .open > .btn.btn-primary.dropdown-toggle.focus, .open > .btn.btn-primary.dropdown-toggle:focus, .open > .btn.btn-primary.dropdown-toggle:hover {\n  color: #fff;\n  background-color: #2a8fa4;\n  border-color: #227284; }\n.btn.btn-primary.disabled.focus, .btn.btn-primary.disabled:focus, .btn.btn-primary:disabled.focus, .btn.btn-primary:disabled:focus {\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.btn.btn-primary.disabled:hover, .btn.btn-primary:disabled:hover {\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.btn.btn-secondary {\n  color: #4f5f6f;\n  background-color: #fff;\n  border-color: #d7dde4; }\n.btn.btn-secondary:hover {\n  color: #4f5f6f;\n  background-color: #e6e6e6;\n  border-color: #b2becb; }\n.btn.btn-secondary.focus, .btn.btn-secondary:focus {\n  color: #4f5f6f;\n  background-color: #e6e6e6;\n  border-color: #b2becb; }\n.btn.btn-secondary.active, .btn.btn-secondary:active, .open > .btn.btn-secondary.dropdown-toggle {\n  color: #4f5f6f;\n  background-color: #e6e6e6;\n  border-color: #b2becb;\n  background-image: none; }\n.btn.btn-secondary.active.focus, .btn.btn-secondary.active:focus, .btn.btn-secondary.active:hover, .btn.btn-secondary:active.focus, .btn.btn-secondary:active:focus, .btn.btn-secondary:active:hover, .open > .btn.btn-secondary.dropdown-toggle.focus, .open > .btn.btn-secondary.dropdown-toggle:focus, .open > .btn.btn-secondary.dropdown-toggle:hover {\n  color: #4f5f6f;\n  background-color: #d4d4d4;\n  border-color: #8b9cb1; }\n.btn.btn-secondary.disabled.focus, .btn.btn-secondary.disabled:focus, .btn.btn-secondary:disabled.focus, .btn.btn-secondary:disabled:focus {\n  background-color: #fff;\n  border-color: #d7dde4; }\n.btn.btn-secondary.disabled:hover, .btn.btn-secondary:disabled:hover {\n  background-color: #fff;\n  border-color: #d7dde4; }\n.btn.btn-success {\n  color: #fff;\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n.btn.btn-success:hover {\n  color: #fff;\n  background-color: #31b680;\n  border-color: #2eae7a; }\n.btn.btn-success.focus, .btn.btn-success:focus {\n  color: #fff;\n  background-color: #31b680;\n  border-color: #2eae7a; }\n.btn.btn-success.active, .btn.btn-success:active, .open > .btn.btn-success.dropdown-toggle {\n  color: #fff;\n  background-color: #31b680;\n  border-color: #2eae7a;\n  background-image: none; }\n.btn.btn-success.active.focus, .btn.btn-success.active:focus, .btn.btn-success.active:hover, .btn.btn-success:active.focus, .btn.btn-success:active:focus, .btn.btn-success:active:hover, .open > .btn.btn-success.dropdown-toggle.focus, .open > .btn.btn-success.dropdown-toggle:focus, .open > .btn.btn-success.dropdown-toggle:hover {\n  color: #fff;\n  background-color: #299a6c;\n  border-color: #217a55; }\n.btn.btn-success.disabled.focus, .btn.btn-success.disabled:focus, .btn.btn-success:disabled.focus, .btn.btn-success:disabled:focus {\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n.btn.btn-success.disabled:hover, .btn.btn-success:disabled:hover {\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n.btn.btn-info {\n  color: #fff;\n  background-color: #76d4f5;\n  border-color: #76d4f5; }\n.btn.btn-info:hover {\n  color: #fff;\n  background-color: #46c5f2;\n  border-color: #3dc2f1; }\n.btn.btn-info.focus, .btn.btn-info:focus {\n  color: #fff;\n  background-color: #46c5f2;\n  border-color: #3dc2f1; }\n.btn.btn-info.active, .btn.btn-info:active, .open > .btn.btn-info.dropdown-toggle {\n  color: #fff;\n  background-color: #46c5f2;\n  border-color: #3dc2f1;\n  background-image: none; }\n.btn.btn-info.active.focus, .btn.btn-info.active:focus, .btn.btn-info.active:hover, .btn.btn-info:active.focus, .btn.btn-info:active:focus, .btn.btn-info:active:hover, .open > .btn.btn-info.dropdown-toggle.focus, .open > .btn.btn-info.dropdown-toggle:focus, .open > .btn.btn-info.dropdown-toggle:hover {\n  color: #fff;\n  background-color: #25bbef;\n  border-color: #10a7db; }\n.btn.btn-info.disabled.focus, .btn.btn-info.disabled:focus, .btn.btn-info:disabled.focus, .btn.btn-info:disabled:focus {\n  background-color: #76d4f5;\n  border-color: #76d4f5; }\n.btn.btn-info.disabled:hover, .btn.btn-info:disabled:hover {\n  background-color: #76d4f5;\n  border-color: #76d4f5; }\n.btn.btn-warning {\n  color: #fff;\n  background-color: #fe974b;\n  border-color: #fe974b; }\n.btn.btn-warning:hover {\n  color: #fff;\n  background-color: #fe7a18;\n  border-color: #fe740e; }\n.btn.btn-warning.focus, .btn.btn-warning:focus {\n  color: #fff;\n  background-color: #fe7a18;\n  border-color: #fe740e; }\n.btn.btn-warning.active, .btn.btn-warning:active, .open > .btn.btn-warning.dropdown-toggle {\n  color: #fff;\n  background-color: #fe7a18;\n  border-color: #fe740e;\n  background-image: none; }\n.btn.btn-warning.active.focus, .btn.btn-warning.active:focus, .btn.btn-warning.active:hover, .btn.btn-warning:active.focus, .btn.btn-warning:active:focus, .btn.btn-warning:active:hover, .open > .btn.btn-warning.dropdown-toggle.focus, .open > .btn.btn-warning.dropdown-toggle:focus, .open > .btn.btn-warning.dropdown-toggle:hover {\n  color: #fff;\n  background-color: #f16701;\n  border-color: #c85601; }\n.btn.btn-warning.disabled.focus, .btn.btn-warning.disabled:focus, .btn.btn-warning:disabled.focus, .btn.btn-warning:disabled:focus {\n  background-color: #fe974b;\n  border-color: #fe974b; }\n.btn.btn-warning.disabled:hover, .btn.btn-warning:disabled:hover {\n  background-color: #fe974b;\n  border-color: #fe974b; }\n.btn.btn-danger {\n  color: #fff;\n  background-color: #f44;\n  border-color: #f44; }\n.btn.btn-danger:hover {\n  color: #fff;\n  background-color: #f11;\n  border-color: #ff0707; }\n.btn.btn-danger.focus, .btn.btn-danger:focus {\n  color: #fff;\n  background-color: #f11;\n  border-color: #ff0707; }\n.btn.btn-danger.active, .btn.btn-danger:active, .open > .btn.btn-danger.dropdown-toggle {\n  color: #fff;\n  background-color: #f11;\n  border-color: #ff0707;\n  background-image: none; }\n.btn.btn-danger.active.focus, .btn.btn-danger.active:focus, .btn.btn-danger.active:hover, .btn.btn-danger:active.focus, .btn.btn-danger:active:focus, .btn.btn-danger:active:hover, .open > .btn.btn-danger.dropdown-toggle.focus, .open > .btn.btn-danger.dropdown-toggle:focus, .open > .btn.btn-danger.dropdown-toggle:hover {\n  color: #fff;\n  background-color: #ec0000;\n  border-color: #c40000; }\n.btn.btn-danger.disabled.focus, .btn.btn-danger.disabled:focus, .btn.btn-danger:disabled.focus, .btn.btn-danger:disabled:focus {\n  background-color: #f44;\n  border-color: #f44; }\n.btn.btn-danger.disabled:hover, .btn.btn-danger:disabled:hover {\n  background-color: #f44;\n  border-color: #f44; }\n.btn.btn-primary-outline {\n  color: #52bcd3;\n  background-image: none;\n  background-color: transparent;\n  border-color: #52bcd3; }\n.btn.btn-primary-outline.active, .btn.btn-primary-outline.focus, .btn.btn-primary-outline:active, .btn.btn-primary-outline:focus, .open > .btn.btn-primary-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.btn.btn-primary-outline:hover {\n  color: #fff;\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.btn.btn-primary-outline.disabled.focus, .btn.btn-primary-outline.disabled:focus, .btn.btn-primary-outline:disabled.focus, .btn.btn-primary-outline:disabled:focus {\n  border-color: #a3dbe8; }\n.btn.btn-primary-outline.disabled:hover, .btn.btn-primary-outline:disabled:hover {\n  border-color: #a3dbe8; }\n.btn.btn-secondary-outline {\n  color: #d7dde4;\n  background-image: none;\n  background-color: transparent;\n  border-color: #d7dde4; }\n.btn.btn-secondary-outline.active, .btn.btn-secondary-outline.focus, .btn.btn-secondary-outline:active, .btn.btn-secondary-outline:focus, .open > .btn.btn-secondary-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #d7dde4;\n  border-color: #d7dde4; }\n.btn.btn-secondary-outline:hover {\n  color: #fff;\n  background-color: #d7dde4;\n  border-color: #d7dde4; }\n.btn.btn-secondary-outline.disabled.focus, .btn.btn-secondary-outline.disabled:focus, .btn.btn-secondary-outline:disabled.focus, .btn.btn-secondary-outline:disabled:focus {\n  border-color: #fff; }\n.btn.btn-secondary-outline.disabled:hover, .btn.btn-secondary-outline:disabled:hover {\n  border-color: #fff; }\n.btn.btn-info-outline {\n  color: #76d4f5;\n  background-image: none;\n  background-color: transparent;\n  border-color: #76d4f5; }\n.btn.btn-info-outline.active, .btn.btn-info-outline.focus, .btn.btn-info-outline:active, .btn.btn-info-outline:focus, .open > .btn.btn-info-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #76d4f5;\n  border-color: #76d4f5; }\n.btn.btn-info-outline:hover {\n  color: #fff;\n  background-color: #76d4f5;\n  border-color: #76d4f5; }\n.btn.btn-info-outline.disabled.focus, .btn.btn-info-outline.disabled:focus, .btn.btn-info-outline:disabled.focus, .btn.btn-info-outline:disabled:focus {\n  border-color: #d5f2fc; }\n.btn.btn-info-outline.disabled:hover, .btn.btn-info-outline:disabled:hover {\n  border-color: #d5f2fc; }\n.btn.btn-success-outline {\n  color: #4bcf99;\n  background-image: none;\n  background-color: transparent;\n  border-color: #4bcf99; }\n.btn.btn-success-outline.active, .btn.btn-success-outline.focus, .btn.btn-success-outline:active, .btn.btn-success-outline:focus, .open > .btn.btn-success-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n.btn.btn-success-outline:hover {\n  color: #fff;\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n.btn.btn-success-outline.disabled.focus, .btn.btn-success-outline.disabled:focus, .btn.btn-success-outline:disabled.focus, .btn.btn-success-outline:disabled:focus {\n  border-color: #9ce4c7; }\n.btn.btn-success-outline.disabled:hover, .btn.btn-success-outline:disabled:hover {\n  border-color: #9ce4c7; }\n.btn.btn-warning-outline {\n  color: #fe974b;\n  background-image: none;\n  background-color: transparent;\n  border-color: #fe974b; }\n.btn.btn-warning-outline.active, .btn.btn-warning-outline.focus, .btn.btn-warning-outline:active, .btn.btn-warning-outline:focus, .open > .btn.btn-warning-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #fe974b;\n  border-color: #fe974b; }\n.btn.btn-warning-outline:hover {\n  color: #fff;\n  background-color: #fe974b;\n  border-color: #fe974b; }\n.btn.btn-warning-outline.disabled.focus, .btn.btn-warning-outline.disabled:focus, .btn.btn-warning-outline:disabled.focus, .btn.btn-warning-outline:disabled:focus {\n  border-color: #ffd2b0; }\n.btn.btn-warning-outline.disabled:hover, .btn.btn-warning-outline:disabled:hover {\n  border-color: #ffd2b0; }\n.btn.btn-danger-outline {\n  color: #f44;\n  background-image: none;\n  background-color: transparent;\n  border-color: #f44; }\n.btn.btn-danger-outline.active, .btn.btn-danger-outline.focus, .btn.btn-danger-outline:active, .btn.btn-danger-outline:focus, .open > .btn.btn-danger-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #f44;\n  border-color: #f44; }\n.btn.btn-danger-outline:hover {\n  color: #fff;\n  background-color: #f44;\n  border-color: #f44; }\n.btn.btn-danger-outline.disabled.focus, .btn.btn-danger-outline.disabled:focus, .btn.btn-danger-outline:disabled.focus, .btn.btn-danger-outline:disabled:focus {\n  border-color: #faa; }\n.btn.btn-danger-outline.disabled:hover, .btn.btn-danger-outline:disabled:hover {\n  border-color: #faa; }\n.btn.btn-oval:focus, .btn.btn-pill-left:focus, .btn.btn-pill-right:focus {\n  outline: 0;\n  outline-offset: initial; }\n.btn.btn-pill-left {\n  border-top-left-radius: 25px;\n  border-bottom-left-radius: 25px; }\n.btn.btn-pill-right {\n  border-top-right-radius: 25px;\n  border-bottom-right-radius: 25px; }\n.btn.btn-oval {\n  border-radius: 25px; }\n.btn.btn-link {\n  text-decoration: none; }\n.btn strong {\n  font-weight: 600; }\n.btn-group .dropdown-menu > li:last-child a:hover:before {\n  height: 0;\n  -webkit-transform: scaleX(0);\n  transform: scaleX(0); }\n.card {\n  background-color: #fff;\n  box-shadow: 1px 1px 5px rgba(126, 142, 159, 0.1);\n  margin-bottom: 10px;\n  border-radius: 0;\n  border: none; }\n.card .card {\n  box-shadow: none; }\n.card .card-header {\n  background-image: none;\n  background-color: #fff;\n  align-items: center;\n  display: flex;\n  flex-direction: row;\n  padding: 0;\n  border-radius: 0;\n  min-height: 50px;\n  border: none; }\n.card .card-header::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n.card .card-header.bordered {\n  border-bottom: 1px solid #d7dde4; }\n.card .card-header.card-header-sm {\n  min-height: 40px; }\n.card .card-header > span {\n  vertical-align: middle; }\n.card .card-header .pull-right {\n  margin-left: auto; }\n.card .card-header .header-block {\n  padding: .5rem 15px; }\n@media (min-width: 1200px) {\n  .card .card-header .header-block {\n    padding: .5rem 20px; } }\n@media (max-width: 767px) {\n  .card .card-header .header-block {\n    padding: .5rem 10px; } }\n.card .card-header .title {\n  color: #4f5f6f;\n  display: inline-flex; }\n.card .card-header .btn {\n  margin: 0; }\n.card .card-header .nav-tabs {\n  border-color: transparent;\n  align-self: stretch;\n  display: flex;\n  position: relative;\n  top: 1px; }\n.card .card-header .nav-tabs .nav-item {\n  margin-left: 0;\n  display: flex;\n  align-self: stretch; }\n.card .card-header .nav-tabs .nav-item .nav-link {\n  display: flex;\n  align-self: stretch;\n  align-items: center;\n  color: #4f5f6f;\n  opacity: .7;\n  padding-left: 10px;\n  padding-right: 10px;\n  border-radius: 0;\n  font-size: 14px;\n  border-top-width: 2px;\n  border-bottom: 1px solid #d7dde4;\n  text-decoration: none; }\n.card .card-header .nav-tabs .nav-item .nav-link.active {\n  border-top-color: #52bcd3;\n  border-bottom-color: transparent;\n  opacity: 1; }\n.card .card-header .nav-tabs .nav-item .nav-link.active:focus, .card .card-header .nav-tabs .nav-item .nav-link.active:hover {\n  opacity: 1;\n  background-color: #fff;\n  border-color: #d7dde4 #d7dde4 transparent;\n  border-top-color: #52bcd3; }\n.card .card-header .nav-tabs .nav-item .nav-link:focus, .card .card-header .nav-tabs .nav-item .nav-link:hover {\n  opacity: 1;\n  background-color: transparent;\n  border-color: transparent; }\n.card.card-default > .card-header {\n  background-color: #fff;\n  color: inherit; }\n.card.card-primary {\n  border-color: #52bcd3; }\n.card.card-primary > .card-header {\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.card.card-success > .card-header {\n  background-color: #4bcf99; }\n.card.card-info > .card-header {\n  background-color: #76d4f5; }\n.card.card-warning > .card-header {\n  background-color: #fe974b; }\n.card.card-danger > .card-header {\n  background-color: #f44; }\n.card.card-inverse > .card-header {\n  background-color: #131e26; }\n.card .card-title-block, .card .title-block {\n  padding-bottom: 0;\n  margin-bottom: 20px;\n  border: none; }\n.card .card-title-block::after, .card .title-block::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n.card .section {\n  margin-bottom: 20px; }\n.card .example, .card .section.demo {\n  margin-bottom: 20px; }\n.card-block {\n  padding: 15px; }\n.card-block .tab-content {\n  padding: 0;\n  border-color: transparent; }\n@media (min-width: 1200px) {\n  .card-block {\n    padding: 20px; } }\n@media (max-width: 767px) {\n  .card-block {\n    padding: 10px; } }\n.card-footer {\n  background-color: #fafafa; }\n.easy-pie-chart {\n  width: 50px;\n  height: 50px;\n  display: inline-block;\n  background-color: #d7dde4;\n  border-radius: 5px; }\n.dropdown-menu {\n  float: left;\n  box-shadow: 2px 3px 6px rgba(126, 142, 159, 0.1);\n  border: 1px solid rgba(126, 142, 159, 0.1);\n  border-top-left-radius: 0;\n  border-top-right-radius: 0; }\n.dropdown-menu .dropdown-item {\n  display: block;\n  padding: 0 15px;\n  clear: both;\n  font-weight: 400;\n  color: #4f5f6f;\n  white-space: nowrap;\n  transition: none; }\n.dropdown-menu .dropdown-item i {\n  margin-right: 2px; }\n.dropdown-menu .dropdown-item:hover {\n  color: #52bcd3 !important;\n  background: 0 0;\n  background-color: #f5f5f5; }\n.flex-row {\n  display: flex;\n  flex-direction: row; }\n.flex-col {\n  display: flex;\n  flex-direction: column; }\n.centralize-y {\n  display: flex;\n  align-items: center; }\ninput, textarea {\n  outline: 0; }\n.form-group .row {\n  margin-left: -10px;\n  margin-right: -10px; }\n.form-group .row [class^=col] {\n  padding-left: 10px;\n  padding-right: 10px; }\n.form-group.has-error span.has-error {\n  color: #f44;\n  font-size: 13px;\n  display: block !important; }\n.form-group.has-error .form-control-feedback {\n  color: #f44; }\n.form-group.has-warning span.has-warning {\n  color: #fe974b;\n  font-size: 13px;\n  display: block !important; }\n.form-group.has-warning .form-control-feedback {\n  color: #fe974b; }\n.form-group.has-success span.has-success {\n  color: #4bcf99;\n  font-size: 13px;\n  display: block !important; }\n.form-group.has-success .form-control-feedback {\n  color: #4bcf99; }\n.input-group {\n  margin-bottom: 10px; }\n.input-group .form-control {\n  padding-left: 5px; }\n.input-group span.input-group-addon {\n  font-style: italic;\n  border: none;\n  border-radius: 0;\n  border: none;\n  background-color: #d7dde4;\n  transition: background-color ease-in-out 15s,color ease-in-out .15s; }\n.input-group span.input-group-addon.focus {\n  background-color: #52bcd3;\n  color: #fff; }\n.control-label, label {\n  font-weight: 600; }\n.form-control.underlined {\n  padding-left: 0;\n  padding-right: 0;\n  border-radius: 0;\n  border: none;\n  border-radius: 0;\n  box-shadow: none;\n  border-bottom: 1px solid #d7dde4; }\n.form-control.underlined.indented {\n  padding: .375rem .75rem; }\n.form-control.underlined:focus, .has-error .form-control.underlined:focus, .has-success .form-control.underlined:focus, .has-warning .form-control.underlined:focus {\n  border: none;\n  box-shadow: none;\n  border-bottom: 1px solid #52bcd3; }\n.has-error .form-control.underlined {\n  box-shadow: none;\n  border-color: #f44; }\n.has-warning .form-control.underlined {\n  box-shadow: none;\n  border-color: #fe974b; }\n.has-success .form-control.underlined {\n  box-shadow: none;\n  border-color: #4bcf99; }\n.form-control.boxed {\n  border-radius: 0;\n  box-shadow: none; }\n.form-control.boxed:focus {\n  border: 1px solid #52bcd3; }\n.checkbox, .radio {\n  display: none; }\n.checkbox + span, .radio + span {\n  padding: 0;\n  padding-right: 10px; }\n.checkbox + span:before, .radio + span:before {\n  font-family: FontAwesome;\n  font-size: 21px;\n  display: inline-block;\n  vertical-align: middle;\n  letter-spacing: 10px;\n  color: #c8d0da; }\n.checkbox:checked + span:before, .radio:checked + span:before {\n  color: #52bcd3; }\n.checkbox:disabled + span:before, .radio:disabled + span:before {\n  opacity: .5;\n  cursor: not-allowed; }\n.checkbox:checked:disabled + span:before, .radio:checked:disabled + span:before {\n  color: #c8d0da; }\n.checkbox + span:before {\n  content: \"\\f0c8\"; }\n.checkbox:checked + span:before {\n  content: \"\\f14a\"; }\n.checkbox.rounded + span:before {\n  content: \"\\f111\"; }\n.checkbox.rounded:checked + span:before {\n  content: \"\\f058\"; }\n.radio + span:before {\n  content: \"\\f111\"; }\n.radio:checked + span:before {\n  content: \"\\f192\"; }\n.radio.squared + span:before {\n  content: \"\\f0c8\"; }\n.radio.squared:checked + span:before {\n  content: \"\\f14a\"; }\n.form-control::-webkit-input-placeholder {\n  font-style: italic;\n  color: #c8d0da; }\n.form-control:-moz-placeholder {\n  font-style: italic;\n  color: #d7dde4; }\n.form-control::-moz-placeholder {\n  font-style: italic;\n  color: #d7dde4; }\n.form-control:-ms-input-placeholder {\n  font-style: italic;\n  color: #d7dde4; }\n.images-container::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n.images-container .image-container {\n  float: left;\n  padding: 3px;\n  margin-right: 10px;\n  margin-bottom: 35px;\n  position: relative;\n  border: 1px solid #e6eaee;\n  overflow: hidden; }\n.images-container .image-container.active {\n  border-color: #52bcd3; }\n.images-container .image-container:hover .controls {\n  bottom: 0;\n  opacity: 1; }\n.images-container .controls {\n  position: absolute;\n  left: 0;\n  right: 0;\n  opacity: 0;\n  bottom: -35px;\n  text-align: center;\n  height: 35px;\n  font-size: 24px;\n  transition: bottom .2s ease,opacity .2s ease;\n  background-color: #fff; }\n.images-container .controls::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n.images-container .controls .control-btn {\n  display: inline-block;\n  color: #4f5f6f;\n  cursor: pointer;\n  width: 35px;\n  height: 35px;\n  line-height: 35px;\n  text-align: center;\n  opacity: .5;\n  transition: opacity .3s ease; }\n.images-container .controls .control-btn:hover {\n  opacity: 1; }\n.images-container .controls .control-btn.move {\n  cursor: move; }\n.images-container .controls .control-btn.star {\n  color: #ffb300; }\n.images-container .controls .control-btn.star i:before {\n  content: \"\\f006\"; }\n.images-container .controls .control-btn.star.active i:before {\n  content: \"\\f005\"; }\n.images-container .controls .control-btn.remove {\n  color: #f44; }\n.images-container .image {\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n  width: 130px;\n  height: 135px;\n  line-height: 135px;\n  text-align: center; }\n.images-container .image-container.main {\n  border-color: #ffb300; }\n.images-container .image-container.new {\n  opacity: .6;\n  transition: opacity .3s ease;\n  border-style: dashed;\n  border: 1px #52bcd3 solid;\n  color: #52bcd3; }\n.images-container .image-container.new .image {\n  font-size: 2.5rem; }\n.images-container .image-container.new:hover {\n  opacity: 1; }\n.item-list {\n  list-style: none;\n  padding: 0;\n  margin: 0;\n  margin-bottom: 0;\n  line-height: 1.4rem;\n  display: flex;\n  flex-flow: column nowrap; }\n@media (min-width: 992px) and (max-width: 1199px) {\n  .item-list {\n    font-size: 1rem; } }\n@media (min-width: 768px) and (max-width: 991px) {\n  .item-list {\n    font-size: .95rem; } }\n@media (max-width: 767px) {\n  .item-list {\n    font-size: 1.05rem; } }\n.item-list.striped > li {\n  border-bottom: 1px solid #e9edf0; }\n.item-list.striped > li:nth-child(2n+1) {\n  background-color: #fcfcfd; }\n@media (max-width: 767px) {\n  .item-list.striped > li:nth-child(2n+1) {\n    background-color: #f8f9fb; } }\n.item-list.striped .item-list-footer {\n  border-bottom: none; }\n.item-list .item {\n  display: flex;\n  flex-direction: column; }\n.item-list .item-row {\n  display: flex;\n  align-items: stretch;\n  flex-direction: row;\n  justify-content: space-between;\n  flex-wrap: wrap;\n  min-width: 100%; }\n.item-list .item-row.nowrap {\n  flex-wrap: nowrap; }\n.item-list .item-col {\n  align-items: center;\n  display: flex;\n  padding: 10px 10px 10px 0;\n  flex-basis: 0;\n  flex-grow: 3;\n  flex-shrink: 3;\n  margin-left: auto;\n  margin-right: auto;\n  min-width: 0; }\n.item-list .item-col.fixed {\n  flex-grow: 0;\n  flex-shrink: 0;\n  flex-basis: auto; }\n.item-list .item-col.pull-left {\n  margin-right: auto; }\n.item-list .item-col.pull-right {\n  margin-left: auto; }\n.item-list .item-col > div {\n  width: 100%; }\n.item-list .item-col:last-child {\n  padding-right: 0; }\n.item-list .no-overflow {\n  overflow: hidden; }\n.item-list .no-wrap {\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap; }\n.item-list .item-list-header .item-col.item-col-header span {\n  color: #999;\n  font-size: .8rem;\n  font-weight: 700 !important; }\n.item-list .item-heading {\n  font-size: .9rem;\n  display: none;\n  color: #666;\n  font-weight: 700;\n  padding-right: 10px; }\n@media (max-width: 767px) {\n  .item-list .item-heading {\n    display: block; } }\n@media (min-width: 544px) and (max-width: 767px) {\n  .item-list .item-heading {\n    width: 100%; } }\n@media (max-width: 543px) {\n  .item-list .item-heading {\n    width: 40%; } }\n.item-list .item-col.item-col-check {\n  flex-basis: 30px; }\n@media (max-width: 767px) {\n  .item-list .item-col.item-col-check {\n    order: -8; } }\n.item-list .item-check {\n  margin-bottom: 0; }\n.item-list .item-check .checkbox + span {\n  padding-right: 0; }\n.item-list .item-check .checkbox + span:before {\n  width: 20px; }\n.item-list .item-col.item-col-img {\n  display: flex;\n  flex-basis: 70px; }\n.item-list .item-col.item-col-img.xs {\n  flex-basis: 40px; }\n.item-list .item-col.item-col-img.sm {\n  flex-basis: 50px; }\n.item-list .item-col.item-col-img.lg {\n  flex-basis: 100px; }\n.item-list .item-col.item-col-img.xl {\n  flex-basis: 120px; }\n.item-list .item-col.item-col-img a {\n  width: 100%; }\n.item-list .item-img {\n  flex-grow: 1;\n  -ms-grid-row-align: stretch;\n      align-self: stretch;\n  background-color: #efefef;\n  padding-bottom: 100%;\n  width: 100%;\n  height: 0;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat; }\n@media (max-width: 767px) {\n  .item-list .item-col.item-col-title {\n    order: -4; } }\n.item-list .item-col.item-col-title a {\n  display: block; }\n.item-list .item-title {\n  margin: 0;\n  font-size: inherit;\n  line-height: inherit;\n  font-weight: 600; }\n.item-list .item-stats {\n  height: 1.4rem; }\n.item-list .item-col.item-col-actions-dropdown {\n  flex-basis: 40px;\n  text-align: center;\n  padding-left: 0 !important; }\n@media (max-width: 767px) {\n  .item-list .item-col.item-col-actions-dropdown {\n    order: -3;\n    flex-basis: 40px !important;\n    padding-right: 10px; } }\n.item-list .item-actions-dropdown {\n  position: relative;\n  font-size: 1.1rem; }\n.item-list .item-actions-dropdown.active .item-actions-block {\n  max-width: 120px; }\n.item-list .item-actions-dropdown.active .item-actions-toggle-btn {\n  color: #52bcd3; }\n.item-list .item-actions-dropdown.active .item-actions-toggle-btn .active {\n  display: block; }\n.item-list .item-actions-dropdown.active .item-actions-toggle-btn .inactive {\n  display: none; }\n.item-list .item-actions-dropdown .item-actions-toggle-btn {\n  color: #9ba8b5;\n  font-size: 1.2rem;\n  cursor: pointer;\n  width: 100%;\n  line-height: 30px;\n  text-align: center;\n  text-decoration: none; }\n.item-list .item-actions-dropdown .item-actions-toggle-btn .active {\n  display: none; }\n.item-list .item-actions-dropdown .item-actions-block {\n  height: 30px;\n  max-width: 0;\n  line-height: 30px;\n  overflow: hidden;\n  position: absolute;\n  top: 0;\n  right: 100%;\n  background-color: #d7dde4;\n  border-radius: 3px;\n  transition: all .15s ease-in-out; }\n.item-list .item-actions-dropdown .item-actions-block.direction-right {\n  right: auto;\n  left: 100%; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list {\n  padding: 0;\n  list-style: none;\n  white-space: nowrap;\n  padding: 0 5px; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list li {\n  display: inline-block;\n  padding: 0; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list a {\n  display: block;\n  padding: 0 5px; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list a.edit {\n  color: #38424c; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list a.check {\n  color: #40b726; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list a.remove {\n  color: #db0e1e; }\n.card > .item-list .item > .item-row {\n  padding: 0 15px; }\n@media (min-width: 1200px) {\n  .card > .item-list .item > .item-row {\n    padding: 0 20px; } }\n@media (max-width: 767px) {\n  .card > .item-list .item > .item-row {\n    padding: 0 10px; } }\n.logo {\n  display: inline-block;\n  width: 45px;\n  height: 25px;\n  vertical-align: middle;\n  margin-right: 5px;\n  position: relative; }\n.logo .l {\n  width: 11px;\n  height: 11px;\n  border-radius: 50%;\n  background-color: #52bcd3;\n  position: absolute; }\n.logo .l.l1 {\n  bottom: 0;\n  left: 0; }\n.logo .l.l2 {\n  width: 7px;\n  height: 7px;\n  bottom: 13px;\n  left: 10px; }\n.logo .l.l3 {\n  width: 7px;\n  height: 7px;\n  bottom: 4px;\n  left: 17px; }\n.logo .l.l4 {\n  bottom: 13px;\n  left: 25px; }\n.logo .l.l5 {\n  bottom: 0;\n  left: 34px; }\n.modal-body.modal-tab-container {\n  padding: 0; }\n.modal-body.modal-tab-container .modal-tabs {\n  padding-left: 0;\n  margin-bottom: 0;\n  list-style: none;\n  background-color: #fff;\n  border-bottom: 1px solid #ddd;\n  box-shadow: 0 0 2px rgba(0, 0, 0, 0.3); }\n.modal-body.modal-tab-container .modal-tabs .nav-link {\n  padding: 10px 20px;\n  border: none; }\n.modal-body.modal-tab-container .modal-tabs .nav-link.active, .modal-body.modal-tab-container .modal-tabs .nav-link:hover {\n  color: #52bcd3;\n  border-bottom: 2px solid #52bcd3; }\n.modal-body.modal-tab-container .modal-tabs .nav-link.active {\n  font-weight: 600; }\na:not(.btn) {\n  transition: color .13s;\n  text-decoration: none;\n  color: #52bcd3; }\na:not(.btn):hover {\n  text-decoration: inherit;\n  color: #2c96ad; }\na:not(.btn):hover:before {\n  -webkit-transform: scaleX(1);\n  transform: scaleX(1); }\na:not(.btn):focus {\n  text-decoration: none; }\nspan a {\n  vertical-align: text-bottom; }\n[class*=' nav'] li > a, [class^=nav] li > a {\n  display: block; }\n[class*=' nav'] li > a:before, [class^=nav] li > a:before {\n  display: none; }\n.nav.nav-tabs-bordered {\n  border-color: #52bcd3; }\n.nav.nav-tabs-bordered + .tab-content {\n  border-style: solid;\n  border-width: 0 1px 1px 1px;\n  border-color: #52bcd3;\n  padding: 10px 20px 0; }\n.nav.nav-tabs-bordered .nav-item .nav-link {\n  text-decoration: none; }\n.nav.nav-tabs-bordered .nav-item .nav-link:hover {\n  color: #fff;\n  background-color: #52bcd3;\n  border: 1px solid #52bcd3; }\n.nav.nav-tabs-bordered .nav-item .nav-link.active {\n  border-color: #52bcd3;\n  border-bottom-color: transparent; }\n.nav.nav-tabs-bordered .nav-item .nav-link.active:hover {\n  background-color: #fff;\n  color: inherit; }\n.nav.nav-pills + .tab-content {\n  border: 0;\n  padding: 5px; }\n.nav.nav-pills .nav-item .nav-link {\n  text-decoration: none; }\n.nav.nav-pills .nav-item .nav-link:hover {\n  color: #4f5f6f;\n  background-color: transparent;\n  border: 0; }\n.nav.nav-pills .nav-item .nav-link.active {\n  border-color: #52bcd3;\n  border-bottom-color: transparent;\n  background-color: #52bcd3; }\n.nav.nav-pills .nav-item .nav-link.active:hover {\n  background-color: #52bcd3;\n  color: #fff; }\n#nprogress .bar {\n  background: #52bcd3 !important; }\n#nprogress .bar .peg {\n  box-shadow: 0 0 10px #52bcd3,0 0 5px #52bcd3; }\n#nprogress .spinner {\n  top: 25px !important;\n  right: 23px !important; }\n#nprogress .spinner .spinner-icon {\n  border-top-color: #52bcd3 !important;\n  border-left-color: #52bcd3 !important; }\n.pagination {\n  margin-top: 0; }\n.pagination .page-item .page-link {\n  color: #52bcd3; }\n.pagination .page-item.active .page-link, .pagination .page-item.active .page-link:focus, .pagination .page-item.active .page-link:hover {\n  color: #fff;\n  border-color: #52bcd3;\n  background-color: #52bcd3; }\n::-webkit-scrollbar {\n  width: 7px;\n  height: 7px; }\n::-webkit-scrollbar-track {\n  border-radius: 0; }\n::-webkit-scrollbar-thumb {\n  border-radius: 0;\n  background: #3eb4ce; }\n::-webkit-scrollbar-thumb:window-inactive {\n  background: #52bcd3; }\n.table label {\n  margin-bottom: 0; }\n.table .checkbox + span {\n  margin-bottom: 0; }\n.table .checkbox + span:before {\n  line-height: 20px; }\n.row .col {\n  padding-left: .9375rem;\n  padding-right: .9375rem;\n  float: left; }\n.row-sm {\n  margin-left: -10px;\n  margin-right: -10px; }\n.row-sm [class^=col] {\n  padding-left: 10px;\n  padding-right: 10px; }\n.title-block {\n  padding-bottom: 15px;\n  margin-bottom: 30px;\n  border-bottom: 1px solid #d7dde4; }\n.title-block::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n@media (max-width: 767px) {\n  .title-block {\n    margin-bottom: 20px; } }\n.subtitle-block {\n  padding-bottom: 10px;\n  margin-bottom: 20px;\n  border-bottom: 1px dashed #e0e5ea; }\n.section {\n  display: block;\n  margin-bottom: 30px; }\n.section:last-of-type {\n  margin-bottom: 0; }\n.box-placeholder {\n  margin-bottom: 15px;\n  padding: 20px;\n  border: 1px dashed #ddd;\n  background: #fafafa;\n  color: #444;\n  cursor: pointer; }\n.underline-animation {\n  position: absolute;\n  top: auto;\n  bottom: 1px;\n  left: 0;\n  width: 100%;\n  height: 1px;\n  background-color: #52bcd3;\n  content: '';\n  transition: all .2s;\n  -webkit-backface-visibility: hidden;\n  backface-visibility: hidden;\n  -webkit-transform: scaleX(0);\n  transform: scaleX(0); }\n.stat-chart {\n  border-radius: 50%; }\n.stat {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: inline-block;\n  margin-right: 10px; }\n.stat .value {\n  font-size: 20px;\n  line-height: 24px;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  font-weight: 500; }\n.stat .name {\n  overflow: hidden;\n  text-overflow: ellipsis; }\n.stat.lg .value {\n  font-size: 26px;\n  line-height: 28px; }\n.stat.lg .name {\n  font-size: 16px; }\n.list-icon [class^=col] {\n  cursor: pointer; }\n.list-icon [class^=col] em {\n  font-size: 14px;\n  width: 40px;\n  vertical-align: middle;\n  margin: 0;\n  display: inline-block;\n  text-align: center;\n  transition: all 1s;\n  line-height: 30px; }\n.list-icon [class^=col]:hover em {\n  -webkit-transform: scale(2, 2);\n  transform: scale(2, 2); }\n.well {\n  background-image: none;\n  background-color: #fff; }\n.jumbotron {\n  background-image: none;\n  background-color: #fff;\n  padding: 15px 30px; }\n.jumbotron.jumbotron-fluid {\n  padding-left: 0;\n  padding-right: 0; }\n.rounded {\n  border-radius: .25rem; }\n.rounded-l {\n  border-radius: .3rem; }\n.rounded-s {\n  border-radius: .2rem; }\n.jqstooltip {\n  height: 25px !important;\n  width: auto !important;\n  border-radius: .2rem; }\n.title {\n  font-size: 1.45rem;\n  font-weight: 600;\n  margin-bottom: 0; }\n.title.l {\n  font-size: 1.6rem; }\n.title.s {\n  font-size: 1.4rem; }\n.card .title {\n  font-size: 1.1rem;\n  color: #4f5f6f; }\n.title-description {\n  margin: 0;\n  font-size: .9rem;\n  font-weight: 400;\n  color: #7e8e9f; }\n.title-description.s {\n  font-size: .8rem; }\n@media (max-width: 767px) {\n  .title-description {\n    display: none; } }\n.subtitle {\n  font-size: 1.2rem;\n  margin: 0;\n  color: #7e8e9f; }\n.text-primary {\n  color: #52bcd3; }\n.text-muted {\n  color: #9ba8b5; }\npre {\n  padding: 0;\n  border: none;\n  background: 0 0; }\n.flot-chart {\n  display: block;\n  height: 225px; }\n.flot-chart .flot-chart-content {\n  width: 100%;\n  height: 100%; }\n.flot-chart .flot-chart-pie-content {\n  width: 225px;\n  height: 225px;\n  margin: auto; }\n.dashboard-page #dashboard-downloads-chart, .dashboard-page #dashboard-visits-chart {\n  height: 220px; }\n@media (max-width: 543px) {\n  .dashboard-page .items .card-header {\n    border: none;\n    flex-wrap: wrap; }\n  .dashboard-page .items .card-header .header-block {\n    display: flex;\n    align-items: center;\n    width: 100%;\n    justify-content: space-between;\n    border-bottom: 1px solid #e9edf0; } }\n.dashboard-page .items .card-header .title {\n  padding-right: 0;\n  margin-right: 5px; }\n.dashboard-page .items .card-header .search {\n  margin: 0;\n  vertical-align: middle;\n  display: inline-flex;\n  flex-direction: row;\n  align-items: center; }\n@media (max-width: 543px) {\n  .dashboard-page .items .card-header .search {\n    min-width: 50%; } }\n.dashboard-page .items .card-header .search .search-input {\n  border: none;\n  background-color: inherit;\n  color: #c2ccd6;\n  width: 100px;\n  transition: color .3s ease; }\n.dashboard-page .items .card-header .search .search-input::-webkit-input-placeholder {\n  transition: color .3s ease;\n  color: #c2ccd6; }\n.dashboard-page .items .card-header .search .search-input:-moz-placeholder {\n  transition: color .3s ease;\n  color: #c2ccd6; }\n.dashboard-page .items .card-header .search .search-input::-moz-placeholder {\n  transition: color .3s ease;\n  color: #c2ccd6; }\n.dashboard-page .items .card-header .search .search-input:-ms-input-placeholder {\n  transition: color .3s ease;\n  color: #c2ccd6; }\n@media (max-width: 543px) {\n  .dashboard-page .items .card-header .search .search-input {\n    min-width: 130px; } }\n.dashboard-page .items .card-header .search .search-input:focus {\n  color: #7e8e9f; }\n.dashboard-page .items .card-header .search .search-input:focus::-webkit-input-placeholder {\n  color: #aab4c0; }\n.dashboard-page .items .card-header .search .search-input:focus:-moz-placeholder {\n  color: #aab4c0; }\n.dashboard-page .items .card-header .search .search-input:focus::-moz-placeholder {\n  color: #aab4c0; }\n.dashboard-page .items .card-header .search .search-input:focus:-ms-input-placeholder {\n  color: #aab4c0; }\n.dashboard-page .items .card-header .search .search-input:focus + .search-icon {\n  color: #7e8e9f; }\n.dashboard-page .items .card-header .search .search-icon {\n  color: #c2ccd6;\n  transition: color .3s ease;\n  order: -1;\n  padding-right: 6px; }\n.dashboard-page .items .card-header .pagination {\n  display: inline-block;\n  margin: 0; }\n.dashboard-page .items .item-list .item-col-title {\n  flex-grow: 9; }\n.dashboard-page .items .item-list .item-col-date {\n  text-align: right; }\n@media (min-width: 1200px) {\n  .dashboard-page .items .item-list .item-col-date {\n    flex-grow: 4; } }\n@media (max-width: 767px) {\n  .dashboard-page .items .item-list .item-row {\n    padding: 0; }\n  .dashboard-page .items .item-list .item-col {\n    padding-left: 10px;\n    padding-right: 10px; }\n  .dashboard-page .items .item-list .item-col-img {\n    padding-left: 10px;\n    flex-basis: 60px;\n    padding-right: 0; }\n  .dashboard-page .items .item-list .item-col-stats {\n    text-align: center; } }\n@media (min-width: 544px) and (max-width: 767px) {\n  .dashboard-page .items .item-list .item-col-title {\n    flex-basis: 100%;\n    border-bottom: 1px solid #e9edf0; }\n  .dashboard-page .items .item-list .item-col:not(.item-col-title):not(.item-col-img) {\n    position: relative;\n    padding-top: 35px; }\n  .dashboard-page .items .item-list .item-heading {\n    position: absolute;\n    height: 30px;\n    width: 100%;\n    left: 0;\n    top: 5px;\n    line-height: 30px;\n    padding-left: 10px;\n    padding-right: 10px; } }\n@media (max-width: 543px) {\n  .dashboard-page .items .item-list .item-col {\n    border-bottom: 1px solid #e9edf0; }\n  .dashboard-page .items .item-list .item-col-img {\n    flex-basis: 50px;\n    order: -5; }\n  .dashboard-page .items .item-list .item-col-title {\n    flex-basis: calc(100% - 50px); }\n  .dashboard-page .items .item-list .item-col:not(.item-col-title):not(.item-col-img) {\n    flex-basis: 100%;\n    text-align: left; }\n  .dashboard-page .items .item-list .item-col:not(.item-col-title):not(.item-col-img) .item-heading {\n    text-align: left; }\n  .dashboard-page .items .item-list .item-col-date {\n    border: none; } }\n.dashboard-page .sales-breakdown .dashboard-sales-breakdown-chart {\n  margin: 0 auto;\n  max-width: 250px;\n  max-height: 250px; }\n.dashboard-page #dashboard-sales-map .jqvmap-zoomin, .dashboard-page #dashboard-sales-map .jqvmap-zoomout {\n  background-color: #52bcd3;\n  height: 20px;\n  width: 20px;\n  line-height: 14px; }\n.dashboard-page #dashboard-sales-map .jqvmap-zoomout {\n  top: 32px; }\n.dashboard-page .stats .card-block {\n  padding-bottom: 0; }\n.dashboard-page .stats .stat-col {\n  margin-bottom: 20px;\n  float: left;\n  white-space: nowrap;\n  overflow: hidden; }\n.dashboard-page .stats .stat-icon {\n  color: #52bcd3;\n  display: inline-block;\n  font-size: 26px;\n  text-align: center;\n  vertical-align: middle;\n  width: 50px; }\n.dashboard-page .stats .stat-chart {\n  margin-right: 5px;\n  vertical-align: middle; }\n@media (min-width: 1200px) {\n  .dashboard-page .stats .stat-chart {\n    margin-right: .6vw; } }\n.dashboard-page .stats .stat {\n  vertical-align: middle; }\n@media (min-width: 1200px) {\n  .dashboard-page .stats .stat .value {\n    font-size: 1.3vw; } }\n@media (min-width: 1200px) {\n  .dashboard-page .stats .stat .name {\n    font-size: .9vw; } }\n.dashboard-page .stats .stat-progress {\n  height: 2px;\n  margin: 5px 0;\n  color: #52bcd3; }\n.dashboard-page .stats .stat-progress[value]::-webkit-progress-bar {\n  background-color: #ddd; }\n.dashboard-page .stats .stat-progress[value]::-webkit-progress-value {\n  background-color: #52bcd3; }\n.dashboard-page .stats .stat-progress[value]::-moz-progress-bar {\n  background-color: #ddd; }\n.dashboard-page .tasks {\n  display: flex;\n  flex-direction: column;\n  align-content: stretch; }\n.dashboard-page .tasks .title-block .title {\n  align-items: center;\n  display: flex;\n  justify-content: space-between; }\n.dashboard-page .tasks label {\n  width: 100%;\n  margin-bottom: 0; }\n.dashboard-page .tasks label .checkbox:checked + span {\n  text-decoration: line-through; }\n.dashboard-page .tasks label span {\n  display: inline-block;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  width: 100%; }\n.dashboard-page .tasks .tasks-block {\n  max-height: 400px;\n  overflow-y: scroll;\n  overflow-x: hidden;\n  margin: 0;\n  margin-right: -5px; }\n.dashboard-page .tasks .item-list .item-col {\n  padding-top: 5px;\n  padding-bottom: 5px; }\n.items-list-page .title-search-block {\n  position: relative; }\n@media (max-width: 767px) {\n  .items-list-page .title-block {\n    padding-bottom: 10px;\n    margin-bottom: 13px; } }\n.items-list-page .title-block .action {\n  display: inline; }\n.items-list-page .title-block .action a {\n  padding: 10px 15px; }\n.items-list-page .title-block .action a .icon {\n  margin-right: 5px;\n  text-align: center;\n  width: 16px; }\n@media (max-width: 767px) {\n  .items-list-page .title-block .action {\n    display: none; } }\n.items-list-page .items-search {\n  position: absolute;\n  margin-bottom: 15px;\n  right: 0;\n  top: 0; }\n@media (max-width: 767px) {\n  .items-list-page .items-search {\n    position: static; } }\n.items-list-page .items-search .search-button {\n  margin: 0; }\n.items-list-page .item-list .item-col.item-col-check {\n  text-align: left; }\n.items-list-page .item-list .item-col.item-col-img {\n  text-align: left;\n  width: auto;\n  text-align: center;\n  flex-basis: 70px; }\n@media (min-width: 544px) {\n  .items-list-page .item-list .item-col.item-col-img:not(.item-col-header) {\n    height: 80px; } }\n.items-list-page .item-list .item-col.item-col-title {\n  text-align: left;\n  margin-left: 0 !important;\n  margin-right: auto;\n  flex-basis: 0;\n  flex-grow: 9; }\n.items-list-page .item-list .item-col.item-col-sales {\n  text-align: right;\n  font-weight: 600; }\n.items-list-page .item-list .item-col.item-col-stats {\n  text-align: center; }\n.items-list-page .item-list .item-col.item-col-category {\n  text-align: left;\n  font-weight: 600; }\n.items-list-page .item-list .item-col.item-col-author {\n  text-align: left;\n  flex-grow: 4.5; }\n.items-list-page .item-list .item-col.item-col-date {\n  text-align: right; }\n@media (max-width: 767px) {\n  .items-list-page .card.items {\n    background: 0 0;\n    box-shadow: none; }\n  .items-list-page .item-list .item {\n    border: none;\n    margin-bottom: 10px;\n    background-color: #fff;\n    box-shadow: 1px 1px 5px rgba(126, 142, 159, 0.1); }\n  .items-list-page .item-list .item-row {\n    padding: 0 !important; }\n  .items-list-page .item-list .item-col.item-col-author {\n    flex-grow: 3; } }\n@media (min-width: 544px) and (max-width: 767px) {\n  .items-list-page .item-list .item {\n    background-color: #fff;\n    margin-bottom: 10px;\n    box-shadow: 1px 1px 5px rgba(126, 142, 159, 0.1); }\n  .items-list-page .item-list .item-row {\n    padding: 0; }\n  .items-list-page .item-list .item-heading {\n    width: 100%;\n    display: block;\n    position: absolute;\n    top: 0;\n    width: 100%;\n    left: 0;\n    line-height: 40px;\n    padding-left: 0; }\n  .items-list-page .item-list .item-col.item-col-actions-dropdown, .items-list-page .item-list .item-col.item-col-check, .items-list-page .item-list .item-col.item-col-title {\n    border-bottom: 1px solid #d7dde4; }\n  .items-list-page .item-list .item-col.item-col-actions-dropdown .item-heading, .items-list-page .item-list .item-col.item-col-check .item-heading, .items-list-page .item-list .item-col.item-col-title .item-heading {\n    display: none; }\n  .items-list-page .item-list .item-col.item-col-author, .items-list-page .item-list .item-col.item-col-category, .items-list-page .item-list .item-col.item-col-date, .items-list-page .item-list .item-col.item-col-sales, .items-list-page .item-list .item-col.item-col-stats {\n    padding-top: 40px;\n    position: relative; }\n  .items-list-page .item-list .item-col.item-col-check {\n    display: none; }\n  .items-list-page .item-list .item-col.item-col-title {\n    padding-left: 10px;\n    text-align: left;\n    margin-left: 0 !important;\n    margin-right: auto;\n    flex-basis: calc(100% - 40px); }\n  .items-list-page .item-list .item-col.item-col-img {\n    padding-left: 10px;\n    flex-basis: 79px; }\n  .items-list-page .item-list .item-col.item-col-sales {\n    text-align: left; }\n  .items-list-page .item-list .item-col.item-col-stats {\n    text-align: center; }\n  .items-list-page .item-list .item-col.item-col-category {\n    text-align: center; }\n  .items-list-page .item-list .item-col.item-col-author {\n    text-align: center; }\n  .items-list-page .item-list .item-col.item-col-date {\n    padding-right: 10px;\n    text-align: right;\n    white-space: nowrap;\n    flex-basis: 100px;\n    flex-basis: 0;\n    flex-grow: 3; } }\n@media (max-width: 543px) {\n  .items-list-page .item-list .item {\n    border: none;\n    font-size: .9rem;\n    margin-bottom: 10px;\n    background-color: #fff;\n    box-shadow: 1px 1px 5px rgba(126, 142, 159, 0.1); }\n  .items-list-page .item-list .item .item-col {\n    text-align: right;\n    border-bottom: 1px solid #d7dde4;\n    padding-left: 10px; }\n  .items-list-page .item-list .item .item-col[class^=item-col] {\n    flex-basis: 100%; }\n  .items-list-page .item-list .item .item-col.item-col-check {\n    display: none; }\n  .items-list-page .item-list .item .item-col.item-col-img .item-img {\n    padding-bottom: 65%; }\n  .items-list-page .item-list .item .item-col.item-col-title {\n    text-align: left;\n    padding-bottom: 0;\n    border: none;\n    flex-grow: 1;\n    flex-basis: 0; }\n  .items-list-page .item-list .item .item-col.item-col-title .item-heading {\n    display: none; }\n  .items-list-page .item-list .item .item-col.item-col-title .item-title {\n    font-size: 1rem;\n    line-height: 1.4rem; }\n  .items-list-page .item-list .item .item-col.item-col-actions-dropdown {\n    border: none;\n    padding-bottom: 0; }\n  .items-list-page .item-list .item .item-col.item-col-sales {\n    text-align: left; }\n  .items-list-page .item-list .item .item-col.item-col-stats {\n    text-align: left; }\n  .items-list-page .item-list .item .item-col.item-col-category {\n    text-align: left; }\n  .items-list-page .item-list .item .item-col.item-col-author {\n    text-align: left; }\n  .items-list-page .item-list .item .item-col.item-col-date {\n    text-align: left; } }\n.table-flip-scroll table {\n  width: 100%; }\n@media only screen and (max-width: 800px) {\n  .table-flip-scroll .flip-content:after, .table-flip-scroll .flip-header:after {\n    visibility: hidden;\n    display: block;\n    font-size: 0;\n    content: \" \";\n    clear: both;\n    height: 0; }\n  .table-flip-scroll html .flip-content, .table-flip-scroll html .flip-header {\n    -ms-zoom: 1;\n    zoom: 1; }\n  .table-flip-scroll table {\n    width: 100%;\n    border-collapse: collapse;\n    border-spacing: 0;\n    display: block;\n    position: relative; }\n  .table-flip-scroll td, .table-flip-scroll th {\n    margin: 0;\n    vertical-align: top; }\n  .table-flip-scroll td:last-child, .table-flip-scroll th:last-child {\n    border-bottom: 1px solid #ddd; }\n  .table-flip-scroll th {\n    border: 0 !important;\n    border-right: 1px solid #ddd !important;\n    width: auto !important;\n    display: block;\n    text-align: right; }\n  .table-flip-scroll td {\n    display: block;\n    text-align: left;\n    border: 0 !important;\n    border-bottom: 1px solid #ddd !important; }\n  .table-flip-scroll thead {\n    display: block;\n    float: left; }\n  .table-flip-scroll thead tr {\n    display: block; }\n  .table-flip-scroll tbody {\n    display: block;\n    width: auto;\n    position: relative;\n    overflow-x: auto;\n    white-space: nowrap; }\n  .table-flip-scroll tbody tr {\n    display: inline-block;\n    vertical-align: top;\n    margin-left: -5px;\n    border-left: 1px solid #ddd; } }\n.wyswyg {\n  border: 1px solid #d7dde4; }\n.wyswyg .ql-container {\n  border-top: 1px solid #d7dde4; }\n.wyswyg .toolbar .btn {\n  margin: 0; }\n.wyswyg .ql-container {\n  font-size: 1rem; }\n.wyswyg .ql-container .ql-editor {\n  min-height: 200px; }\n.footer {\n  background-color: #fff;\n  position: absolute;\n  left: 230px;\n  right: 0;\n  bottom: 0;\n  height: 50px;\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n.footer-fixed .footer {\n  position: fixed; }\n.footer .footer-block {\n  vertical-align: middle;\n  margin-left: 20px;\n  margin-right: 20px; }\n.footer .footer-github-btn {\n  vertical-align: middle; }\n@media (max-width: 991px) {\n  .footer {\n    left: 0; } }\n.footer .author > ul {\n  list-style: none;\n  margin: 0;\n  padding: 0; }\n.footer .author > ul li {\n  display: inline-block; }\n.footer .author > ul li:after {\n  content: \"|\"; }\n.footer .author > ul li:last-child:after {\n  content: \"\"; }\n@media (max-width: 991px) {\n  .footer .author > ul li {\n    display: block;\n    text-align: right; }\n  .footer .author > ul li:after {\n    content: \"\"; } }\n@media (max-width: 991px) {\n  .footer .author > ul {\n    display: block; } }\n@media (max-width: 767px) {\n  .footer .author > ul {\n    display: none; } }\n.modal .modal-content {\n  border-radius: 0; }\n.modal .modal-header {\n  background-color: #52bcd3;\n  color: #fff; }\n.modal .modal-footer .btn {\n  margin-bottom: 0; }\n.header {\n  background-color: #d7dde4;\n  height: 70px;\n  position: absolute;\n  left: 230px;\n  right: 0;\n  transition: left .3s ease;\n  z-index: 10;\n  display: flex;\n  align-items: center; }\n@media (max-width: 991px) {\n  .header {\n    left: 0; } }\n@media (max-width: 767px) {\n  .header {\n    left: 0;\n    height: 50px; } }\n.header-fixed .header {\n  position: fixed; }\n.header .header-block {\n  margin-right: 15px; }\n@media (max-width: 767px) {\n  .header .header-block {\n    padding: 5px; } }\n.sidebar {\n  background-color: #3a4651;\n  width: 230px;\n  padding-bottom: 60px;\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  transition: left .3s ease;\n  z-index: 20; }\n@media (max-width: 991px) {\n  .sidebar {\n    position: fixed;\n    left: -230px; } }\n.sidebar-fixed .sidebar {\n  position: fixed; }\n.sidebar-open .sidebar {\n  left: 0; }\n.sidebar .sidebar-container {\n  position: absolute;\n  top: 0;\n  bottom: 51px;\n  width: 100%;\n  left: 0;\n  overflow-y: auto;\n  overflow-x: hidden; }\n.sidebar .sidebar-container::-webkit-scrollbar-track {\n  background-color: #2c353e; }\n.sidebar .nav {\n  font-size: 14px; }\n.open .sidebar .nav li a:focus, .sidebar .nav li a:focus {\n  background-color: inherit; }\n.sidebar .nav ul {\n  padding: 0;\n  height: 0;\n  overflow: hidden; }\n.loaded .sidebar .nav ul {\n  height: auto; }\n.sidebar .nav li.active ul {\n  height: auto; }\n.sidebar .nav li a {\n  color: rgba(255, 255, 255, 0.5);\n  text-decoration: none; }\n.sidebar .nav li a:hover, .sidebar .nav li.open a:hover, .sidebar .nav li.open > a {\n  color: #fff;\n  background-color: #2d363f; }\n.sidebar .nav > li > a {\n  padding-top: 15px;\n  padding-bottom: 15px;\n  padding-left: 20px;\n  padding-right: 10px; }\n.sidebar .nav > li.active > a, .sidebar .nav > li.active > a:hover {\n  background-color: #52bcd3 !important;\n  color: #fff !important; }\n.sidebar .nav > li.open > a {\n  background-color: #333e48; }\n.sidebar .nav > li.open > a i.arrow {\n  -webkit-transform: rotate(90deg);\n  transform: rotate(90deg); }\n.sidebar .nav > li > a i {\n  margin-right: 5px;\n  font-size: 16px; }\n.sidebar .nav > li > a i.arrow {\n  float: right;\n  font-size: 20px;\n  line-height: initial;\n  transition: all .3s ease; }\n.sidebar .nav > li > a i.arrow:before {\n  content: \"\" !important; }\n.sidebar .nav > li > ul > li a {\n  padding-top: 10px;\n  padding-bottom: 10px;\n  padding-left: 50px;\n  background-color: #333e48; }\n.sidebar .nav > li > ul > li.active a {\n  color: #fff; }\n.sidebar-overlay {\n  position: absolute;\n  display: none;\n  left: 200vw;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  z-index: 5;\n  opacity: 0;\n  transition: opacity .3s ease;\n  z-index: 15; }\n@media (max-width: 991px) {\n  .sidebar-overlay {\n    display: block; } }\n@media (max-width: 767px) {\n  .sidebar-overlay {\n    background-color: rgba(0, 0, 0, 0.7); } }\n@media (max-width: 991px) {\n  .sidebar-open .sidebar-overlay {\n    left: 0;\n    opacity: 1; } }\n#modal-media .modal-body {\n  min-height: 250px; }\n#modal-media .modal-tab-content {\n  min-height: 300px; }\n#modal-media .images-container {\n  padding: 15px;\n  text-align: center; }\n#modal-media .images-container .image-container {\n  margin: 0 auto 10px auto;\n  cursor: pointer;\n  transition: all .3s ease;\n  display: inline-block;\n  float: none; }\n#modal-media .images-container .image-container:hover {\n  border-color: rgba(82, 188, 211, 0.5); }\n#modal-media .images-container .image-container.active {\n  border-color: rgba(82, 188, 211, 0.5); }\n#modal-media .upload-container {\n  padding: 15px; }\n#modal-media .upload-container .dropzone {\n  position: relative;\n  border: 2px dashed #52bcd3;\n  height: 270px; }\n#modal-media .upload-container .dropzone .dz-message-block {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translateY(-50%) translateX(-50%);\n  transform: translateY(-50%) translateX(-50%); }\n#modal-media .upload-container .dropzone .dz-message-block .dz-message {\n  margin: 0;\n  font-size: 24px;\n  color: #52bcd3;\n  width: 230px; }\n.header .header-block-buttons {\n  text-align: center;\n  margin-left: auto;\n  margin-right: auto;\n  white-space: nowrap; }\n.header .header-block-buttons .btn.header-btn {\n  background-color: transparent;\n  border: 1px solid #64798d;\n  color: #64798d;\n  margin: 0 5px; }\n.header .header-block-buttons .btn.header-btn:focus, .header .header-block-buttons .btn.header-btn:hover {\n  border: 1px solid #3a4651;\n  color: #3a4651; }\n@media (max-width: 767px) {\n  .header .header-block-buttons {\n    display: none; } }\n.header .header-block-collapse {\n  padding-right: 5px; }\n.header .header-block-collapse .collapse-btn {\n  background: 0 0;\n  border: none;\n  box-shadow: none;\n  color: #52bcd3;\n  font-size: 24px;\n  line-height: 40px;\n  border-radius: 0;\n  outline: 0;\n  padding: 0;\n  padding-left: 10px;\n  padding-right: 10px;\n  vertical-align: initial; }\n.header .header-block-nav {\n  margin-left: auto;\n  white-space: nowrap; }\n.header .header-block-nav::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n.header .header-block-nav a {\n  text-decoration: none; }\n.header .header-block-nav ul {\n  margin: 0;\n  padding: 0;\n  list-style: none; }\n.header .header-block-nav > ul {\n  display: table; }\n.header .header-block-nav > ul > li {\n  display: table-cell;\n  position: relative; }\n.header .header-block-nav > ul > li:before {\n  display: block;\n  content: \" \";\n  width: 1px;\n  height: 24px;\n  top: 50%;\n  margin-top: -12px;\n  background-color: #8b9cb1;\n  position: absolute;\n  left: 0; }\n.header .header-block-nav > ul > li:first-child:before {\n  display: none; }\n.header .header-block-nav > ul > li > a {\n  padding: 0 15px;\n  color: #4f5f6f; }\n.header .header-block-nav > ul > li > a:hover {\n  color: #52bcd3; }\n.header .header-block-nav .dropdown-menu {\n  margin-top: 15px; }\n.header .header-block-search {\n  margin-right: auto;\n  padding-left: 30px; }\n@media (max-width: 767px) {\n  .header .header-block-search {\n    padding-left: 10px; } }\n@media (min-width: 768px) and (max-width: 991px) {\n  .header .header-block-search {\n    padding-left: 20px; } }\n@media (min-width: 992px) and (max-width: 1199px) {\n  .header .header-block-search {\n    padding-left: 30px; } }\n@media (min-width: 1200px) {\n  .header .header-block-search {\n    padding-left: 50px; } }\n.header .header-block-search > form {\n  float: right; }\n@media (max-width: 767px) {\n  .header .header-block-search > form {\n    padding-left: 0; } }\n.header .header-block-search .input-container {\n  position: relative;\n  color: #7e8e9f; }\n.header .header-block-search .input-container i {\n  position: absolute;\n  pointer-events: none;\n  display: block;\n  height: 40px;\n  line-height: 40px;\n  left: 0; }\n.header .header-block-search .input-container input {\n  background-color: transparent;\n  border: none;\n  padding-left: 25px;\n  height: 40px;\n  max-width: 150px; }\n@media (max-width: 767px) {\n  .header .header-block-search .input-container input {\n    max-width: 140px; } }\n.header .header-block-search .input-container input:focus + .underline {\n  -webkit-transform: scaleX(1);\n  transform: scaleX(1); }\n.sidebar-header .brand {\n  color: #fff;\n  text-align: left;\n  padding-left: 25px;\n  line-height: 70px;\n  font-size: 16px; }\n@media (max-width: 767px) {\n  .sidebar-header .brand {\n    line-height: 50px;\n    font-size: 16px; } }\n.customize {\n  width: 100%;\n  color: rgba(255, 255, 255, 0.5);\n  padding: 5px 15px;\n  text-align: center; }\n.customize .customize-header {\n  margin-bottom: 10px; }\n#customize-menu {\n  position: fixed;\n  bottom: 0;\n  left: 0;\n  width: 230px; }\n@media (max-width: 991px) {\n  .sidebar-open #customize-menu {\n    left: 0; } }\n@media (max-width: 991px) {\n  #customize-menu {\n    transition: left .3s ease;\n    left: -230px; } }\n#customize-menu > li > a {\n  background-color: #3a4651;\n  border-top: 1px solid rgba(45, 54, 63, 0.5); }\n#customize-menu > li.open > a, #customize-menu > li > a:hover {\n  background-color: #2d363f; }\n#customize-menu .customize {\n  width: 230px;\n  color: rgba(255, 255, 255, 0.5);\n  background-color: #2d363f;\n  text-align: center;\n  padding: 10px 15px;\n  border-top: 2px solid #52bcd3; }\n#customize-menu .customize .customize-item {\n  margin-bottom: 15px; }\n#customize-menu .customize .customize-item .customize-header {\n  margin-bottom: 10px; }\n#customize-menu .customize .customize-item label {\n  font-weight: 400; }\n#customize-menu .customize .customize-item label.title {\n  font-size: 14px; }\n#customize-menu .customize .customize-item .radio + span {\n  padding: 0;\n  padding-left: 5px; }\n#customize-menu .customize .customize-item .radio + span:before {\n  font-size: 17px;\n  color: #546273;\n  cursor: pointer; }\n#customize-menu .customize .customize-item .radio:checked + span:before {\n  color: #52bcd3; }\n#customize-menu .customize .customize-item .customize-colors {\n  list-style: none; }\n#customize-menu .customize .customize-item .customize-colors li {\n  display: inline-block;\n  margin-left: 5px;\n  margin-right: 5px; }\n#customize-menu .customize .customize-item .customize-colors li .color-item {\n  display: block;\n  height: 20px;\n  width: 20px;\n  border: 1px solid;\n  cursor: pointer; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-red {\n  background-color: #fb494d;\n  border-color: #fb494d; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-orange {\n  background-color: #fe7a0e;\n  border-color: #fe7a0e; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-green {\n  background-color: #8cde33;\n  border-color: #8cde33; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-seagreen {\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-blue {\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-purple {\n  background-color: #7867a7;\n  border-color: #7867a7; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.active {\n  position: relative;\n  font-family: FontAwesome;\n  font-size: 17px;\n  line-height: 17px; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.active:before {\n  content: \"\\f00c\";\n  position: absolute;\n  top: 0;\n  left: 0;\n  color: #fff; }\n.header .header-block-nav .notifications {\n  font-size: 16px; }\n.header .header-block-nav .notifications a {\n  padding-right: 10px; }\n.header .header-block-nav .notifications .counter {\n  font-weight: 700;\n  font-size: 14px;\n  position: relative;\n  top: -3px;\n  left: -2px; }\n.header .header-block-nav .notifications.new .counter {\n  color: #52bcd3;\n  font-weight: 700; }\n@media (max-width: 767px) {\n  .header .header-block-nav .notifications {\n    position: static; } }\n.header .header-block-nav .notifications-dropdown-menu {\n  white-space: normal;\n  left: auto;\n  right: 0;\n  min-width: 350px; }\n.header .header-block-nav .notifications-dropdown-menu:before {\n  position: absolute;\n  right: 20px;\n  bottom: 100%;\n  margin-right: -1px; }\n.header .header-block-nav .notifications-dropdown-menu:after {\n  position: absolute;\n  right: 20px;\n  bottom: 100%; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container .notification-item {\n  border-bottom: 1px solid rgba(126, 142, 159, 0.1);\n  padding: 5px; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container .notification-item:hover {\n  background-color: #f5f5f5; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container .img-col {\n  display: table-cell;\n  padding: 5px; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container .body-col {\n  padding: 5px;\n  display: table-cell; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container .img {\n  width: 40px;\n  height: 40px;\n  border-radius: 3px;\n  vertical-align: top;\n  display: inline-block;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container p {\n  color: #4f5f6f;\n  display: inline-block;\n  line-height: 18px;\n  font-size: 13px;\n  margin: 0;\n  vertical-align: top; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container p .accent {\n  font-weight: 700; }\n.header .header-block-nav .notifications-dropdown-menu footer {\n  text-align: center; }\n.header .header-block-nav .notifications-dropdown-menu footer a {\n  color: #373a3c;\n  transition: none; }\n.header .header-block-nav .notifications-dropdown-menu footer a:hover {\n  background-color: #f5f5f5;\n  color: #52bcd3; }\n@media (max-width: 767px) {\n  .header .header-block-nav .notifications-dropdown-menu {\n    min-width: 100px;\n    width: 100%;\n    margin-top: 5px; }\n  .header .header-block-nav .notifications-dropdown-menu:after, .header .header-block-nav .notifications-dropdown-menu:before {\n    right: 107px; } }\n.header .header-block-nav .profile .img {\n  display: inline-block;\n  width: 30px;\n  height: 30px;\n  line-height: 30px;\n  border-radius: 4px;\n  background-color: #8b9cb1;\n  color: #fff;\n  text-align: center;\n  margin-right: 10px;\n  background-repeat: no-repeat;\n  background-position: center;\n  background-size: cover;\n  vertical-align: middle; }\n.header .header-block-nav .profile .name {\n  display: inline-block;\n  margin-right: 9px;\n  font-weight: 700; }\n@media (max-width: 767px) {\n  .header .header-block-nav .profile .name {\n    display: none; } }\n.header .header-block-nav .profile .arrow {\n  color: #52bcd3; }\n.header .header-block-nav .profile-dropdown-menu {\n  left: auto;\n  right: 0;\n  min-width: 180px;\n  white-space: normal; }\n.header .header-block-nav .profile-dropdown-menu:before {\n  position: absolute;\n  right: 10px;\n  bottom: 100%;\n  margin-right: -1px; }\n.header .header-block-nav .profile-dropdown-menu:after {\n  position: absolute;\n  right: 10px;\n  bottom: 100%; }\n.header .header-block-nav .profile-dropdown-menu a {\n  padding: 10px 15px; }\n.header .header-block-nav .profile-dropdown-menu a .icon {\n  color: #52bcd3;\n  text-align: center;\n  width: 16px; }\n.header .header-block-nav .profile-dropdown-menu a span {\n  display: inline-block;\n  padding-left: 5px;\n  text-align: left;\n  color: #7e8e9f; }\n.header .header-block-nav .profile-dropdown-menu .profile-dropdown-menu-icon {\n  padding: 0; }\n.header .header-block-nav .profile-dropdown-menu .profile-dropdown-menu-topic {\n  color: #7e8e9f;\n  padding: 0; }\n.header .header-block-nav .profile-dropdown-menu .dropdown-divider {\n  margin: 0; }\n.header .header-block-nav .profile-dropdown-menu .logout {\n  border-top: 1px solid rgba(126, 142, 159, 0.1); }\n@media (max-width: 767px) {\n  .header .header-block-nav .profile-dropdown-menu {\n    margin-top: 8px; } }\n.form-group.has-error .form-help-text {\n  color: #fb494d;\n  font-size: 13px; }\n.form-group.has-error .form-control {\n  border-color: #fb494d; }\n.form-group.has-success .form-help-text {\n  color: #8cde33;\n  font-size: 13px; }\n.n-progress .progress-bar {\n  float: left;\n  width: 0;\n  height: 100%;\n  font-size: 12px;\n  line-height: 20px;\n  color: #211f33;\n  text-align: center;\n  background-color: #337ab7;\n  box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);\n  transition: width .6s ease; }\n.n-progress .progress-bar.success {\n  background-color: #5cb85c; }\n.n-progress .progress-bar.danger {\n  background-color: #f44; }\n.n-progress .progress-bar.warning {\n  background-color: #f0ad4e; }\n.n-progress {\n  height: 20px;\n  margin-bottom: 20px;\n  overflow: hidden;\n  background-color: #f5f5f5;\n  border-radius: 4px;\n  box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1); }\n.planning-expenses {\n  margin-right: 8px;\n  margin-top: 6px; }\n.text-center {\n  text-align: center; }\na {\n  cursor: pointer; }\n"

/***/ }),

/***/ "./src/app/client/client.component.ts":
/*!********************************************!*\
  !*** ./src/app/client/client.component.ts ***!
  \********************************************/
/*! exports provided: ClientComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientComponent", function() { return ClientComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ClientComponent = /** @class */ (function () {
    // tslint:disable-next-line
    function ClientComponent() {
    }
    // tslint:disable-next-line
    ClientComponent.prototype.ngOnInit = function () {
    };
    ClientComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-client',
            template: __webpack_require__(/*! ./client.component.html */ "./src/app/client/client.component.html"),
            styles: [__webpack_require__(/*! ./client.component.scss */ "./src/app/client/client.component.scss")],
        }),
        __metadata("design:paramtypes", [])
    ], ClientComponent);
    return ClientComponent;
}());



/***/ }),

/***/ "./src/app/client/client.module.ts":
/*!*****************************************!*\
  !*** ./src/app/client/client.module.ts ***!
  \*****************************************/
/*! exports provided: ClientModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientModule", function() { return ClientModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_app_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/app-shared.module */ "./src/app/shared/app-shared.module.ts");
/* harmony import */ var _client_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./client.component */ "./src/app/client/client.component.ts");
/* harmony import */ var _client_routing__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./client.routing */ "./src/app/client/client.routing.ts");
/* harmony import */ var _components_header_header_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/header/header.component */ "./src/app/client/components/header/header.component.ts");
/* harmony import */ var _components_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/sidebar/sidebar.component */ "./src/app/client/components/sidebar/sidebar.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var ClientModule = /** @class */ (function () {
    function ClientModule() {
    }
    ClientModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _client_routing__WEBPACK_IMPORTED_MODULE_4__["ClientRoutingModule"], _shared_app_shared_module__WEBPACK_IMPORTED_MODULE_2__["AppSharedModule"]],
            declarations: [_client_component__WEBPACK_IMPORTED_MODULE_3__["ClientComponent"], _components_header_header_component__WEBPACK_IMPORTED_MODULE_5__["HeaderComponent"], _components_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_6__["SidebarComponent"]],
            providers: [],
        })
    ], ClientModule);
    return ClientModule;
}());



/***/ }),

/***/ "./src/app/client/client.routing.ts":
/*!******************************************!*\
  !*** ./src/app/client/client.routing.ts ***!
  \******************************************/
/*! exports provided: ClientRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientRoutingModule", function() { return ClientRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _client_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./client.component */ "./src/app/client/client.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CLIENT_CHILD_ROUTES = [
    {
        path: 'client',
        component: _client_component__WEBPACK_IMPORTED_MODULE_2__["ClientComponent"],
        children: [],
    },
];
var ClientRoutingModule = /** @class */ (function () {
    function ClientRoutingModule() {
    }
    ClientRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(CLIENT_CHILD_ROUTES)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        })
    ], ClientRoutingModule);
    return ClientRoutingModule;
}());



/***/ }),

/***/ "./src/app/client/components/header/header.component.html":
/*!****************************************************************!*\
  !*** ./src/app/client/components/header/header.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"header\">\n  <div class=\"header-block header-block-search\">\n    12:12:12\n  </div>\n  <div class=\"header-block header-block-nav\">\n    <ul class=\"nav-profile\">\n      <li class=\"profile dropdown\">\n        <a class=\"nav-link dropdown-toggle\" href=\"#\" role=\"button\">\n                                <span class=\"name\">\n                                    Здравствуйте, пользователь\n                                </span>\n        </a>\n        <div class=\"dropdown-menu profile-dropdown-menu\">\n          <a class=\"dropdown-item\" href=\"#\"> <i class=\"fa fa-gear icon\"></i>Сделать запись</a>\n          <div class=\"dropdown-divider\"></div>\n          <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-power-off icon\"></i>Выйти</a>\n        </div>\n      </li>\n    </ul>\n  </div>\n</header>\n"

/***/ }),

/***/ "./src/app/client/components/header/header.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/client/components/header/header.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n@import url(http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,800,700,600);\n@charset \"UTF-8\";\nbody, html {\n  padding: 0;\n  margin: 0;\n  height: 100%;\n  min-height: 100%;\n  font-family: 'Open Sans',sans-serif;\n  color: #4f5f6f;\n  overflow-x: hidden; }\n.main-wrapper {\n  width: 100%;\n  position: absolute;\n  height: 100%;\n  overflow-y: auto;\n  overflow-x: hidden; }\n#ref .color-primary {\n  color: #52bcd3; }\n#ref .chart .color-primary {\n  color: #52bcd3; }\n#ref .chart .color-secondary {\n  color: #7bccdd; }\n.app {\n  position: relative;\n  width: 100%;\n  padding-left: 230px;\n  min-height: 100vh;\n  margin: 0 auto;\n  left: 0;\n  background-color: #f0f3f6;\n  box-shadow: 0 0 3px #ccc;\n  transition: left .3s ease,padding-left .3s ease;\n  overflow: hidden; }\n.app .content {\n  padding: 110px 30px 90px 30px;\n  min-height: 100vh; }\n@media (min-width: 1200px) {\n  .app .content {\n    padding: 120px 50px 100px 50px; } }\n@media (min-width: 992px) and (max-width: 1199px) {\n  .app .content {\n    padding: 110px 30px 90px 30px; } }\n@media (min-width: 768px) and (max-width: 991px) {\n  .app .content {\n    padding: 95px 20px 75px 20px; } }\n@media (max-width: 767px) {\n  .app .content {\n    padding: 65px 10px 65px 10px; } }\n@media (max-width: 991px) {\n  .app {\n    padding-left: 0; } }\n@media (max-width: 991px) {\n  .app.sidebar-open {\n    left: 0; } }\n.app.blank {\n  background-color: #667380; }\n.auth {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  background-color: #667380;\n  overflow-x: hidden;\n  overflow-y: auto; }\n.auth-container {\n  width: 450px;\n  min-height: 330px;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translateY(-50%) translateX(-50%);\n  transform: translateY(-50%) translateX(-50%); }\n.auth-container .auth-header {\n  text-align: center;\n  border-bottom: 1px solid #52bcd3; }\n.auth-container .auth-title {\n  color: #97a4b1;\n  padding: 20px;\n  line-height: 30px;\n  font-size: 26px;\n  font-weight: 600;\n  margin: 0; }\n.auth-container .auth-content {\n  padding: 30px 50px;\n  min-height: 260px; }\n.auth-container .forgot-btn {\n  line-height: 28px; }\n.auth-container .checkbox label {\n  padding: 0; }\n.auth-container .checkbox a {\n  vertical-align: text-top; }\n.auth-container .checkbox span {\n  color: #4f5f6f; }\n@media (max-width: 767px) {\n  .auth-container {\n    width: 100%;\n    position: relative;\n    left: 0;\n    top: 0;\n    -webkit-transform: inherit;\n    transform: inherit;\n    margin: 0;\n    margin-bottom: 10px; }\n  .auth-container .auth-content {\n    padding: 30px 25px; } }\n.error-card {\n  width: 410px;\n  min-height: 330px;\n  margin: 60px auto; }\n.error-card .error-title {\n  font-size: 150px;\n  line-height: 150px;\n  font-weight: 700;\n  color: #252932;\n  text-align: center;\n  text-shadow: rgba(61, 61, 61, 0.3) 0.5px 0.5px, rgba(61, 61, 61, 0.2) 1px 1px, rgba(61, 61, 61, 0.3) 1.5px 1.5px; }\n.error-card .error-sub-title {\n  font-weight: 100;\n  text-align: center; }\n.error-card .error-container {\n  text-align: center;\n  visibility: hidden; }\n.error-card .error-container.visible {\n  visibility: visible; }\n.error-card.global {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translateY(-50%) translateX(-50%);\n  transform: translateY(-50%) translateX(-50%);\n  margin: 0; }\n.error-card.global .error-title {\n  color: #fff; }\n.error-card.global .error-container, .error-card.global .error-sub-title {\n  color: #fff; }\n@media (min-width: 768px) and (max-width: 991px) {\n  .error-card {\n    width: 50%; }\n  .error-card.global {\n    position: relative;\n    top: 25%;\n    left: 0;\n    -webkit-transform: inherit;\n    transform: inherit;\n    margin: 40px auto; } }\n@media (max-width: 767px) {\n  .error-card {\n    width: 90%; }\n  .error-card.global {\n    position: relative;\n    top: 25%;\n    left: 0;\n    -webkit-transform: inherit;\n    transform: inherit;\n    margin: 40px auto; } }\n.alert {\n  background-image: none; }\n.alert.alert-primary {\n  background-color: #52bcd3;\n  border-color: #52bcd3;\n  color: #fff; }\n.alert.alert-primary hr {\n  border-top-color: #3eb4ce; }\n.alert.alert-primary .alert-link {\n  color: #e6e6e6; }\n.alert.alert-success {\n  background-color: #4bcf99;\n  border-color: #4bcf99;\n  color: #fff; }\n.alert.alert-success hr {\n  border-top-color: #37ca8e; }\n.alert.alert-success .alert-link {\n  color: #e6e6e6; }\n.alert.alert-info {\n  background-color: #76d4f5;\n  border-color: #76d4f5;\n  color: #fff; }\n.alert.alert-info hr {\n  border-top-color: #5ecdf3; }\n.alert.alert-info .alert-link {\n  color: #e6e6e6; }\n.alert.alert-warning {\n  background-color: #fe974b;\n  border-color: #fe974b;\n  color: #fff; }\n.alert.alert-warning hr {\n  border-top-color: #fe8832; }\n.alert.alert-warning .alert-link {\n  color: #e6e6e6; }\n.alert.alert-danger {\n  background-color: #f44;\n  border-color: #f44;\n  color: #fff; }\n.alert.alert-danger hr {\n  border-top-color: #ff2b2b; }\n.alert.alert-danger .alert-link {\n  color: #e6e6e6; }\n.alert.alert-inverse {\n  background-color: #131e26;\n  border-color: #131e26;\n  color: #fff; }\n.alert.alert-inverse hr {\n  border-top-color: #0b1115; }\n.alert.alert-inverse .alert-link {\n  color: #e6e6e6; }\n.animated {\n  -webkit-animation-duration: .5s;\n  animation-duration: .5s;\n  -webkit-animation-delay: .1s;\n  animation-delay: .1s; }\n.btn {\n  background-image: none;\n  border-radius: 0;\n  margin-bottom: 5px; }\n.btn.btn-primary {\n  color: #fff;\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.btn.btn-primary:hover {\n  color: #fff;\n  background-color: #31a7c1;\n  border-color: #2fa0b9; }\n.btn.btn-primary.focus, .btn.btn-primary:focus {\n  color: #fff;\n  background-color: #31a7c1;\n  border-color: #2fa0b9; }\n.btn.btn-primary.active, .btn.btn-primary:active, .open > .btn.btn-primary.dropdown-toggle {\n  color: #fff;\n  background-color: #31a7c1;\n  border-color: #2fa0b9;\n  background-image: none; }\n.btn.btn-primary.active.focus, .btn.btn-primary.active:focus, .btn.btn-primary.active:hover, .btn.btn-primary:active.focus, .btn.btn-primary:active:focus, .btn.btn-primary:active:hover, .open > .btn.btn-primary.dropdown-toggle.focus, .open > .btn.btn-primary.dropdown-toggle:focus, .open > .btn.btn-primary.dropdown-toggle:hover {\n  color: #fff;\n  background-color: #2a8fa4;\n  border-color: #227284; }\n.btn.btn-primary.disabled.focus, .btn.btn-primary.disabled:focus, .btn.btn-primary:disabled.focus, .btn.btn-primary:disabled:focus {\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.btn.btn-primary.disabled:hover, .btn.btn-primary:disabled:hover {\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.btn.btn-secondary {\n  color: #4f5f6f;\n  background-color: #fff;\n  border-color: #d7dde4; }\n.btn.btn-secondary:hover {\n  color: #4f5f6f;\n  background-color: #e6e6e6;\n  border-color: #b2becb; }\n.btn.btn-secondary.focus, .btn.btn-secondary:focus {\n  color: #4f5f6f;\n  background-color: #e6e6e6;\n  border-color: #b2becb; }\n.btn.btn-secondary.active, .btn.btn-secondary:active, .open > .btn.btn-secondary.dropdown-toggle {\n  color: #4f5f6f;\n  background-color: #e6e6e6;\n  border-color: #b2becb;\n  background-image: none; }\n.btn.btn-secondary.active.focus, .btn.btn-secondary.active:focus, .btn.btn-secondary.active:hover, .btn.btn-secondary:active.focus, .btn.btn-secondary:active:focus, .btn.btn-secondary:active:hover, .open > .btn.btn-secondary.dropdown-toggle.focus, .open > .btn.btn-secondary.dropdown-toggle:focus, .open > .btn.btn-secondary.dropdown-toggle:hover {\n  color: #4f5f6f;\n  background-color: #d4d4d4;\n  border-color: #8b9cb1; }\n.btn.btn-secondary.disabled.focus, .btn.btn-secondary.disabled:focus, .btn.btn-secondary:disabled.focus, .btn.btn-secondary:disabled:focus {\n  background-color: #fff;\n  border-color: #d7dde4; }\n.btn.btn-secondary.disabled:hover, .btn.btn-secondary:disabled:hover {\n  background-color: #fff;\n  border-color: #d7dde4; }\n.btn.btn-success {\n  color: #fff;\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n.btn.btn-success:hover {\n  color: #fff;\n  background-color: #31b680;\n  border-color: #2eae7a; }\n.btn.btn-success.focus, .btn.btn-success:focus {\n  color: #fff;\n  background-color: #31b680;\n  border-color: #2eae7a; }\n.btn.btn-success.active, .btn.btn-success:active, .open > .btn.btn-success.dropdown-toggle {\n  color: #fff;\n  background-color: #31b680;\n  border-color: #2eae7a;\n  background-image: none; }\n.btn.btn-success.active.focus, .btn.btn-success.active:focus, .btn.btn-success.active:hover, .btn.btn-success:active.focus, .btn.btn-success:active:focus, .btn.btn-success:active:hover, .open > .btn.btn-success.dropdown-toggle.focus, .open > .btn.btn-success.dropdown-toggle:focus, .open > .btn.btn-success.dropdown-toggle:hover {\n  color: #fff;\n  background-color: #299a6c;\n  border-color: #217a55; }\n.btn.btn-success.disabled.focus, .btn.btn-success.disabled:focus, .btn.btn-success:disabled.focus, .btn.btn-success:disabled:focus {\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n.btn.btn-success.disabled:hover, .btn.btn-success:disabled:hover {\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n.btn.btn-info {\n  color: #fff;\n  background-color: #76d4f5;\n  border-color: #76d4f5; }\n.btn.btn-info:hover {\n  color: #fff;\n  background-color: #46c5f2;\n  border-color: #3dc2f1; }\n.btn.btn-info.focus, .btn.btn-info:focus {\n  color: #fff;\n  background-color: #46c5f2;\n  border-color: #3dc2f1; }\n.btn.btn-info.active, .btn.btn-info:active, .open > .btn.btn-info.dropdown-toggle {\n  color: #fff;\n  background-color: #46c5f2;\n  border-color: #3dc2f1;\n  background-image: none; }\n.btn.btn-info.active.focus, .btn.btn-info.active:focus, .btn.btn-info.active:hover, .btn.btn-info:active.focus, .btn.btn-info:active:focus, .btn.btn-info:active:hover, .open > .btn.btn-info.dropdown-toggle.focus, .open > .btn.btn-info.dropdown-toggle:focus, .open > .btn.btn-info.dropdown-toggle:hover {\n  color: #fff;\n  background-color: #25bbef;\n  border-color: #10a7db; }\n.btn.btn-info.disabled.focus, .btn.btn-info.disabled:focus, .btn.btn-info:disabled.focus, .btn.btn-info:disabled:focus {\n  background-color: #76d4f5;\n  border-color: #76d4f5; }\n.btn.btn-info.disabled:hover, .btn.btn-info:disabled:hover {\n  background-color: #76d4f5;\n  border-color: #76d4f5; }\n.btn.btn-warning {\n  color: #fff;\n  background-color: #fe974b;\n  border-color: #fe974b; }\n.btn.btn-warning:hover {\n  color: #fff;\n  background-color: #fe7a18;\n  border-color: #fe740e; }\n.btn.btn-warning.focus, .btn.btn-warning:focus {\n  color: #fff;\n  background-color: #fe7a18;\n  border-color: #fe740e; }\n.btn.btn-warning.active, .btn.btn-warning:active, .open > .btn.btn-warning.dropdown-toggle {\n  color: #fff;\n  background-color: #fe7a18;\n  border-color: #fe740e;\n  background-image: none; }\n.btn.btn-warning.active.focus, .btn.btn-warning.active:focus, .btn.btn-warning.active:hover, .btn.btn-warning:active.focus, .btn.btn-warning:active:focus, .btn.btn-warning:active:hover, .open > .btn.btn-warning.dropdown-toggle.focus, .open > .btn.btn-warning.dropdown-toggle:focus, .open > .btn.btn-warning.dropdown-toggle:hover {\n  color: #fff;\n  background-color: #f16701;\n  border-color: #c85601; }\n.btn.btn-warning.disabled.focus, .btn.btn-warning.disabled:focus, .btn.btn-warning:disabled.focus, .btn.btn-warning:disabled:focus {\n  background-color: #fe974b;\n  border-color: #fe974b; }\n.btn.btn-warning.disabled:hover, .btn.btn-warning:disabled:hover {\n  background-color: #fe974b;\n  border-color: #fe974b; }\n.btn.btn-danger {\n  color: #fff;\n  background-color: #f44;\n  border-color: #f44; }\n.btn.btn-danger:hover {\n  color: #fff;\n  background-color: #f11;\n  border-color: #ff0707; }\n.btn.btn-danger.focus, .btn.btn-danger:focus {\n  color: #fff;\n  background-color: #f11;\n  border-color: #ff0707; }\n.btn.btn-danger.active, .btn.btn-danger:active, .open > .btn.btn-danger.dropdown-toggle {\n  color: #fff;\n  background-color: #f11;\n  border-color: #ff0707;\n  background-image: none; }\n.btn.btn-danger.active.focus, .btn.btn-danger.active:focus, .btn.btn-danger.active:hover, .btn.btn-danger:active.focus, .btn.btn-danger:active:focus, .btn.btn-danger:active:hover, .open > .btn.btn-danger.dropdown-toggle.focus, .open > .btn.btn-danger.dropdown-toggle:focus, .open > .btn.btn-danger.dropdown-toggle:hover {\n  color: #fff;\n  background-color: #ec0000;\n  border-color: #c40000; }\n.btn.btn-danger.disabled.focus, .btn.btn-danger.disabled:focus, .btn.btn-danger:disabled.focus, .btn.btn-danger:disabled:focus {\n  background-color: #f44;\n  border-color: #f44; }\n.btn.btn-danger.disabled:hover, .btn.btn-danger:disabled:hover {\n  background-color: #f44;\n  border-color: #f44; }\n.btn.btn-primary-outline {\n  color: #52bcd3;\n  background-image: none;\n  background-color: transparent;\n  border-color: #52bcd3; }\n.btn.btn-primary-outline.active, .btn.btn-primary-outline.focus, .btn.btn-primary-outline:active, .btn.btn-primary-outline:focus, .open > .btn.btn-primary-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.btn.btn-primary-outline:hover {\n  color: #fff;\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.btn.btn-primary-outline.disabled.focus, .btn.btn-primary-outline.disabled:focus, .btn.btn-primary-outline:disabled.focus, .btn.btn-primary-outline:disabled:focus {\n  border-color: #a3dbe8; }\n.btn.btn-primary-outline.disabled:hover, .btn.btn-primary-outline:disabled:hover {\n  border-color: #a3dbe8; }\n.btn.btn-secondary-outline {\n  color: #d7dde4;\n  background-image: none;\n  background-color: transparent;\n  border-color: #d7dde4; }\n.btn.btn-secondary-outline.active, .btn.btn-secondary-outline.focus, .btn.btn-secondary-outline:active, .btn.btn-secondary-outline:focus, .open > .btn.btn-secondary-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #d7dde4;\n  border-color: #d7dde4; }\n.btn.btn-secondary-outline:hover {\n  color: #fff;\n  background-color: #d7dde4;\n  border-color: #d7dde4; }\n.btn.btn-secondary-outline.disabled.focus, .btn.btn-secondary-outline.disabled:focus, .btn.btn-secondary-outline:disabled.focus, .btn.btn-secondary-outline:disabled:focus {\n  border-color: #fff; }\n.btn.btn-secondary-outline.disabled:hover, .btn.btn-secondary-outline:disabled:hover {\n  border-color: #fff; }\n.btn.btn-info-outline {\n  color: #76d4f5;\n  background-image: none;\n  background-color: transparent;\n  border-color: #76d4f5; }\n.btn.btn-info-outline.active, .btn.btn-info-outline.focus, .btn.btn-info-outline:active, .btn.btn-info-outline:focus, .open > .btn.btn-info-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #76d4f5;\n  border-color: #76d4f5; }\n.btn.btn-info-outline:hover {\n  color: #fff;\n  background-color: #76d4f5;\n  border-color: #76d4f5; }\n.btn.btn-info-outline.disabled.focus, .btn.btn-info-outline.disabled:focus, .btn.btn-info-outline:disabled.focus, .btn.btn-info-outline:disabled:focus {\n  border-color: #d5f2fc; }\n.btn.btn-info-outline.disabled:hover, .btn.btn-info-outline:disabled:hover {\n  border-color: #d5f2fc; }\n.btn.btn-success-outline {\n  color: #4bcf99;\n  background-image: none;\n  background-color: transparent;\n  border-color: #4bcf99; }\n.btn.btn-success-outline.active, .btn.btn-success-outline.focus, .btn.btn-success-outline:active, .btn.btn-success-outline:focus, .open > .btn.btn-success-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n.btn.btn-success-outline:hover {\n  color: #fff;\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n.btn.btn-success-outline.disabled.focus, .btn.btn-success-outline.disabled:focus, .btn.btn-success-outline:disabled.focus, .btn.btn-success-outline:disabled:focus {\n  border-color: #9ce4c7; }\n.btn.btn-success-outline.disabled:hover, .btn.btn-success-outline:disabled:hover {\n  border-color: #9ce4c7; }\n.btn.btn-warning-outline {\n  color: #fe974b;\n  background-image: none;\n  background-color: transparent;\n  border-color: #fe974b; }\n.btn.btn-warning-outline.active, .btn.btn-warning-outline.focus, .btn.btn-warning-outline:active, .btn.btn-warning-outline:focus, .open > .btn.btn-warning-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #fe974b;\n  border-color: #fe974b; }\n.btn.btn-warning-outline:hover {\n  color: #fff;\n  background-color: #fe974b;\n  border-color: #fe974b; }\n.btn.btn-warning-outline.disabled.focus, .btn.btn-warning-outline.disabled:focus, .btn.btn-warning-outline:disabled.focus, .btn.btn-warning-outline:disabled:focus {\n  border-color: #ffd2b0; }\n.btn.btn-warning-outline.disabled:hover, .btn.btn-warning-outline:disabled:hover {\n  border-color: #ffd2b0; }\n.btn.btn-danger-outline {\n  color: #f44;\n  background-image: none;\n  background-color: transparent;\n  border-color: #f44; }\n.btn.btn-danger-outline.active, .btn.btn-danger-outline.focus, .btn.btn-danger-outline:active, .btn.btn-danger-outline:focus, .open > .btn.btn-danger-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #f44;\n  border-color: #f44; }\n.btn.btn-danger-outline:hover {\n  color: #fff;\n  background-color: #f44;\n  border-color: #f44; }\n.btn.btn-danger-outline.disabled.focus, .btn.btn-danger-outline.disabled:focus, .btn.btn-danger-outline:disabled.focus, .btn.btn-danger-outline:disabled:focus {\n  border-color: #faa; }\n.btn.btn-danger-outline.disabled:hover, .btn.btn-danger-outline:disabled:hover {\n  border-color: #faa; }\n.btn.btn-oval:focus, .btn.btn-pill-left:focus, .btn.btn-pill-right:focus {\n  outline: 0;\n  outline-offset: initial; }\n.btn.btn-pill-left {\n  border-top-left-radius: 25px;\n  border-bottom-left-radius: 25px; }\n.btn.btn-pill-right {\n  border-top-right-radius: 25px;\n  border-bottom-right-radius: 25px; }\n.btn.btn-oval {\n  border-radius: 25px; }\n.btn.btn-link {\n  text-decoration: none; }\n.btn strong {\n  font-weight: 600; }\n.btn-group .dropdown-menu > li:last-child a:hover:before {\n  height: 0;\n  -webkit-transform: scaleX(0);\n  transform: scaleX(0); }\n.card {\n  background-color: #fff;\n  box-shadow: 1px 1px 5px rgba(126, 142, 159, 0.1);\n  margin-bottom: 10px;\n  border-radius: 0;\n  border: none; }\n.card .card {\n  box-shadow: none; }\n.card .card-header {\n  background-image: none;\n  background-color: #fff;\n  align-items: center;\n  display: flex;\n  flex-direction: row;\n  padding: 0;\n  border-radius: 0;\n  min-height: 50px;\n  border: none; }\n.card .card-header::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n.card .card-header.bordered {\n  border-bottom: 1px solid #d7dde4; }\n.card .card-header.card-header-sm {\n  min-height: 40px; }\n.card .card-header > span {\n  vertical-align: middle; }\n.card .card-header .pull-right {\n  margin-left: auto; }\n.card .card-header .header-block {\n  padding: .5rem 15px; }\n@media (min-width: 1200px) {\n  .card .card-header .header-block {\n    padding: .5rem 20px; } }\n@media (max-width: 767px) {\n  .card .card-header .header-block {\n    padding: .5rem 10px; } }\n.card .card-header .title {\n  color: #4f5f6f;\n  display: inline-flex; }\n.card .card-header .btn {\n  margin: 0; }\n.card .card-header .nav-tabs {\n  border-color: transparent;\n  align-self: stretch;\n  display: flex;\n  position: relative;\n  top: 1px; }\n.card .card-header .nav-tabs .nav-item {\n  margin-left: 0;\n  display: flex;\n  align-self: stretch; }\n.card .card-header .nav-tabs .nav-item .nav-link {\n  display: flex;\n  align-self: stretch;\n  align-items: center;\n  color: #4f5f6f;\n  opacity: .7;\n  padding-left: 10px;\n  padding-right: 10px;\n  border-radius: 0;\n  font-size: 14px;\n  border-top-width: 2px;\n  border-bottom: 1px solid #d7dde4;\n  text-decoration: none; }\n.card .card-header .nav-tabs .nav-item .nav-link.active {\n  border-top-color: #52bcd3;\n  border-bottom-color: transparent;\n  opacity: 1; }\n.card .card-header .nav-tabs .nav-item .nav-link.active:focus, .card .card-header .nav-tabs .nav-item .nav-link.active:hover {\n  opacity: 1;\n  background-color: #fff;\n  border-color: #d7dde4 #d7dde4 transparent;\n  border-top-color: #52bcd3; }\n.card .card-header .nav-tabs .nav-item .nav-link:focus, .card .card-header .nav-tabs .nav-item .nav-link:hover {\n  opacity: 1;\n  background-color: transparent;\n  border-color: transparent; }\n.card.card-default > .card-header {\n  background-color: #fff;\n  color: inherit; }\n.card.card-primary {\n  border-color: #52bcd3; }\n.card.card-primary > .card-header {\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.card.card-success > .card-header {\n  background-color: #4bcf99; }\n.card.card-info > .card-header {\n  background-color: #76d4f5; }\n.card.card-warning > .card-header {\n  background-color: #fe974b; }\n.card.card-danger > .card-header {\n  background-color: #f44; }\n.card.card-inverse > .card-header {\n  background-color: #131e26; }\n.card .card-title-block, .card .title-block {\n  padding-bottom: 0;\n  margin-bottom: 20px;\n  border: none; }\n.card .card-title-block::after, .card .title-block::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n.card .section {\n  margin-bottom: 20px; }\n.card .example, .card .section.demo {\n  margin-bottom: 20px; }\n.card-block {\n  padding: 15px; }\n.card-block .tab-content {\n  padding: 0;\n  border-color: transparent; }\n@media (min-width: 1200px) {\n  .card-block {\n    padding: 20px; } }\n@media (max-width: 767px) {\n  .card-block {\n    padding: 10px; } }\n.card-footer {\n  background-color: #fafafa; }\n.easy-pie-chart {\n  width: 50px;\n  height: 50px;\n  display: inline-block;\n  background-color: #d7dde4;\n  border-radius: 5px; }\n.dropdown-menu {\n  float: left;\n  box-shadow: 2px 3px 6px rgba(126, 142, 159, 0.1);\n  border: 1px solid rgba(126, 142, 159, 0.1);\n  border-top-left-radius: 0;\n  border-top-right-radius: 0; }\n.dropdown-menu .dropdown-item {\n  display: block;\n  padding: 0 15px;\n  clear: both;\n  font-weight: 400;\n  color: #4f5f6f;\n  white-space: nowrap;\n  transition: none; }\n.dropdown-menu .dropdown-item i {\n  margin-right: 2px; }\n.dropdown-menu .dropdown-item:hover {\n  color: #52bcd3 !important;\n  background: 0 0;\n  background-color: #f5f5f5; }\n.flex-row {\n  display: flex;\n  flex-direction: row; }\n.flex-col {\n  display: flex;\n  flex-direction: column; }\n.centralize-y {\n  display: flex;\n  align-items: center; }\ninput, textarea {\n  outline: 0; }\n.form-group .row {\n  margin-left: -10px;\n  margin-right: -10px; }\n.form-group .row [class^=col] {\n  padding-left: 10px;\n  padding-right: 10px; }\n.form-group.has-error span.has-error {\n  color: #f44;\n  font-size: 13px;\n  display: block !important; }\n.form-group.has-error .form-control-feedback {\n  color: #f44; }\n.form-group.has-warning span.has-warning {\n  color: #fe974b;\n  font-size: 13px;\n  display: block !important; }\n.form-group.has-warning .form-control-feedback {\n  color: #fe974b; }\n.form-group.has-success span.has-success {\n  color: #4bcf99;\n  font-size: 13px;\n  display: block !important; }\n.form-group.has-success .form-control-feedback {\n  color: #4bcf99; }\n.input-group {\n  margin-bottom: 10px; }\n.input-group .form-control {\n  padding-left: 5px; }\n.input-group span.input-group-addon {\n  font-style: italic;\n  border: none;\n  border-radius: 0;\n  border: none;\n  background-color: #d7dde4;\n  transition: background-color ease-in-out 15s,color ease-in-out .15s; }\n.input-group span.input-group-addon.focus {\n  background-color: #52bcd3;\n  color: #fff; }\n.control-label, label {\n  font-weight: 600; }\n.form-control.underlined {\n  padding-left: 0;\n  padding-right: 0;\n  border-radius: 0;\n  border: none;\n  border-radius: 0;\n  box-shadow: none;\n  border-bottom: 1px solid #d7dde4; }\n.form-control.underlined.indented {\n  padding: .375rem .75rem; }\n.form-control.underlined:focus, .has-error .form-control.underlined:focus, .has-success .form-control.underlined:focus, .has-warning .form-control.underlined:focus {\n  border: none;\n  box-shadow: none;\n  border-bottom: 1px solid #52bcd3; }\n.has-error .form-control.underlined {\n  box-shadow: none;\n  border-color: #f44; }\n.has-warning .form-control.underlined {\n  box-shadow: none;\n  border-color: #fe974b; }\n.has-success .form-control.underlined {\n  box-shadow: none;\n  border-color: #4bcf99; }\n.form-control.boxed {\n  border-radius: 0;\n  box-shadow: none; }\n.form-control.boxed:focus {\n  border: 1px solid #52bcd3; }\n.checkbox, .radio {\n  display: none; }\n.checkbox + span, .radio + span {\n  padding: 0;\n  padding-right: 10px; }\n.checkbox + span:before, .radio + span:before {\n  font-family: FontAwesome;\n  font-size: 21px;\n  display: inline-block;\n  vertical-align: middle;\n  letter-spacing: 10px;\n  color: #c8d0da; }\n.checkbox:checked + span:before, .radio:checked + span:before {\n  color: #52bcd3; }\n.checkbox:disabled + span:before, .radio:disabled + span:before {\n  opacity: .5;\n  cursor: not-allowed; }\n.checkbox:checked:disabled + span:before, .radio:checked:disabled + span:before {\n  color: #c8d0da; }\n.checkbox + span:before {\n  content: \"\\f0c8\"; }\n.checkbox:checked + span:before {\n  content: \"\\f14a\"; }\n.checkbox.rounded + span:before {\n  content: \"\\f111\"; }\n.checkbox.rounded:checked + span:before {\n  content: \"\\f058\"; }\n.radio + span:before {\n  content: \"\\f111\"; }\n.radio:checked + span:before {\n  content: \"\\f192\"; }\n.radio.squared + span:before {\n  content: \"\\f0c8\"; }\n.radio.squared:checked + span:before {\n  content: \"\\f14a\"; }\n.form-control::-webkit-input-placeholder {\n  font-style: italic;\n  color: #c8d0da; }\n.form-control:-moz-placeholder {\n  font-style: italic;\n  color: #d7dde4; }\n.form-control::-moz-placeholder {\n  font-style: italic;\n  color: #d7dde4; }\n.form-control:-ms-input-placeholder {\n  font-style: italic;\n  color: #d7dde4; }\n.images-container::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n.images-container .image-container {\n  float: left;\n  padding: 3px;\n  margin-right: 10px;\n  margin-bottom: 35px;\n  position: relative;\n  border: 1px solid #e6eaee;\n  overflow: hidden; }\n.images-container .image-container.active {\n  border-color: #52bcd3; }\n.images-container .image-container:hover .controls {\n  bottom: 0;\n  opacity: 1; }\n.images-container .controls {\n  position: absolute;\n  left: 0;\n  right: 0;\n  opacity: 0;\n  bottom: -35px;\n  text-align: center;\n  height: 35px;\n  font-size: 24px;\n  transition: bottom .2s ease,opacity .2s ease;\n  background-color: #fff; }\n.images-container .controls::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n.images-container .controls .control-btn {\n  display: inline-block;\n  color: #4f5f6f;\n  cursor: pointer;\n  width: 35px;\n  height: 35px;\n  line-height: 35px;\n  text-align: center;\n  opacity: .5;\n  transition: opacity .3s ease; }\n.images-container .controls .control-btn:hover {\n  opacity: 1; }\n.images-container .controls .control-btn.move {\n  cursor: move; }\n.images-container .controls .control-btn.star {\n  color: #ffb300; }\n.images-container .controls .control-btn.star i:before {\n  content: \"\\f006\"; }\n.images-container .controls .control-btn.star.active i:before {\n  content: \"\\f005\"; }\n.images-container .controls .control-btn.remove {\n  color: #f44; }\n.images-container .image {\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n  width: 130px;\n  height: 135px;\n  line-height: 135px;\n  text-align: center; }\n.images-container .image-container.main {\n  border-color: #ffb300; }\n.images-container .image-container.new {\n  opacity: .6;\n  transition: opacity .3s ease;\n  border-style: dashed;\n  border: 1px #52bcd3 solid;\n  color: #52bcd3; }\n.images-container .image-container.new .image {\n  font-size: 2.5rem; }\n.images-container .image-container.new:hover {\n  opacity: 1; }\n.item-list {\n  list-style: none;\n  padding: 0;\n  margin: 0;\n  margin-bottom: 0;\n  line-height: 1.4rem;\n  display: flex;\n  flex-flow: column nowrap; }\n@media (min-width: 992px) and (max-width: 1199px) {\n  .item-list {\n    font-size: 1rem; } }\n@media (min-width: 768px) and (max-width: 991px) {\n  .item-list {\n    font-size: .95rem; } }\n@media (max-width: 767px) {\n  .item-list {\n    font-size: 1.05rem; } }\n.item-list.striped > li {\n  border-bottom: 1px solid #e9edf0; }\n.item-list.striped > li:nth-child(2n+1) {\n  background-color: #fcfcfd; }\n@media (max-width: 767px) {\n  .item-list.striped > li:nth-child(2n+1) {\n    background-color: #f8f9fb; } }\n.item-list.striped .item-list-footer {\n  border-bottom: none; }\n.item-list .item {\n  display: flex;\n  flex-direction: column; }\n.item-list .item-row {\n  display: flex;\n  align-items: stretch;\n  flex-direction: row;\n  justify-content: space-between;\n  flex-wrap: wrap;\n  min-width: 100%; }\n.item-list .item-row.nowrap {\n  flex-wrap: nowrap; }\n.item-list .item-col {\n  align-items: center;\n  display: flex;\n  padding: 10px 10px 10px 0;\n  flex-basis: 0;\n  flex-grow: 3;\n  flex-shrink: 3;\n  margin-left: auto;\n  margin-right: auto;\n  min-width: 0; }\n.item-list .item-col.fixed {\n  flex-grow: 0;\n  flex-shrink: 0;\n  flex-basis: auto; }\n.item-list .item-col.pull-left {\n  margin-right: auto; }\n.item-list .item-col.pull-right {\n  margin-left: auto; }\n.item-list .item-col > div {\n  width: 100%; }\n.item-list .item-col:last-child {\n  padding-right: 0; }\n.item-list .no-overflow {\n  overflow: hidden; }\n.item-list .no-wrap {\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap; }\n.item-list .item-list-header .item-col.item-col-header span {\n  color: #999;\n  font-size: .8rem;\n  font-weight: 700 !important; }\n.item-list .item-heading {\n  font-size: .9rem;\n  display: none;\n  color: #666;\n  font-weight: 700;\n  padding-right: 10px; }\n@media (max-width: 767px) {\n  .item-list .item-heading {\n    display: block; } }\n@media (min-width: 544px) and (max-width: 767px) {\n  .item-list .item-heading {\n    width: 100%; } }\n@media (max-width: 543px) {\n  .item-list .item-heading {\n    width: 40%; } }\n.item-list .item-col.item-col-check {\n  flex-basis: 30px; }\n@media (max-width: 767px) {\n  .item-list .item-col.item-col-check {\n    order: -8; } }\n.item-list .item-check {\n  margin-bottom: 0; }\n.item-list .item-check .checkbox + span {\n  padding-right: 0; }\n.item-list .item-check .checkbox + span:before {\n  width: 20px; }\n.item-list .item-col.item-col-img {\n  display: flex;\n  flex-basis: 70px; }\n.item-list .item-col.item-col-img.xs {\n  flex-basis: 40px; }\n.item-list .item-col.item-col-img.sm {\n  flex-basis: 50px; }\n.item-list .item-col.item-col-img.lg {\n  flex-basis: 100px; }\n.item-list .item-col.item-col-img.xl {\n  flex-basis: 120px; }\n.item-list .item-col.item-col-img a {\n  width: 100%; }\n.item-list .item-img {\n  flex-grow: 1;\n  -ms-grid-row-align: stretch;\n      align-self: stretch;\n  background-color: #efefef;\n  padding-bottom: 100%;\n  width: 100%;\n  height: 0;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat; }\n@media (max-width: 767px) {\n  .item-list .item-col.item-col-title {\n    order: -4; } }\n.item-list .item-col.item-col-title a {\n  display: block; }\n.item-list .item-title {\n  margin: 0;\n  font-size: inherit;\n  line-height: inherit;\n  font-weight: 600; }\n.item-list .item-stats {\n  height: 1.4rem; }\n.item-list .item-col.item-col-actions-dropdown {\n  flex-basis: 40px;\n  text-align: center;\n  padding-left: 0 !important; }\n@media (max-width: 767px) {\n  .item-list .item-col.item-col-actions-dropdown {\n    order: -3;\n    flex-basis: 40px !important;\n    padding-right: 10px; } }\n.item-list .item-actions-dropdown {\n  position: relative;\n  font-size: 1.1rem; }\n.item-list .item-actions-dropdown.active .item-actions-block {\n  max-width: 120px; }\n.item-list .item-actions-dropdown.active .item-actions-toggle-btn {\n  color: #52bcd3; }\n.item-list .item-actions-dropdown.active .item-actions-toggle-btn .active {\n  display: block; }\n.item-list .item-actions-dropdown.active .item-actions-toggle-btn .inactive {\n  display: none; }\n.item-list .item-actions-dropdown .item-actions-toggle-btn {\n  color: #9ba8b5;\n  font-size: 1.2rem;\n  cursor: pointer;\n  width: 100%;\n  line-height: 30px;\n  text-align: center;\n  text-decoration: none; }\n.item-list .item-actions-dropdown .item-actions-toggle-btn .active {\n  display: none; }\n.item-list .item-actions-dropdown .item-actions-block {\n  height: 30px;\n  max-width: 0;\n  line-height: 30px;\n  overflow: hidden;\n  position: absolute;\n  top: 0;\n  right: 100%;\n  background-color: #d7dde4;\n  border-radius: 3px;\n  transition: all .15s ease-in-out; }\n.item-list .item-actions-dropdown .item-actions-block.direction-right {\n  right: auto;\n  left: 100%; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list {\n  padding: 0;\n  list-style: none;\n  white-space: nowrap;\n  padding: 0 5px; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list li {\n  display: inline-block;\n  padding: 0; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list a {\n  display: block;\n  padding: 0 5px; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list a.edit {\n  color: #38424c; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list a.check {\n  color: #40b726; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list a.remove {\n  color: #db0e1e; }\n.card > .item-list .item > .item-row {\n  padding: 0 15px; }\n@media (min-width: 1200px) {\n  .card > .item-list .item > .item-row {\n    padding: 0 20px; } }\n@media (max-width: 767px) {\n  .card > .item-list .item > .item-row {\n    padding: 0 10px; } }\n.logo {\n  display: inline-block;\n  width: 45px;\n  height: 25px;\n  vertical-align: middle;\n  margin-right: 5px;\n  position: relative; }\n.logo .l {\n  width: 11px;\n  height: 11px;\n  border-radius: 50%;\n  background-color: #52bcd3;\n  position: absolute; }\n.logo .l.l1 {\n  bottom: 0;\n  left: 0; }\n.logo .l.l2 {\n  width: 7px;\n  height: 7px;\n  bottom: 13px;\n  left: 10px; }\n.logo .l.l3 {\n  width: 7px;\n  height: 7px;\n  bottom: 4px;\n  left: 17px; }\n.logo .l.l4 {\n  bottom: 13px;\n  left: 25px; }\n.logo .l.l5 {\n  bottom: 0;\n  left: 34px; }\n.modal-body.modal-tab-container {\n  padding: 0; }\n.modal-body.modal-tab-container .modal-tabs {\n  padding-left: 0;\n  margin-bottom: 0;\n  list-style: none;\n  background-color: #fff;\n  border-bottom: 1px solid #ddd;\n  box-shadow: 0 0 2px rgba(0, 0, 0, 0.3); }\n.modal-body.modal-tab-container .modal-tabs .nav-link {\n  padding: 10px 20px;\n  border: none; }\n.modal-body.modal-tab-container .modal-tabs .nav-link.active, .modal-body.modal-tab-container .modal-tabs .nav-link:hover {\n  color: #52bcd3;\n  border-bottom: 2px solid #52bcd3; }\n.modal-body.modal-tab-container .modal-tabs .nav-link.active {\n  font-weight: 600; }\na:not(.btn) {\n  transition: color .13s;\n  text-decoration: none;\n  color: #52bcd3; }\na:not(.btn):hover {\n  text-decoration: inherit;\n  color: #2c96ad; }\na:not(.btn):hover:before {\n  -webkit-transform: scaleX(1);\n  transform: scaleX(1); }\na:not(.btn):focus {\n  text-decoration: none; }\nspan a {\n  vertical-align: text-bottom; }\n[class*=' nav'] li > a, [class^=nav] li > a {\n  display: block; }\n[class*=' nav'] li > a:before, [class^=nav] li > a:before {\n  display: none; }\n.nav.nav-tabs-bordered {\n  border-color: #52bcd3; }\n.nav.nav-tabs-bordered + .tab-content {\n  border-style: solid;\n  border-width: 0 1px 1px 1px;\n  border-color: #52bcd3;\n  padding: 10px 20px 0; }\n.nav.nav-tabs-bordered .nav-item .nav-link {\n  text-decoration: none; }\n.nav.nav-tabs-bordered .nav-item .nav-link:hover {\n  color: #fff;\n  background-color: #52bcd3;\n  border: 1px solid #52bcd3; }\n.nav.nav-tabs-bordered .nav-item .nav-link.active {\n  border-color: #52bcd3;\n  border-bottom-color: transparent; }\n.nav.nav-tabs-bordered .nav-item .nav-link.active:hover {\n  background-color: #fff;\n  color: inherit; }\n.nav.nav-pills + .tab-content {\n  border: 0;\n  padding: 5px; }\n.nav.nav-pills .nav-item .nav-link {\n  text-decoration: none; }\n.nav.nav-pills .nav-item .nav-link:hover {\n  color: #4f5f6f;\n  background-color: transparent;\n  border: 0; }\n.nav.nav-pills .nav-item .nav-link.active {\n  border-color: #52bcd3;\n  border-bottom-color: transparent;\n  background-color: #52bcd3; }\n.nav.nav-pills .nav-item .nav-link.active:hover {\n  background-color: #52bcd3;\n  color: #fff; }\n#nprogress .bar {\n  background: #52bcd3 !important; }\n#nprogress .bar .peg {\n  box-shadow: 0 0 10px #52bcd3,0 0 5px #52bcd3; }\n#nprogress .spinner {\n  top: 25px !important;\n  right: 23px !important; }\n#nprogress .spinner .spinner-icon {\n  border-top-color: #52bcd3 !important;\n  border-left-color: #52bcd3 !important; }\n.pagination {\n  margin-top: 0; }\n.pagination .page-item .page-link {\n  color: #52bcd3; }\n.pagination .page-item.active .page-link, .pagination .page-item.active .page-link:focus, .pagination .page-item.active .page-link:hover {\n  color: #fff;\n  border-color: #52bcd3;\n  background-color: #52bcd3; }\n::-webkit-scrollbar {\n  width: 7px;\n  height: 7px; }\n::-webkit-scrollbar-track {\n  border-radius: 0; }\n::-webkit-scrollbar-thumb {\n  border-radius: 0;\n  background: #3eb4ce; }\n::-webkit-scrollbar-thumb:window-inactive {\n  background: #52bcd3; }\n.table label {\n  margin-bottom: 0; }\n.table .checkbox + span {\n  margin-bottom: 0; }\n.table .checkbox + span:before {\n  line-height: 20px; }\n.row .col {\n  padding-left: .9375rem;\n  padding-right: .9375rem;\n  float: left; }\n.row-sm {\n  margin-left: -10px;\n  margin-right: -10px; }\n.row-sm [class^=col] {\n  padding-left: 10px;\n  padding-right: 10px; }\n.title-block {\n  padding-bottom: 15px;\n  margin-bottom: 30px;\n  border-bottom: 1px solid #d7dde4; }\n.title-block::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n@media (max-width: 767px) {\n  .title-block {\n    margin-bottom: 20px; } }\n.subtitle-block {\n  padding-bottom: 10px;\n  margin-bottom: 20px;\n  border-bottom: 1px dashed #e0e5ea; }\n.section {\n  display: block;\n  margin-bottom: 30px; }\n.section:last-of-type {\n  margin-bottom: 0; }\n.box-placeholder {\n  margin-bottom: 15px;\n  padding: 20px;\n  border: 1px dashed #ddd;\n  background: #fafafa;\n  color: #444;\n  cursor: pointer; }\n.underline-animation {\n  position: absolute;\n  top: auto;\n  bottom: 1px;\n  left: 0;\n  width: 100%;\n  height: 1px;\n  background-color: #52bcd3;\n  content: '';\n  transition: all .2s;\n  -webkit-backface-visibility: hidden;\n  backface-visibility: hidden;\n  -webkit-transform: scaleX(0);\n  transform: scaleX(0); }\n.stat-chart {\n  border-radius: 50%; }\n.stat {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: inline-block;\n  margin-right: 10px; }\n.stat .value {\n  font-size: 20px;\n  line-height: 24px;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  font-weight: 500; }\n.stat .name {\n  overflow: hidden;\n  text-overflow: ellipsis; }\n.stat.lg .value {\n  font-size: 26px;\n  line-height: 28px; }\n.stat.lg .name {\n  font-size: 16px; }\n.list-icon [class^=col] {\n  cursor: pointer; }\n.list-icon [class^=col] em {\n  font-size: 14px;\n  width: 40px;\n  vertical-align: middle;\n  margin: 0;\n  display: inline-block;\n  text-align: center;\n  transition: all 1s;\n  line-height: 30px; }\n.list-icon [class^=col]:hover em {\n  -webkit-transform: scale(2, 2);\n  transform: scale(2, 2); }\n.well {\n  background-image: none;\n  background-color: #fff; }\n.jumbotron {\n  background-image: none;\n  background-color: #fff;\n  padding: 15px 30px; }\n.jumbotron.jumbotron-fluid {\n  padding-left: 0;\n  padding-right: 0; }\n.rounded {\n  border-radius: .25rem; }\n.rounded-l {\n  border-radius: .3rem; }\n.rounded-s {\n  border-radius: .2rem; }\n.jqstooltip {\n  height: 25px !important;\n  width: auto !important;\n  border-radius: .2rem; }\n.title {\n  font-size: 1.45rem;\n  font-weight: 600;\n  margin-bottom: 0; }\n.title.l {\n  font-size: 1.6rem; }\n.title.s {\n  font-size: 1.4rem; }\n.card .title {\n  font-size: 1.1rem;\n  color: #4f5f6f; }\n.title-description {\n  margin: 0;\n  font-size: .9rem;\n  font-weight: 400;\n  color: #7e8e9f; }\n.title-description.s {\n  font-size: .8rem; }\n@media (max-width: 767px) {\n  .title-description {\n    display: none; } }\n.subtitle {\n  font-size: 1.2rem;\n  margin: 0;\n  color: #7e8e9f; }\n.text-primary {\n  color: #52bcd3; }\n.text-muted {\n  color: #9ba8b5; }\npre {\n  padding: 0;\n  border: none;\n  background: 0 0; }\n.flot-chart {\n  display: block;\n  height: 225px; }\n.flot-chart .flot-chart-content {\n  width: 100%;\n  height: 100%; }\n.flot-chart .flot-chart-pie-content {\n  width: 225px;\n  height: 225px;\n  margin: auto; }\n.dashboard-page #dashboard-downloads-chart, .dashboard-page #dashboard-visits-chart {\n  height: 220px; }\n@media (max-width: 543px) {\n  .dashboard-page .items .card-header {\n    border: none;\n    flex-wrap: wrap; }\n  .dashboard-page .items .card-header .header-block {\n    display: flex;\n    align-items: center;\n    width: 100%;\n    justify-content: space-between;\n    border-bottom: 1px solid #e9edf0; } }\n.dashboard-page .items .card-header .title {\n  padding-right: 0;\n  margin-right: 5px; }\n.dashboard-page .items .card-header .search {\n  margin: 0;\n  vertical-align: middle;\n  display: inline-flex;\n  flex-direction: row;\n  align-items: center; }\n@media (max-width: 543px) {\n  .dashboard-page .items .card-header .search {\n    min-width: 50%; } }\n.dashboard-page .items .card-header .search .search-input {\n  border: none;\n  background-color: inherit;\n  color: #c2ccd6;\n  width: 100px;\n  transition: color .3s ease; }\n.dashboard-page .items .card-header .search .search-input::-webkit-input-placeholder {\n  transition: color .3s ease;\n  color: #c2ccd6; }\n.dashboard-page .items .card-header .search .search-input:-moz-placeholder {\n  transition: color .3s ease;\n  color: #c2ccd6; }\n.dashboard-page .items .card-header .search .search-input::-moz-placeholder {\n  transition: color .3s ease;\n  color: #c2ccd6; }\n.dashboard-page .items .card-header .search .search-input:-ms-input-placeholder {\n  transition: color .3s ease;\n  color: #c2ccd6; }\n@media (max-width: 543px) {\n  .dashboard-page .items .card-header .search .search-input {\n    min-width: 130px; } }\n.dashboard-page .items .card-header .search .search-input:focus {\n  color: #7e8e9f; }\n.dashboard-page .items .card-header .search .search-input:focus::-webkit-input-placeholder {\n  color: #aab4c0; }\n.dashboard-page .items .card-header .search .search-input:focus:-moz-placeholder {\n  color: #aab4c0; }\n.dashboard-page .items .card-header .search .search-input:focus::-moz-placeholder {\n  color: #aab4c0; }\n.dashboard-page .items .card-header .search .search-input:focus:-ms-input-placeholder {\n  color: #aab4c0; }\n.dashboard-page .items .card-header .search .search-input:focus + .search-icon {\n  color: #7e8e9f; }\n.dashboard-page .items .card-header .search .search-icon {\n  color: #c2ccd6;\n  transition: color .3s ease;\n  order: -1;\n  padding-right: 6px; }\n.dashboard-page .items .card-header .pagination {\n  display: inline-block;\n  margin: 0; }\n.dashboard-page .items .item-list .item-col-title {\n  flex-grow: 9; }\n.dashboard-page .items .item-list .item-col-date {\n  text-align: right; }\n@media (min-width: 1200px) {\n  .dashboard-page .items .item-list .item-col-date {\n    flex-grow: 4; } }\n@media (max-width: 767px) {\n  .dashboard-page .items .item-list .item-row {\n    padding: 0; }\n  .dashboard-page .items .item-list .item-col {\n    padding-left: 10px;\n    padding-right: 10px; }\n  .dashboard-page .items .item-list .item-col-img {\n    padding-left: 10px;\n    flex-basis: 60px;\n    padding-right: 0; }\n  .dashboard-page .items .item-list .item-col-stats {\n    text-align: center; } }\n@media (min-width: 544px) and (max-width: 767px) {\n  .dashboard-page .items .item-list .item-col-title {\n    flex-basis: 100%;\n    border-bottom: 1px solid #e9edf0; }\n  .dashboard-page .items .item-list .item-col:not(.item-col-title):not(.item-col-img) {\n    position: relative;\n    padding-top: 35px; }\n  .dashboard-page .items .item-list .item-heading {\n    position: absolute;\n    height: 30px;\n    width: 100%;\n    left: 0;\n    top: 5px;\n    line-height: 30px;\n    padding-left: 10px;\n    padding-right: 10px; } }\n@media (max-width: 543px) {\n  .dashboard-page .items .item-list .item-col {\n    border-bottom: 1px solid #e9edf0; }\n  .dashboard-page .items .item-list .item-col-img {\n    flex-basis: 50px;\n    order: -5; }\n  .dashboard-page .items .item-list .item-col-title {\n    flex-basis: calc(100% - 50px); }\n  .dashboard-page .items .item-list .item-col:not(.item-col-title):not(.item-col-img) {\n    flex-basis: 100%;\n    text-align: left; }\n  .dashboard-page .items .item-list .item-col:not(.item-col-title):not(.item-col-img) .item-heading {\n    text-align: left; }\n  .dashboard-page .items .item-list .item-col-date {\n    border: none; } }\n.dashboard-page .sales-breakdown .dashboard-sales-breakdown-chart {\n  margin: 0 auto;\n  max-width: 250px;\n  max-height: 250px; }\n.dashboard-page #dashboard-sales-map .jqvmap-zoomin, .dashboard-page #dashboard-sales-map .jqvmap-zoomout {\n  background-color: #52bcd3;\n  height: 20px;\n  width: 20px;\n  line-height: 14px; }\n.dashboard-page #dashboard-sales-map .jqvmap-zoomout {\n  top: 32px; }\n.dashboard-page .stats .card-block {\n  padding-bottom: 0; }\n.dashboard-page .stats .stat-col {\n  margin-bottom: 20px;\n  float: left;\n  white-space: nowrap;\n  overflow: hidden; }\n.dashboard-page .stats .stat-icon {\n  color: #52bcd3;\n  display: inline-block;\n  font-size: 26px;\n  text-align: center;\n  vertical-align: middle;\n  width: 50px; }\n.dashboard-page .stats .stat-chart {\n  margin-right: 5px;\n  vertical-align: middle; }\n@media (min-width: 1200px) {\n  .dashboard-page .stats .stat-chart {\n    margin-right: .6vw; } }\n.dashboard-page .stats .stat {\n  vertical-align: middle; }\n@media (min-width: 1200px) {\n  .dashboard-page .stats .stat .value {\n    font-size: 1.3vw; } }\n@media (min-width: 1200px) {\n  .dashboard-page .stats .stat .name {\n    font-size: .9vw; } }\n.dashboard-page .stats .stat-progress {\n  height: 2px;\n  margin: 5px 0;\n  color: #52bcd3; }\n.dashboard-page .stats .stat-progress[value]::-webkit-progress-bar {\n  background-color: #ddd; }\n.dashboard-page .stats .stat-progress[value]::-webkit-progress-value {\n  background-color: #52bcd3; }\n.dashboard-page .stats .stat-progress[value]::-moz-progress-bar {\n  background-color: #ddd; }\n.dashboard-page .tasks {\n  display: flex;\n  flex-direction: column;\n  align-content: stretch; }\n.dashboard-page .tasks .title-block .title {\n  align-items: center;\n  display: flex;\n  justify-content: space-between; }\n.dashboard-page .tasks label {\n  width: 100%;\n  margin-bottom: 0; }\n.dashboard-page .tasks label .checkbox:checked + span {\n  text-decoration: line-through; }\n.dashboard-page .tasks label span {\n  display: inline-block;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  width: 100%; }\n.dashboard-page .tasks .tasks-block {\n  max-height: 400px;\n  overflow-y: scroll;\n  overflow-x: hidden;\n  margin: 0;\n  margin-right: -5px; }\n.dashboard-page .tasks .item-list .item-col {\n  padding-top: 5px;\n  padding-bottom: 5px; }\n.items-list-page .title-search-block {\n  position: relative; }\n@media (max-width: 767px) {\n  .items-list-page .title-block {\n    padding-bottom: 10px;\n    margin-bottom: 13px; } }\n.items-list-page .title-block .action {\n  display: inline; }\n.items-list-page .title-block .action a {\n  padding: 10px 15px; }\n.items-list-page .title-block .action a .icon {\n  margin-right: 5px;\n  text-align: center;\n  width: 16px; }\n@media (max-width: 767px) {\n  .items-list-page .title-block .action {\n    display: none; } }\n.items-list-page .items-search {\n  position: absolute;\n  margin-bottom: 15px;\n  right: 0;\n  top: 0; }\n@media (max-width: 767px) {\n  .items-list-page .items-search {\n    position: static; } }\n.items-list-page .items-search .search-button {\n  margin: 0; }\n.items-list-page .item-list .item-col.item-col-check {\n  text-align: left; }\n.items-list-page .item-list .item-col.item-col-img {\n  text-align: left;\n  width: auto;\n  text-align: center;\n  flex-basis: 70px; }\n@media (min-width: 544px) {\n  .items-list-page .item-list .item-col.item-col-img:not(.item-col-header) {\n    height: 80px; } }\n.items-list-page .item-list .item-col.item-col-title {\n  text-align: left;\n  margin-left: 0 !important;\n  margin-right: auto;\n  flex-basis: 0;\n  flex-grow: 9; }\n.items-list-page .item-list .item-col.item-col-sales {\n  text-align: right;\n  font-weight: 600; }\n.items-list-page .item-list .item-col.item-col-stats {\n  text-align: center; }\n.items-list-page .item-list .item-col.item-col-category {\n  text-align: left;\n  font-weight: 600; }\n.items-list-page .item-list .item-col.item-col-author {\n  text-align: left;\n  flex-grow: 4.5; }\n.items-list-page .item-list .item-col.item-col-date {\n  text-align: right; }\n@media (max-width: 767px) {\n  .items-list-page .card.items {\n    background: 0 0;\n    box-shadow: none; }\n  .items-list-page .item-list .item {\n    border: none;\n    margin-bottom: 10px;\n    background-color: #fff;\n    box-shadow: 1px 1px 5px rgba(126, 142, 159, 0.1); }\n  .items-list-page .item-list .item-row {\n    padding: 0 !important; }\n  .items-list-page .item-list .item-col.item-col-author {\n    flex-grow: 3; } }\n@media (min-width: 544px) and (max-width: 767px) {\n  .items-list-page .item-list .item {\n    background-color: #fff;\n    margin-bottom: 10px;\n    box-shadow: 1px 1px 5px rgba(126, 142, 159, 0.1); }\n  .items-list-page .item-list .item-row {\n    padding: 0; }\n  .items-list-page .item-list .item-heading {\n    width: 100%;\n    display: block;\n    position: absolute;\n    top: 0;\n    width: 100%;\n    left: 0;\n    line-height: 40px;\n    padding-left: 0; }\n  .items-list-page .item-list .item-col.item-col-actions-dropdown, .items-list-page .item-list .item-col.item-col-check, .items-list-page .item-list .item-col.item-col-title {\n    border-bottom: 1px solid #d7dde4; }\n  .items-list-page .item-list .item-col.item-col-actions-dropdown .item-heading, .items-list-page .item-list .item-col.item-col-check .item-heading, .items-list-page .item-list .item-col.item-col-title .item-heading {\n    display: none; }\n  .items-list-page .item-list .item-col.item-col-author, .items-list-page .item-list .item-col.item-col-category, .items-list-page .item-list .item-col.item-col-date, .items-list-page .item-list .item-col.item-col-sales, .items-list-page .item-list .item-col.item-col-stats {\n    padding-top: 40px;\n    position: relative; }\n  .items-list-page .item-list .item-col.item-col-check {\n    display: none; }\n  .items-list-page .item-list .item-col.item-col-title {\n    padding-left: 10px;\n    text-align: left;\n    margin-left: 0 !important;\n    margin-right: auto;\n    flex-basis: calc(100% - 40px); }\n  .items-list-page .item-list .item-col.item-col-img {\n    padding-left: 10px;\n    flex-basis: 79px; }\n  .items-list-page .item-list .item-col.item-col-sales {\n    text-align: left; }\n  .items-list-page .item-list .item-col.item-col-stats {\n    text-align: center; }\n  .items-list-page .item-list .item-col.item-col-category {\n    text-align: center; }\n  .items-list-page .item-list .item-col.item-col-author {\n    text-align: center; }\n  .items-list-page .item-list .item-col.item-col-date {\n    padding-right: 10px;\n    text-align: right;\n    white-space: nowrap;\n    flex-basis: 100px;\n    flex-basis: 0;\n    flex-grow: 3; } }\n@media (max-width: 543px) {\n  .items-list-page .item-list .item {\n    border: none;\n    font-size: .9rem;\n    margin-bottom: 10px;\n    background-color: #fff;\n    box-shadow: 1px 1px 5px rgba(126, 142, 159, 0.1); }\n  .items-list-page .item-list .item .item-col {\n    text-align: right;\n    border-bottom: 1px solid #d7dde4;\n    padding-left: 10px; }\n  .items-list-page .item-list .item .item-col[class^=item-col] {\n    flex-basis: 100%; }\n  .items-list-page .item-list .item .item-col.item-col-check {\n    display: none; }\n  .items-list-page .item-list .item .item-col.item-col-img .item-img {\n    padding-bottom: 65%; }\n  .items-list-page .item-list .item .item-col.item-col-title {\n    text-align: left;\n    padding-bottom: 0;\n    border: none;\n    flex-grow: 1;\n    flex-basis: 0; }\n  .items-list-page .item-list .item .item-col.item-col-title .item-heading {\n    display: none; }\n  .items-list-page .item-list .item .item-col.item-col-title .item-title {\n    font-size: 1rem;\n    line-height: 1.4rem; }\n  .items-list-page .item-list .item .item-col.item-col-actions-dropdown {\n    border: none;\n    padding-bottom: 0; }\n  .items-list-page .item-list .item .item-col.item-col-sales {\n    text-align: left; }\n  .items-list-page .item-list .item .item-col.item-col-stats {\n    text-align: left; }\n  .items-list-page .item-list .item .item-col.item-col-category {\n    text-align: left; }\n  .items-list-page .item-list .item .item-col.item-col-author {\n    text-align: left; }\n  .items-list-page .item-list .item .item-col.item-col-date {\n    text-align: left; } }\n.table-flip-scroll table {\n  width: 100%; }\n@media only screen and (max-width: 800px) {\n  .table-flip-scroll .flip-content:after, .table-flip-scroll .flip-header:after {\n    visibility: hidden;\n    display: block;\n    font-size: 0;\n    content: \" \";\n    clear: both;\n    height: 0; }\n  .table-flip-scroll html .flip-content, .table-flip-scroll html .flip-header {\n    -ms-zoom: 1;\n    zoom: 1; }\n  .table-flip-scroll table {\n    width: 100%;\n    border-collapse: collapse;\n    border-spacing: 0;\n    display: block;\n    position: relative; }\n  .table-flip-scroll td, .table-flip-scroll th {\n    margin: 0;\n    vertical-align: top; }\n  .table-flip-scroll td:last-child, .table-flip-scroll th:last-child {\n    border-bottom: 1px solid #ddd; }\n  .table-flip-scroll th {\n    border: 0 !important;\n    border-right: 1px solid #ddd !important;\n    width: auto !important;\n    display: block;\n    text-align: right; }\n  .table-flip-scroll td {\n    display: block;\n    text-align: left;\n    border: 0 !important;\n    border-bottom: 1px solid #ddd !important; }\n  .table-flip-scroll thead {\n    display: block;\n    float: left; }\n  .table-flip-scroll thead tr {\n    display: block; }\n  .table-flip-scroll tbody {\n    display: block;\n    width: auto;\n    position: relative;\n    overflow-x: auto;\n    white-space: nowrap; }\n  .table-flip-scroll tbody tr {\n    display: inline-block;\n    vertical-align: top;\n    margin-left: -5px;\n    border-left: 1px solid #ddd; } }\n.wyswyg {\n  border: 1px solid #d7dde4; }\n.wyswyg .ql-container {\n  border-top: 1px solid #d7dde4; }\n.wyswyg .toolbar .btn {\n  margin: 0; }\n.wyswyg .ql-container {\n  font-size: 1rem; }\n.wyswyg .ql-container .ql-editor {\n  min-height: 200px; }\n.footer {\n  background-color: #fff;\n  position: absolute;\n  left: 230px;\n  right: 0;\n  bottom: 0;\n  height: 50px;\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n.footer-fixed .footer {\n  position: fixed; }\n.footer .footer-block {\n  vertical-align: middle;\n  margin-left: 20px;\n  margin-right: 20px; }\n.footer .footer-github-btn {\n  vertical-align: middle; }\n@media (max-width: 991px) {\n  .footer {\n    left: 0; } }\n.footer .author > ul {\n  list-style: none;\n  margin: 0;\n  padding: 0; }\n.footer .author > ul li {\n  display: inline-block; }\n.footer .author > ul li:after {\n  content: \"|\"; }\n.footer .author > ul li:last-child:after {\n  content: \"\"; }\n@media (max-width: 991px) {\n  .footer .author > ul li {\n    display: block;\n    text-align: right; }\n  .footer .author > ul li:after {\n    content: \"\"; } }\n@media (max-width: 991px) {\n  .footer .author > ul {\n    display: block; } }\n@media (max-width: 767px) {\n  .footer .author > ul {\n    display: none; } }\n.modal .modal-content {\n  border-radius: 0; }\n.modal .modal-header {\n  background-color: #52bcd3;\n  color: #fff; }\n.modal .modal-footer .btn {\n  margin-bottom: 0; }\n.header {\n  background-color: #d7dde4;\n  height: 70px;\n  position: absolute;\n  left: 230px;\n  right: 0;\n  transition: left .3s ease;\n  z-index: 10;\n  display: flex;\n  align-items: center; }\n@media (max-width: 991px) {\n  .header {\n    left: 0; } }\n@media (max-width: 767px) {\n  .header {\n    left: 0;\n    height: 50px; } }\n.header-fixed .header {\n  position: fixed; }\n.header .header-block {\n  margin-right: 15px; }\n@media (max-width: 767px) {\n  .header .header-block {\n    padding: 5px; } }\n.sidebar {\n  background-color: #3a4651;\n  width: 230px;\n  padding-bottom: 60px;\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  transition: left .3s ease;\n  z-index: 20; }\n@media (max-width: 991px) {\n  .sidebar {\n    position: fixed;\n    left: -230px; } }\n.sidebar-fixed .sidebar {\n  position: fixed; }\n.sidebar-open .sidebar {\n  left: 0; }\n.sidebar .sidebar-container {\n  position: absolute;\n  top: 0;\n  bottom: 51px;\n  width: 100%;\n  left: 0;\n  overflow-y: auto;\n  overflow-x: hidden; }\n.sidebar .sidebar-container::-webkit-scrollbar-track {\n  background-color: #2c353e; }\n.sidebar .nav {\n  font-size: 14px; }\n.open .sidebar .nav li a:focus, .sidebar .nav li a:focus {\n  background-color: inherit; }\n.sidebar .nav ul {\n  padding: 0;\n  height: 0;\n  overflow: hidden; }\n.loaded .sidebar .nav ul {\n  height: auto; }\n.sidebar .nav li.active ul {\n  height: auto; }\n.sidebar .nav li a {\n  color: rgba(255, 255, 255, 0.5);\n  text-decoration: none; }\n.sidebar .nav li a:hover, .sidebar .nav li.open a:hover, .sidebar .nav li.open > a {\n  color: #fff;\n  background-color: #2d363f; }\n.sidebar .nav > li > a {\n  padding-top: 15px;\n  padding-bottom: 15px;\n  padding-left: 20px;\n  padding-right: 10px; }\n.sidebar .nav > li.active > a, .sidebar .nav > li.active > a:hover {\n  background-color: #52bcd3 !important;\n  color: #fff !important; }\n.sidebar .nav > li.open > a {\n  background-color: #333e48; }\n.sidebar .nav > li.open > a i.arrow {\n  -webkit-transform: rotate(90deg);\n  transform: rotate(90deg); }\n.sidebar .nav > li > a i {\n  margin-right: 5px;\n  font-size: 16px; }\n.sidebar .nav > li > a i.arrow {\n  float: right;\n  font-size: 20px;\n  line-height: initial;\n  transition: all .3s ease; }\n.sidebar .nav > li > a i.arrow:before {\n  content: \"\" !important; }\n.sidebar .nav > li > ul > li a {\n  padding-top: 10px;\n  padding-bottom: 10px;\n  padding-left: 50px;\n  background-color: #333e48; }\n.sidebar .nav > li > ul > li.active a {\n  color: #fff; }\n.sidebar-overlay {\n  position: absolute;\n  display: none;\n  left: 200vw;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  z-index: 5;\n  opacity: 0;\n  transition: opacity .3s ease;\n  z-index: 15; }\n@media (max-width: 991px) {\n  .sidebar-overlay {\n    display: block; } }\n@media (max-width: 767px) {\n  .sidebar-overlay {\n    background-color: rgba(0, 0, 0, 0.7); } }\n@media (max-width: 991px) {\n  .sidebar-open .sidebar-overlay {\n    left: 0;\n    opacity: 1; } }\n#modal-media .modal-body {\n  min-height: 250px; }\n#modal-media .modal-tab-content {\n  min-height: 300px; }\n#modal-media .images-container {\n  padding: 15px;\n  text-align: center; }\n#modal-media .images-container .image-container {\n  margin: 0 auto 10px auto;\n  cursor: pointer;\n  transition: all .3s ease;\n  display: inline-block;\n  float: none; }\n#modal-media .images-container .image-container:hover {\n  border-color: rgba(82, 188, 211, 0.5); }\n#modal-media .images-container .image-container.active {\n  border-color: rgba(82, 188, 211, 0.5); }\n#modal-media .upload-container {\n  padding: 15px; }\n#modal-media .upload-container .dropzone {\n  position: relative;\n  border: 2px dashed #52bcd3;\n  height: 270px; }\n#modal-media .upload-container .dropzone .dz-message-block {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translateY(-50%) translateX(-50%);\n  transform: translateY(-50%) translateX(-50%); }\n#modal-media .upload-container .dropzone .dz-message-block .dz-message {\n  margin: 0;\n  font-size: 24px;\n  color: #52bcd3;\n  width: 230px; }\n.header .header-block-buttons {\n  text-align: center;\n  margin-left: auto;\n  margin-right: auto;\n  white-space: nowrap; }\n.header .header-block-buttons .btn.header-btn {\n  background-color: transparent;\n  border: 1px solid #64798d;\n  color: #64798d;\n  margin: 0 5px; }\n.header .header-block-buttons .btn.header-btn:focus, .header .header-block-buttons .btn.header-btn:hover {\n  border: 1px solid #3a4651;\n  color: #3a4651; }\n@media (max-width: 767px) {\n  .header .header-block-buttons {\n    display: none; } }\n.header .header-block-collapse {\n  padding-right: 5px; }\n.header .header-block-collapse .collapse-btn {\n  background: 0 0;\n  border: none;\n  box-shadow: none;\n  color: #52bcd3;\n  font-size: 24px;\n  line-height: 40px;\n  border-radius: 0;\n  outline: 0;\n  padding: 0;\n  padding-left: 10px;\n  padding-right: 10px;\n  vertical-align: initial; }\n.header .header-block-nav {\n  margin-left: auto;\n  white-space: nowrap; }\n.header .header-block-nav::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n.header .header-block-nav a {\n  text-decoration: none; }\n.header .header-block-nav ul {\n  margin: 0;\n  padding: 0;\n  list-style: none; }\n.header .header-block-nav > ul {\n  display: table; }\n.header .header-block-nav > ul > li {\n  display: table-cell;\n  position: relative; }\n.header .header-block-nav > ul > li:before {\n  display: block;\n  content: \" \";\n  width: 1px;\n  height: 24px;\n  top: 50%;\n  margin-top: -12px;\n  background-color: #8b9cb1;\n  position: absolute;\n  left: 0; }\n.header .header-block-nav > ul > li:first-child:before {\n  display: none; }\n.header .header-block-nav > ul > li > a {\n  padding: 0 15px;\n  color: #4f5f6f; }\n.header .header-block-nav > ul > li > a:hover {\n  color: #52bcd3; }\n.header .header-block-nav .dropdown-menu {\n  margin-top: 15px; }\n.header .header-block-search {\n  margin-right: auto;\n  padding-left: 30px; }\n@media (max-width: 767px) {\n  .header .header-block-search {\n    padding-left: 10px; } }\n@media (min-width: 768px) and (max-width: 991px) {\n  .header .header-block-search {\n    padding-left: 20px; } }\n@media (min-width: 992px) and (max-width: 1199px) {\n  .header .header-block-search {\n    padding-left: 30px; } }\n@media (min-width: 1200px) {\n  .header .header-block-search {\n    padding-left: 50px; } }\n.header .header-block-search > form {\n  float: right; }\n@media (max-width: 767px) {\n  .header .header-block-search > form {\n    padding-left: 0; } }\n.header .header-block-search .input-container {\n  position: relative;\n  color: #7e8e9f; }\n.header .header-block-search .input-container i {\n  position: absolute;\n  pointer-events: none;\n  display: block;\n  height: 40px;\n  line-height: 40px;\n  left: 0; }\n.header .header-block-search .input-container input {\n  background-color: transparent;\n  border: none;\n  padding-left: 25px;\n  height: 40px;\n  max-width: 150px; }\n@media (max-width: 767px) {\n  .header .header-block-search .input-container input {\n    max-width: 140px; } }\n.header .header-block-search .input-container input:focus + .underline {\n  -webkit-transform: scaleX(1);\n  transform: scaleX(1); }\n.sidebar-header .brand {\n  color: #fff;\n  text-align: left;\n  padding-left: 25px;\n  line-height: 70px;\n  font-size: 16px; }\n@media (max-width: 767px) {\n  .sidebar-header .brand {\n    line-height: 50px;\n    font-size: 16px; } }\n.customize {\n  width: 100%;\n  color: rgba(255, 255, 255, 0.5);\n  padding: 5px 15px;\n  text-align: center; }\n.customize .customize-header {\n  margin-bottom: 10px; }\n#customize-menu {\n  position: fixed;\n  bottom: 0;\n  left: 0;\n  width: 230px; }\n@media (max-width: 991px) {\n  .sidebar-open #customize-menu {\n    left: 0; } }\n@media (max-width: 991px) {\n  #customize-menu {\n    transition: left .3s ease;\n    left: -230px; } }\n#customize-menu > li > a {\n  background-color: #3a4651;\n  border-top: 1px solid rgba(45, 54, 63, 0.5); }\n#customize-menu > li.open > a, #customize-menu > li > a:hover {\n  background-color: #2d363f; }\n#customize-menu .customize {\n  width: 230px;\n  color: rgba(255, 255, 255, 0.5);\n  background-color: #2d363f;\n  text-align: center;\n  padding: 10px 15px;\n  border-top: 2px solid #52bcd3; }\n#customize-menu .customize .customize-item {\n  margin-bottom: 15px; }\n#customize-menu .customize .customize-item .customize-header {\n  margin-bottom: 10px; }\n#customize-menu .customize .customize-item label {\n  font-weight: 400; }\n#customize-menu .customize .customize-item label.title {\n  font-size: 14px; }\n#customize-menu .customize .customize-item .radio + span {\n  padding: 0;\n  padding-left: 5px; }\n#customize-menu .customize .customize-item .radio + span:before {\n  font-size: 17px;\n  color: #546273;\n  cursor: pointer; }\n#customize-menu .customize .customize-item .radio:checked + span:before {\n  color: #52bcd3; }\n#customize-menu .customize .customize-item .customize-colors {\n  list-style: none; }\n#customize-menu .customize .customize-item .customize-colors li {\n  display: inline-block;\n  margin-left: 5px;\n  margin-right: 5px; }\n#customize-menu .customize .customize-item .customize-colors li .color-item {\n  display: block;\n  height: 20px;\n  width: 20px;\n  border: 1px solid;\n  cursor: pointer; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-red {\n  background-color: #fb494d;\n  border-color: #fb494d; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-orange {\n  background-color: #fe7a0e;\n  border-color: #fe7a0e; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-green {\n  background-color: #8cde33;\n  border-color: #8cde33; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-seagreen {\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-blue {\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-purple {\n  background-color: #7867a7;\n  border-color: #7867a7; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.active {\n  position: relative;\n  font-family: FontAwesome;\n  font-size: 17px;\n  line-height: 17px; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.active:before {\n  content: \"\\f00c\";\n  position: absolute;\n  top: 0;\n  left: 0;\n  color: #fff; }\n.header .header-block-nav .notifications {\n  font-size: 16px; }\n.header .header-block-nav .notifications a {\n  padding-right: 10px; }\n.header .header-block-nav .notifications .counter {\n  font-weight: 700;\n  font-size: 14px;\n  position: relative;\n  top: -3px;\n  left: -2px; }\n.header .header-block-nav .notifications.new .counter {\n  color: #52bcd3;\n  font-weight: 700; }\n@media (max-width: 767px) {\n  .header .header-block-nav .notifications {\n    position: static; } }\n.header .header-block-nav .notifications-dropdown-menu {\n  white-space: normal;\n  left: auto;\n  right: 0;\n  min-width: 350px; }\n.header .header-block-nav .notifications-dropdown-menu:before {\n  position: absolute;\n  right: 20px;\n  bottom: 100%;\n  margin-right: -1px; }\n.header .header-block-nav .notifications-dropdown-menu:after {\n  position: absolute;\n  right: 20px;\n  bottom: 100%; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container .notification-item {\n  border-bottom: 1px solid rgba(126, 142, 159, 0.1);\n  padding: 5px; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container .notification-item:hover {\n  background-color: #f5f5f5; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container .img-col {\n  display: table-cell;\n  padding: 5px; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container .body-col {\n  padding: 5px;\n  display: table-cell; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container .img {\n  width: 40px;\n  height: 40px;\n  border-radius: 3px;\n  vertical-align: top;\n  display: inline-block;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container p {\n  color: #4f5f6f;\n  display: inline-block;\n  line-height: 18px;\n  font-size: 13px;\n  margin: 0;\n  vertical-align: top; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container p .accent {\n  font-weight: 700; }\n.header .header-block-nav .notifications-dropdown-menu footer {\n  text-align: center; }\n.header .header-block-nav .notifications-dropdown-menu footer a {\n  color: #373a3c;\n  transition: none; }\n.header .header-block-nav .notifications-dropdown-menu footer a:hover {\n  background-color: #f5f5f5;\n  color: #52bcd3; }\n@media (max-width: 767px) {\n  .header .header-block-nav .notifications-dropdown-menu {\n    min-width: 100px;\n    width: 100%;\n    margin-top: 5px; }\n  .header .header-block-nav .notifications-dropdown-menu:after, .header .header-block-nav .notifications-dropdown-menu:before {\n    right: 107px; } }\n.header .header-block-nav .profile .img {\n  display: inline-block;\n  width: 30px;\n  height: 30px;\n  line-height: 30px;\n  border-radius: 4px;\n  background-color: #8b9cb1;\n  color: #fff;\n  text-align: center;\n  margin-right: 10px;\n  background-repeat: no-repeat;\n  background-position: center;\n  background-size: cover;\n  vertical-align: middle; }\n.header .header-block-nav .profile .name {\n  display: inline-block;\n  margin-right: 9px;\n  font-weight: 700; }\n@media (max-width: 767px) {\n  .header .header-block-nav .profile .name {\n    display: none; } }\n.header .header-block-nav .profile .arrow {\n  color: #52bcd3; }\n.header .header-block-nav .profile-dropdown-menu {\n  left: auto;\n  right: 0;\n  min-width: 180px;\n  white-space: normal; }\n.header .header-block-nav .profile-dropdown-menu:before {\n  position: absolute;\n  right: 10px;\n  bottom: 100%;\n  margin-right: -1px; }\n.header .header-block-nav .profile-dropdown-menu:after {\n  position: absolute;\n  right: 10px;\n  bottom: 100%; }\n.header .header-block-nav .profile-dropdown-menu a {\n  padding: 10px 15px; }\n.header .header-block-nav .profile-dropdown-menu a .icon {\n  color: #52bcd3;\n  text-align: center;\n  width: 16px; }\n.header .header-block-nav .profile-dropdown-menu a span {\n  display: inline-block;\n  padding-left: 5px;\n  text-align: left;\n  color: #7e8e9f; }\n.header .header-block-nav .profile-dropdown-menu .profile-dropdown-menu-icon {\n  padding: 0; }\n.header .header-block-nav .profile-dropdown-menu .profile-dropdown-menu-topic {\n  color: #7e8e9f;\n  padding: 0; }\n.header .header-block-nav .profile-dropdown-menu .dropdown-divider {\n  margin: 0; }\n.header .header-block-nav .profile-dropdown-menu .logout {\n  border-top: 1px solid rgba(126, 142, 159, 0.1); }\n@media (max-width: 767px) {\n  .header .header-block-nav .profile-dropdown-menu {\n    margin-top: 8px; } }\n.form-group.has-error .form-help-text {\n  color: #fb494d;\n  font-size: 13px; }\n.form-group.has-error .form-control {\n  border-color: #fb494d; }\n.form-group.has-success .form-help-text {\n  color: #8cde33;\n  font-size: 13px; }\n.n-progress .progress-bar {\n  float: left;\n  width: 0;\n  height: 100%;\n  font-size: 12px;\n  line-height: 20px;\n  color: #211f33;\n  text-align: center;\n  background-color: #337ab7;\n  box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);\n  transition: width .6s ease; }\n.n-progress .progress-bar.success {\n  background-color: #5cb85c; }\n.n-progress .progress-bar.danger {\n  background-color: #f44; }\n.n-progress .progress-bar.warning {\n  background-color: #f0ad4e; }\n.n-progress {\n  height: 20px;\n  margin-bottom: 20px;\n  overflow: hidden;\n  background-color: #f5f5f5;\n  border-radius: 4px;\n  box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1); }\n.planning-expenses {\n  margin-right: 8px;\n  margin-top: 6px; }\n.text-center {\n  text-align: center; }\na {\n  cursor: pointer; }\n"

/***/ }),

/***/ "./src/app/client/components/header/header.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/client/components/header/header.component.ts ***!
  \**************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/client/components/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/client/components/header/header.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/client/components/sidebar/sidebar.component.html":
/*!******************************************************************!*\
  !*** ./src/app/client/components/sidebar/sidebar.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<aside class=\"sidebar\">\n  <div class=\"sidebar-container\">\n    <div class=\"sidebar-header\">\n      <div class=\"brand\">\n        <div class=\"logo\">\n         <h5>Тут логотип</h5>\n        </div>\n       <h3> Кабинет пользователя</h3>\n      </div>\n    </div>\n    <nav class=\"menu\">\n      <ul class=\"nav metismenu\">\n        <li class=\"active\">\n          <a href=\"#\"> <i class=\"fa fa-building-o\"></i>Текущие покупки</a>\n        </li>\n        <li>\n          <a href=\"#\"> <i class=\"fa fa-flash\"></i>История покупок</a>\n        </li>\n        <li>\n          <a href=\"#\"> <i class=\"fa fa-heart \"></i>Мои желания</a>\n        </li>\n        <li>\n          <a href=\"#\"> <i class=\"fa fa-plus-square\"></i>Запись</a>\n        </li>\n      </ul>\n    </nav>\n  </div>\n</aside>\n"

/***/ }),

/***/ "./src/app/client/components/sidebar/sidebar.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/client/components/sidebar/sidebar.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n@import url(http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,800,700,600);\n@charset \"UTF-8\";\nbody, html {\n  padding: 0;\n  margin: 0;\n  height: 100%;\n  min-height: 100%;\n  font-family: 'Open Sans',sans-serif;\n  color: #4f5f6f;\n  overflow-x: hidden; }\n.main-wrapper {\n  width: 100%;\n  position: absolute;\n  height: 100%;\n  overflow-y: auto;\n  overflow-x: hidden; }\n#ref .color-primary {\n  color: #52bcd3; }\n#ref .chart .color-primary {\n  color: #52bcd3; }\n#ref .chart .color-secondary {\n  color: #7bccdd; }\n.app {\n  position: relative;\n  width: 100%;\n  padding-left: 230px;\n  min-height: 100vh;\n  margin: 0 auto;\n  left: 0;\n  background-color: #f0f3f6;\n  box-shadow: 0 0 3px #ccc;\n  transition: left .3s ease,padding-left .3s ease;\n  overflow: hidden; }\n.app .content {\n  padding: 110px 30px 90px 30px;\n  min-height: 100vh; }\n@media (min-width: 1200px) {\n  .app .content {\n    padding: 120px 50px 100px 50px; } }\n@media (min-width: 992px) and (max-width: 1199px) {\n  .app .content {\n    padding: 110px 30px 90px 30px; } }\n@media (min-width: 768px) and (max-width: 991px) {\n  .app .content {\n    padding: 95px 20px 75px 20px; } }\n@media (max-width: 767px) {\n  .app .content {\n    padding: 65px 10px 65px 10px; } }\n@media (max-width: 991px) {\n  .app {\n    padding-left: 0; } }\n@media (max-width: 991px) {\n  .app.sidebar-open {\n    left: 0; } }\n.app.blank {\n  background-color: #667380; }\n.auth {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  background-color: #667380;\n  overflow-x: hidden;\n  overflow-y: auto; }\n.auth-container {\n  width: 450px;\n  min-height: 330px;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translateY(-50%) translateX(-50%);\n  transform: translateY(-50%) translateX(-50%); }\n.auth-container .auth-header {\n  text-align: center;\n  border-bottom: 1px solid #52bcd3; }\n.auth-container .auth-title {\n  color: #97a4b1;\n  padding: 20px;\n  line-height: 30px;\n  font-size: 26px;\n  font-weight: 600;\n  margin: 0; }\n.auth-container .auth-content {\n  padding: 30px 50px;\n  min-height: 260px; }\n.auth-container .forgot-btn {\n  line-height: 28px; }\n.auth-container .checkbox label {\n  padding: 0; }\n.auth-container .checkbox a {\n  vertical-align: text-top; }\n.auth-container .checkbox span {\n  color: #4f5f6f; }\n@media (max-width: 767px) {\n  .auth-container {\n    width: 100%;\n    position: relative;\n    left: 0;\n    top: 0;\n    -webkit-transform: inherit;\n    transform: inherit;\n    margin: 0;\n    margin-bottom: 10px; }\n  .auth-container .auth-content {\n    padding: 30px 25px; } }\n.error-card {\n  width: 410px;\n  min-height: 330px;\n  margin: 60px auto; }\n.error-card .error-title {\n  font-size: 150px;\n  line-height: 150px;\n  font-weight: 700;\n  color: #252932;\n  text-align: center;\n  text-shadow: rgba(61, 61, 61, 0.3) 0.5px 0.5px, rgba(61, 61, 61, 0.2) 1px 1px, rgba(61, 61, 61, 0.3) 1.5px 1.5px; }\n.error-card .error-sub-title {\n  font-weight: 100;\n  text-align: center; }\n.error-card .error-container {\n  text-align: center;\n  visibility: hidden; }\n.error-card .error-container.visible {\n  visibility: visible; }\n.error-card.global {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translateY(-50%) translateX(-50%);\n  transform: translateY(-50%) translateX(-50%);\n  margin: 0; }\n.error-card.global .error-title {\n  color: #fff; }\n.error-card.global .error-container, .error-card.global .error-sub-title {\n  color: #fff; }\n@media (min-width: 768px) and (max-width: 991px) {\n  .error-card {\n    width: 50%; }\n  .error-card.global {\n    position: relative;\n    top: 25%;\n    left: 0;\n    -webkit-transform: inherit;\n    transform: inherit;\n    margin: 40px auto; } }\n@media (max-width: 767px) {\n  .error-card {\n    width: 90%; }\n  .error-card.global {\n    position: relative;\n    top: 25%;\n    left: 0;\n    -webkit-transform: inherit;\n    transform: inherit;\n    margin: 40px auto; } }\n.alert {\n  background-image: none; }\n.alert.alert-primary {\n  background-color: #52bcd3;\n  border-color: #52bcd3;\n  color: #fff; }\n.alert.alert-primary hr {\n  border-top-color: #3eb4ce; }\n.alert.alert-primary .alert-link {\n  color: #e6e6e6; }\n.alert.alert-success {\n  background-color: #4bcf99;\n  border-color: #4bcf99;\n  color: #fff; }\n.alert.alert-success hr {\n  border-top-color: #37ca8e; }\n.alert.alert-success .alert-link {\n  color: #e6e6e6; }\n.alert.alert-info {\n  background-color: #76d4f5;\n  border-color: #76d4f5;\n  color: #fff; }\n.alert.alert-info hr {\n  border-top-color: #5ecdf3; }\n.alert.alert-info .alert-link {\n  color: #e6e6e6; }\n.alert.alert-warning {\n  background-color: #fe974b;\n  border-color: #fe974b;\n  color: #fff; }\n.alert.alert-warning hr {\n  border-top-color: #fe8832; }\n.alert.alert-warning .alert-link {\n  color: #e6e6e6; }\n.alert.alert-danger {\n  background-color: #f44;\n  border-color: #f44;\n  color: #fff; }\n.alert.alert-danger hr {\n  border-top-color: #ff2b2b; }\n.alert.alert-danger .alert-link {\n  color: #e6e6e6; }\n.alert.alert-inverse {\n  background-color: #131e26;\n  border-color: #131e26;\n  color: #fff; }\n.alert.alert-inverse hr {\n  border-top-color: #0b1115; }\n.alert.alert-inverse .alert-link {\n  color: #e6e6e6; }\n.animated {\n  -webkit-animation-duration: .5s;\n  animation-duration: .5s;\n  -webkit-animation-delay: .1s;\n  animation-delay: .1s; }\n.btn {\n  background-image: none;\n  border-radius: 0;\n  margin-bottom: 5px; }\n.btn.btn-primary {\n  color: #fff;\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.btn.btn-primary:hover {\n  color: #fff;\n  background-color: #31a7c1;\n  border-color: #2fa0b9; }\n.btn.btn-primary.focus, .btn.btn-primary:focus {\n  color: #fff;\n  background-color: #31a7c1;\n  border-color: #2fa0b9; }\n.btn.btn-primary.active, .btn.btn-primary:active, .open > .btn.btn-primary.dropdown-toggle {\n  color: #fff;\n  background-color: #31a7c1;\n  border-color: #2fa0b9;\n  background-image: none; }\n.btn.btn-primary.active.focus, .btn.btn-primary.active:focus, .btn.btn-primary.active:hover, .btn.btn-primary:active.focus, .btn.btn-primary:active:focus, .btn.btn-primary:active:hover, .open > .btn.btn-primary.dropdown-toggle.focus, .open > .btn.btn-primary.dropdown-toggle:focus, .open > .btn.btn-primary.dropdown-toggle:hover {\n  color: #fff;\n  background-color: #2a8fa4;\n  border-color: #227284; }\n.btn.btn-primary.disabled.focus, .btn.btn-primary.disabled:focus, .btn.btn-primary:disabled.focus, .btn.btn-primary:disabled:focus {\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.btn.btn-primary.disabled:hover, .btn.btn-primary:disabled:hover {\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.btn.btn-secondary {\n  color: #4f5f6f;\n  background-color: #fff;\n  border-color: #d7dde4; }\n.btn.btn-secondary:hover {\n  color: #4f5f6f;\n  background-color: #e6e6e6;\n  border-color: #b2becb; }\n.btn.btn-secondary.focus, .btn.btn-secondary:focus {\n  color: #4f5f6f;\n  background-color: #e6e6e6;\n  border-color: #b2becb; }\n.btn.btn-secondary.active, .btn.btn-secondary:active, .open > .btn.btn-secondary.dropdown-toggle {\n  color: #4f5f6f;\n  background-color: #e6e6e6;\n  border-color: #b2becb;\n  background-image: none; }\n.btn.btn-secondary.active.focus, .btn.btn-secondary.active:focus, .btn.btn-secondary.active:hover, .btn.btn-secondary:active.focus, .btn.btn-secondary:active:focus, .btn.btn-secondary:active:hover, .open > .btn.btn-secondary.dropdown-toggle.focus, .open > .btn.btn-secondary.dropdown-toggle:focus, .open > .btn.btn-secondary.dropdown-toggle:hover {\n  color: #4f5f6f;\n  background-color: #d4d4d4;\n  border-color: #8b9cb1; }\n.btn.btn-secondary.disabled.focus, .btn.btn-secondary.disabled:focus, .btn.btn-secondary:disabled.focus, .btn.btn-secondary:disabled:focus {\n  background-color: #fff;\n  border-color: #d7dde4; }\n.btn.btn-secondary.disabled:hover, .btn.btn-secondary:disabled:hover {\n  background-color: #fff;\n  border-color: #d7dde4; }\n.btn.btn-success {\n  color: #fff;\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n.btn.btn-success:hover {\n  color: #fff;\n  background-color: #31b680;\n  border-color: #2eae7a; }\n.btn.btn-success.focus, .btn.btn-success:focus {\n  color: #fff;\n  background-color: #31b680;\n  border-color: #2eae7a; }\n.btn.btn-success.active, .btn.btn-success:active, .open > .btn.btn-success.dropdown-toggle {\n  color: #fff;\n  background-color: #31b680;\n  border-color: #2eae7a;\n  background-image: none; }\n.btn.btn-success.active.focus, .btn.btn-success.active:focus, .btn.btn-success.active:hover, .btn.btn-success:active.focus, .btn.btn-success:active:focus, .btn.btn-success:active:hover, .open > .btn.btn-success.dropdown-toggle.focus, .open > .btn.btn-success.dropdown-toggle:focus, .open > .btn.btn-success.dropdown-toggle:hover {\n  color: #fff;\n  background-color: #299a6c;\n  border-color: #217a55; }\n.btn.btn-success.disabled.focus, .btn.btn-success.disabled:focus, .btn.btn-success:disabled.focus, .btn.btn-success:disabled:focus {\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n.btn.btn-success.disabled:hover, .btn.btn-success:disabled:hover {\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n.btn.btn-info {\n  color: #fff;\n  background-color: #76d4f5;\n  border-color: #76d4f5; }\n.btn.btn-info:hover {\n  color: #fff;\n  background-color: #46c5f2;\n  border-color: #3dc2f1; }\n.btn.btn-info.focus, .btn.btn-info:focus {\n  color: #fff;\n  background-color: #46c5f2;\n  border-color: #3dc2f1; }\n.btn.btn-info.active, .btn.btn-info:active, .open > .btn.btn-info.dropdown-toggle {\n  color: #fff;\n  background-color: #46c5f2;\n  border-color: #3dc2f1;\n  background-image: none; }\n.btn.btn-info.active.focus, .btn.btn-info.active:focus, .btn.btn-info.active:hover, .btn.btn-info:active.focus, .btn.btn-info:active:focus, .btn.btn-info:active:hover, .open > .btn.btn-info.dropdown-toggle.focus, .open > .btn.btn-info.dropdown-toggle:focus, .open > .btn.btn-info.dropdown-toggle:hover {\n  color: #fff;\n  background-color: #25bbef;\n  border-color: #10a7db; }\n.btn.btn-info.disabled.focus, .btn.btn-info.disabled:focus, .btn.btn-info:disabled.focus, .btn.btn-info:disabled:focus {\n  background-color: #76d4f5;\n  border-color: #76d4f5; }\n.btn.btn-info.disabled:hover, .btn.btn-info:disabled:hover {\n  background-color: #76d4f5;\n  border-color: #76d4f5; }\n.btn.btn-warning {\n  color: #fff;\n  background-color: #fe974b;\n  border-color: #fe974b; }\n.btn.btn-warning:hover {\n  color: #fff;\n  background-color: #fe7a18;\n  border-color: #fe740e; }\n.btn.btn-warning.focus, .btn.btn-warning:focus {\n  color: #fff;\n  background-color: #fe7a18;\n  border-color: #fe740e; }\n.btn.btn-warning.active, .btn.btn-warning:active, .open > .btn.btn-warning.dropdown-toggle {\n  color: #fff;\n  background-color: #fe7a18;\n  border-color: #fe740e;\n  background-image: none; }\n.btn.btn-warning.active.focus, .btn.btn-warning.active:focus, .btn.btn-warning.active:hover, .btn.btn-warning:active.focus, .btn.btn-warning:active:focus, .btn.btn-warning:active:hover, .open > .btn.btn-warning.dropdown-toggle.focus, .open > .btn.btn-warning.dropdown-toggle:focus, .open > .btn.btn-warning.dropdown-toggle:hover {\n  color: #fff;\n  background-color: #f16701;\n  border-color: #c85601; }\n.btn.btn-warning.disabled.focus, .btn.btn-warning.disabled:focus, .btn.btn-warning:disabled.focus, .btn.btn-warning:disabled:focus {\n  background-color: #fe974b;\n  border-color: #fe974b; }\n.btn.btn-warning.disabled:hover, .btn.btn-warning:disabled:hover {\n  background-color: #fe974b;\n  border-color: #fe974b; }\n.btn.btn-danger {\n  color: #fff;\n  background-color: #f44;\n  border-color: #f44; }\n.btn.btn-danger:hover {\n  color: #fff;\n  background-color: #f11;\n  border-color: #ff0707; }\n.btn.btn-danger.focus, .btn.btn-danger:focus {\n  color: #fff;\n  background-color: #f11;\n  border-color: #ff0707; }\n.btn.btn-danger.active, .btn.btn-danger:active, .open > .btn.btn-danger.dropdown-toggle {\n  color: #fff;\n  background-color: #f11;\n  border-color: #ff0707;\n  background-image: none; }\n.btn.btn-danger.active.focus, .btn.btn-danger.active:focus, .btn.btn-danger.active:hover, .btn.btn-danger:active.focus, .btn.btn-danger:active:focus, .btn.btn-danger:active:hover, .open > .btn.btn-danger.dropdown-toggle.focus, .open > .btn.btn-danger.dropdown-toggle:focus, .open > .btn.btn-danger.dropdown-toggle:hover {\n  color: #fff;\n  background-color: #ec0000;\n  border-color: #c40000; }\n.btn.btn-danger.disabled.focus, .btn.btn-danger.disabled:focus, .btn.btn-danger:disabled.focus, .btn.btn-danger:disabled:focus {\n  background-color: #f44;\n  border-color: #f44; }\n.btn.btn-danger.disabled:hover, .btn.btn-danger:disabled:hover {\n  background-color: #f44;\n  border-color: #f44; }\n.btn.btn-primary-outline {\n  color: #52bcd3;\n  background-image: none;\n  background-color: transparent;\n  border-color: #52bcd3; }\n.btn.btn-primary-outline.active, .btn.btn-primary-outline.focus, .btn.btn-primary-outline:active, .btn.btn-primary-outline:focus, .open > .btn.btn-primary-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.btn.btn-primary-outline:hover {\n  color: #fff;\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.btn.btn-primary-outline.disabled.focus, .btn.btn-primary-outline.disabled:focus, .btn.btn-primary-outline:disabled.focus, .btn.btn-primary-outline:disabled:focus {\n  border-color: #a3dbe8; }\n.btn.btn-primary-outline.disabled:hover, .btn.btn-primary-outline:disabled:hover {\n  border-color: #a3dbe8; }\n.btn.btn-secondary-outline {\n  color: #d7dde4;\n  background-image: none;\n  background-color: transparent;\n  border-color: #d7dde4; }\n.btn.btn-secondary-outline.active, .btn.btn-secondary-outline.focus, .btn.btn-secondary-outline:active, .btn.btn-secondary-outline:focus, .open > .btn.btn-secondary-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #d7dde4;\n  border-color: #d7dde4; }\n.btn.btn-secondary-outline:hover {\n  color: #fff;\n  background-color: #d7dde4;\n  border-color: #d7dde4; }\n.btn.btn-secondary-outline.disabled.focus, .btn.btn-secondary-outline.disabled:focus, .btn.btn-secondary-outline:disabled.focus, .btn.btn-secondary-outline:disabled:focus {\n  border-color: #fff; }\n.btn.btn-secondary-outline.disabled:hover, .btn.btn-secondary-outline:disabled:hover {\n  border-color: #fff; }\n.btn.btn-info-outline {\n  color: #76d4f5;\n  background-image: none;\n  background-color: transparent;\n  border-color: #76d4f5; }\n.btn.btn-info-outline.active, .btn.btn-info-outline.focus, .btn.btn-info-outline:active, .btn.btn-info-outline:focus, .open > .btn.btn-info-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #76d4f5;\n  border-color: #76d4f5; }\n.btn.btn-info-outline:hover {\n  color: #fff;\n  background-color: #76d4f5;\n  border-color: #76d4f5; }\n.btn.btn-info-outline.disabled.focus, .btn.btn-info-outline.disabled:focus, .btn.btn-info-outline:disabled.focus, .btn.btn-info-outline:disabled:focus {\n  border-color: #d5f2fc; }\n.btn.btn-info-outline.disabled:hover, .btn.btn-info-outline:disabled:hover {\n  border-color: #d5f2fc; }\n.btn.btn-success-outline {\n  color: #4bcf99;\n  background-image: none;\n  background-color: transparent;\n  border-color: #4bcf99; }\n.btn.btn-success-outline.active, .btn.btn-success-outline.focus, .btn.btn-success-outline:active, .btn.btn-success-outline:focus, .open > .btn.btn-success-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n.btn.btn-success-outline:hover {\n  color: #fff;\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n.btn.btn-success-outline.disabled.focus, .btn.btn-success-outline.disabled:focus, .btn.btn-success-outline:disabled.focus, .btn.btn-success-outline:disabled:focus {\n  border-color: #9ce4c7; }\n.btn.btn-success-outline.disabled:hover, .btn.btn-success-outline:disabled:hover {\n  border-color: #9ce4c7; }\n.btn.btn-warning-outline {\n  color: #fe974b;\n  background-image: none;\n  background-color: transparent;\n  border-color: #fe974b; }\n.btn.btn-warning-outline.active, .btn.btn-warning-outline.focus, .btn.btn-warning-outline:active, .btn.btn-warning-outline:focus, .open > .btn.btn-warning-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #fe974b;\n  border-color: #fe974b; }\n.btn.btn-warning-outline:hover {\n  color: #fff;\n  background-color: #fe974b;\n  border-color: #fe974b; }\n.btn.btn-warning-outline.disabled.focus, .btn.btn-warning-outline.disabled:focus, .btn.btn-warning-outline:disabled.focus, .btn.btn-warning-outline:disabled:focus {\n  border-color: #ffd2b0; }\n.btn.btn-warning-outline.disabled:hover, .btn.btn-warning-outline:disabled:hover {\n  border-color: #ffd2b0; }\n.btn.btn-danger-outline {\n  color: #f44;\n  background-image: none;\n  background-color: transparent;\n  border-color: #f44; }\n.btn.btn-danger-outline.active, .btn.btn-danger-outline.focus, .btn.btn-danger-outline:active, .btn.btn-danger-outline:focus, .open > .btn.btn-danger-outline.dropdown-toggle {\n  color: #fff;\n  background-color: #f44;\n  border-color: #f44; }\n.btn.btn-danger-outline:hover {\n  color: #fff;\n  background-color: #f44;\n  border-color: #f44; }\n.btn.btn-danger-outline.disabled.focus, .btn.btn-danger-outline.disabled:focus, .btn.btn-danger-outline:disabled.focus, .btn.btn-danger-outline:disabled:focus {\n  border-color: #faa; }\n.btn.btn-danger-outline.disabled:hover, .btn.btn-danger-outline:disabled:hover {\n  border-color: #faa; }\n.btn.btn-oval:focus, .btn.btn-pill-left:focus, .btn.btn-pill-right:focus {\n  outline: 0;\n  outline-offset: initial; }\n.btn.btn-pill-left {\n  border-top-left-radius: 25px;\n  border-bottom-left-radius: 25px; }\n.btn.btn-pill-right {\n  border-top-right-radius: 25px;\n  border-bottom-right-radius: 25px; }\n.btn.btn-oval {\n  border-radius: 25px; }\n.btn.btn-link {\n  text-decoration: none; }\n.btn strong {\n  font-weight: 600; }\n.btn-group .dropdown-menu > li:last-child a:hover:before {\n  height: 0;\n  -webkit-transform: scaleX(0);\n  transform: scaleX(0); }\n.card {\n  background-color: #fff;\n  box-shadow: 1px 1px 5px rgba(126, 142, 159, 0.1);\n  margin-bottom: 10px;\n  border-radius: 0;\n  border: none; }\n.card .card {\n  box-shadow: none; }\n.card .card-header {\n  background-image: none;\n  background-color: #fff;\n  align-items: center;\n  display: flex;\n  flex-direction: row;\n  padding: 0;\n  border-radius: 0;\n  min-height: 50px;\n  border: none; }\n.card .card-header::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n.card .card-header.bordered {\n  border-bottom: 1px solid #d7dde4; }\n.card .card-header.card-header-sm {\n  min-height: 40px; }\n.card .card-header > span {\n  vertical-align: middle; }\n.card .card-header .pull-right {\n  margin-left: auto; }\n.card .card-header .header-block {\n  padding: .5rem 15px; }\n@media (min-width: 1200px) {\n  .card .card-header .header-block {\n    padding: .5rem 20px; } }\n@media (max-width: 767px) {\n  .card .card-header .header-block {\n    padding: .5rem 10px; } }\n.card .card-header .title {\n  color: #4f5f6f;\n  display: inline-flex; }\n.card .card-header .btn {\n  margin: 0; }\n.card .card-header .nav-tabs {\n  border-color: transparent;\n  align-self: stretch;\n  display: flex;\n  position: relative;\n  top: 1px; }\n.card .card-header .nav-tabs .nav-item {\n  margin-left: 0;\n  display: flex;\n  align-self: stretch; }\n.card .card-header .nav-tabs .nav-item .nav-link {\n  display: flex;\n  align-self: stretch;\n  align-items: center;\n  color: #4f5f6f;\n  opacity: .7;\n  padding-left: 10px;\n  padding-right: 10px;\n  border-radius: 0;\n  font-size: 14px;\n  border-top-width: 2px;\n  border-bottom: 1px solid #d7dde4;\n  text-decoration: none; }\n.card .card-header .nav-tabs .nav-item .nav-link.active {\n  border-top-color: #52bcd3;\n  border-bottom-color: transparent;\n  opacity: 1; }\n.card .card-header .nav-tabs .nav-item .nav-link.active:focus, .card .card-header .nav-tabs .nav-item .nav-link.active:hover {\n  opacity: 1;\n  background-color: #fff;\n  border-color: #d7dde4 #d7dde4 transparent;\n  border-top-color: #52bcd3; }\n.card .card-header .nav-tabs .nav-item .nav-link:focus, .card .card-header .nav-tabs .nav-item .nav-link:hover {\n  opacity: 1;\n  background-color: transparent;\n  border-color: transparent; }\n.card.card-default > .card-header {\n  background-color: #fff;\n  color: inherit; }\n.card.card-primary {\n  border-color: #52bcd3; }\n.card.card-primary > .card-header {\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n.card.card-success > .card-header {\n  background-color: #4bcf99; }\n.card.card-info > .card-header {\n  background-color: #76d4f5; }\n.card.card-warning > .card-header {\n  background-color: #fe974b; }\n.card.card-danger > .card-header {\n  background-color: #f44; }\n.card.card-inverse > .card-header {\n  background-color: #131e26; }\n.card .card-title-block, .card .title-block {\n  padding-bottom: 0;\n  margin-bottom: 20px;\n  border: none; }\n.card .card-title-block::after, .card .title-block::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n.card .section {\n  margin-bottom: 20px; }\n.card .example, .card .section.demo {\n  margin-bottom: 20px; }\n.card-block {\n  padding: 15px; }\n.card-block .tab-content {\n  padding: 0;\n  border-color: transparent; }\n@media (min-width: 1200px) {\n  .card-block {\n    padding: 20px; } }\n@media (max-width: 767px) {\n  .card-block {\n    padding: 10px; } }\n.card-footer {\n  background-color: #fafafa; }\n.easy-pie-chart {\n  width: 50px;\n  height: 50px;\n  display: inline-block;\n  background-color: #d7dde4;\n  border-radius: 5px; }\n.dropdown-menu {\n  float: left;\n  box-shadow: 2px 3px 6px rgba(126, 142, 159, 0.1);\n  border: 1px solid rgba(126, 142, 159, 0.1);\n  border-top-left-radius: 0;\n  border-top-right-radius: 0; }\n.dropdown-menu .dropdown-item {\n  display: block;\n  padding: 0 15px;\n  clear: both;\n  font-weight: 400;\n  color: #4f5f6f;\n  white-space: nowrap;\n  transition: none; }\n.dropdown-menu .dropdown-item i {\n  margin-right: 2px; }\n.dropdown-menu .dropdown-item:hover {\n  color: #52bcd3 !important;\n  background: 0 0;\n  background-color: #f5f5f5; }\n.flex-row {\n  display: flex;\n  flex-direction: row; }\n.flex-col {\n  display: flex;\n  flex-direction: column; }\n.centralize-y {\n  display: flex;\n  align-items: center; }\ninput, textarea {\n  outline: 0; }\n.form-group .row {\n  margin-left: -10px;\n  margin-right: -10px; }\n.form-group .row [class^=col] {\n  padding-left: 10px;\n  padding-right: 10px; }\n.form-group.has-error span.has-error {\n  color: #f44;\n  font-size: 13px;\n  display: block !important; }\n.form-group.has-error .form-control-feedback {\n  color: #f44; }\n.form-group.has-warning span.has-warning {\n  color: #fe974b;\n  font-size: 13px;\n  display: block !important; }\n.form-group.has-warning .form-control-feedback {\n  color: #fe974b; }\n.form-group.has-success span.has-success {\n  color: #4bcf99;\n  font-size: 13px;\n  display: block !important; }\n.form-group.has-success .form-control-feedback {\n  color: #4bcf99; }\n.input-group {\n  margin-bottom: 10px; }\n.input-group .form-control {\n  padding-left: 5px; }\n.input-group span.input-group-addon {\n  font-style: italic;\n  border: none;\n  border-radius: 0;\n  border: none;\n  background-color: #d7dde4;\n  transition: background-color ease-in-out 15s,color ease-in-out .15s; }\n.input-group span.input-group-addon.focus {\n  background-color: #52bcd3;\n  color: #fff; }\n.control-label, label {\n  font-weight: 600; }\n.form-control.underlined {\n  padding-left: 0;\n  padding-right: 0;\n  border-radius: 0;\n  border: none;\n  border-radius: 0;\n  box-shadow: none;\n  border-bottom: 1px solid #d7dde4; }\n.form-control.underlined.indented {\n  padding: .375rem .75rem; }\n.form-control.underlined:focus, .has-error .form-control.underlined:focus, .has-success .form-control.underlined:focus, .has-warning .form-control.underlined:focus {\n  border: none;\n  box-shadow: none;\n  border-bottom: 1px solid #52bcd3; }\n.has-error .form-control.underlined {\n  box-shadow: none;\n  border-color: #f44; }\n.has-warning .form-control.underlined {\n  box-shadow: none;\n  border-color: #fe974b; }\n.has-success .form-control.underlined {\n  box-shadow: none;\n  border-color: #4bcf99; }\n.form-control.boxed {\n  border-radius: 0;\n  box-shadow: none; }\n.form-control.boxed:focus {\n  border: 1px solid #52bcd3; }\n.checkbox, .radio {\n  display: none; }\n.checkbox + span, .radio + span {\n  padding: 0;\n  padding-right: 10px; }\n.checkbox + span:before, .radio + span:before {\n  font-family: FontAwesome;\n  font-size: 21px;\n  display: inline-block;\n  vertical-align: middle;\n  letter-spacing: 10px;\n  color: #c8d0da; }\n.checkbox:checked + span:before, .radio:checked + span:before {\n  color: #52bcd3; }\n.checkbox:disabled + span:before, .radio:disabled + span:before {\n  opacity: .5;\n  cursor: not-allowed; }\n.checkbox:checked:disabled + span:before, .radio:checked:disabled + span:before {\n  color: #c8d0da; }\n.checkbox + span:before {\n  content: \"\\f0c8\"; }\n.checkbox:checked + span:before {\n  content: \"\\f14a\"; }\n.checkbox.rounded + span:before {\n  content: \"\\f111\"; }\n.checkbox.rounded:checked + span:before {\n  content: \"\\f058\"; }\n.radio + span:before {\n  content: \"\\f111\"; }\n.radio:checked + span:before {\n  content: \"\\f192\"; }\n.radio.squared + span:before {\n  content: \"\\f0c8\"; }\n.radio.squared:checked + span:before {\n  content: \"\\f14a\"; }\n.form-control::-webkit-input-placeholder {\n  font-style: italic;\n  color: #c8d0da; }\n.form-control:-moz-placeholder {\n  font-style: italic;\n  color: #d7dde4; }\n.form-control::-moz-placeholder {\n  font-style: italic;\n  color: #d7dde4; }\n.form-control:-ms-input-placeholder {\n  font-style: italic;\n  color: #d7dde4; }\n.images-container::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n.images-container .image-container {\n  float: left;\n  padding: 3px;\n  margin-right: 10px;\n  margin-bottom: 35px;\n  position: relative;\n  border: 1px solid #e6eaee;\n  overflow: hidden; }\n.images-container .image-container.active {\n  border-color: #52bcd3; }\n.images-container .image-container:hover .controls {\n  bottom: 0;\n  opacity: 1; }\n.images-container .controls {\n  position: absolute;\n  left: 0;\n  right: 0;\n  opacity: 0;\n  bottom: -35px;\n  text-align: center;\n  height: 35px;\n  font-size: 24px;\n  transition: bottom .2s ease,opacity .2s ease;\n  background-color: #fff; }\n.images-container .controls::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n.images-container .controls .control-btn {\n  display: inline-block;\n  color: #4f5f6f;\n  cursor: pointer;\n  width: 35px;\n  height: 35px;\n  line-height: 35px;\n  text-align: center;\n  opacity: .5;\n  transition: opacity .3s ease; }\n.images-container .controls .control-btn:hover {\n  opacity: 1; }\n.images-container .controls .control-btn.move {\n  cursor: move; }\n.images-container .controls .control-btn.star {\n  color: #ffb300; }\n.images-container .controls .control-btn.star i:before {\n  content: \"\\f006\"; }\n.images-container .controls .control-btn.star.active i:before {\n  content: \"\\f005\"; }\n.images-container .controls .control-btn.remove {\n  color: #f44; }\n.images-container .image {\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n  width: 130px;\n  height: 135px;\n  line-height: 135px;\n  text-align: center; }\n.images-container .image-container.main {\n  border-color: #ffb300; }\n.images-container .image-container.new {\n  opacity: .6;\n  transition: opacity .3s ease;\n  border-style: dashed;\n  border: 1px #52bcd3 solid;\n  color: #52bcd3; }\n.images-container .image-container.new .image {\n  font-size: 2.5rem; }\n.images-container .image-container.new:hover {\n  opacity: 1; }\n.item-list {\n  list-style: none;\n  padding: 0;\n  margin: 0;\n  margin-bottom: 0;\n  line-height: 1.4rem;\n  display: flex;\n  flex-flow: column nowrap; }\n@media (min-width: 992px) and (max-width: 1199px) {\n  .item-list {\n    font-size: 1rem; } }\n@media (min-width: 768px) and (max-width: 991px) {\n  .item-list {\n    font-size: .95rem; } }\n@media (max-width: 767px) {\n  .item-list {\n    font-size: 1.05rem; } }\n.item-list.striped > li {\n  border-bottom: 1px solid #e9edf0; }\n.item-list.striped > li:nth-child(2n+1) {\n  background-color: #fcfcfd; }\n@media (max-width: 767px) {\n  .item-list.striped > li:nth-child(2n+1) {\n    background-color: #f8f9fb; } }\n.item-list.striped .item-list-footer {\n  border-bottom: none; }\n.item-list .item {\n  display: flex;\n  flex-direction: column; }\n.item-list .item-row {\n  display: flex;\n  align-items: stretch;\n  flex-direction: row;\n  justify-content: space-between;\n  flex-wrap: wrap;\n  min-width: 100%; }\n.item-list .item-row.nowrap {\n  flex-wrap: nowrap; }\n.item-list .item-col {\n  align-items: center;\n  display: flex;\n  padding: 10px 10px 10px 0;\n  flex-basis: 0;\n  flex-grow: 3;\n  flex-shrink: 3;\n  margin-left: auto;\n  margin-right: auto;\n  min-width: 0; }\n.item-list .item-col.fixed {\n  flex-grow: 0;\n  flex-shrink: 0;\n  flex-basis: auto; }\n.item-list .item-col.pull-left {\n  margin-right: auto; }\n.item-list .item-col.pull-right {\n  margin-left: auto; }\n.item-list .item-col > div {\n  width: 100%; }\n.item-list .item-col:last-child {\n  padding-right: 0; }\n.item-list .no-overflow {\n  overflow: hidden; }\n.item-list .no-wrap {\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap; }\n.item-list .item-list-header .item-col.item-col-header span {\n  color: #999;\n  font-size: .8rem;\n  font-weight: 700 !important; }\n.item-list .item-heading {\n  font-size: .9rem;\n  display: none;\n  color: #666;\n  font-weight: 700;\n  padding-right: 10px; }\n@media (max-width: 767px) {\n  .item-list .item-heading {\n    display: block; } }\n@media (min-width: 544px) and (max-width: 767px) {\n  .item-list .item-heading {\n    width: 100%; } }\n@media (max-width: 543px) {\n  .item-list .item-heading {\n    width: 40%; } }\n.item-list .item-col.item-col-check {\n  flex-basis: 30px; }\n@media (max-width: 767px) {\n  .item-list .item-col.item-col-check {\n    order: -8; } }\n.item-list .item-check {\n  margin-bottom: 0; }\n.item-list .item-check .checkbox + span {\n  padding-right: 0; }\n.item-list .item-check .checkbox + span:before {\n  width: 20px; }\n.item-list .item-col.item-col-img {\n  display: flex;\n  flex-basis: 70px; }\n.item-list .item-col.item-col-img.xs {\n  flex-basis: 40px; }\n.item-list .item-col.item-col-img.sm {\n  flex-basis: 50px; }\n.item-list .item-col.item-col-img.lg {\n  flex-basis: 100px; }\n.item-list .item-col.item-col-img.xl {\n  flex-basis: 120px; }\n.item-list .item-col.item-col-img a {\n  width: 100%; }\n.item-list .item-img {\n  flex-grow: 1;\n  -ms-grid-row-align: stretch;\n      align-self: stretch;\n  background-color: #efefef;\n  padding-bottom: 100%;\n  width: 100%;\n  height: 0;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat; }\n@media (max-width: 767px) {\n  .item-list .item-col.item-col-title {\n    order: -4; } }\n.item-list .item-col.item-col-title a {\n  display: block; }\n.item-list .item-title {\n  margin: 0;\n  font-size: inherit;\n  line-height: inherit;\n  font-weight: 600; }\n.item-list .item-stats {\n  height: 1.4rem; }\n.item-list .item-col.item-col-actions-dropdown {\n  flex-basis: 40px;\n  text-align: center;\n  padding-left: 0 !important; }\n@media (max-width: 767px) {\n  .item-list .item-col.item-col-actions-dropdown {\n    order: -3;\n    flex-basis: 40px !important;\n    padding-right: 10px; } }\n.item-list .item-actions-dropdown {\n  position: relative;\n  font-size: 1.1rem; }\n.item-list .item-actions-dropdown.active .item-actions-block {\n  max-width: 120px; }\n.item-list .item-actions-dropdown.active .item-actions-toggle-btn {\n  color: #52bcd3; }\n.item-list .item-actions-dropdown.active .item-actions-toggle-btn .active {\n  display: block; }\n.item-list .item-actions-dropdown.active .item-actions-toggle-btn .inactive {\n  display: none; }\n.item-list .item-actions-dropdown .item-actions-toggle-btn {\n  color: #9ba8b5;\n  font-size: 1.2rem;\n  cursor: pointer;\n  width: 100%;\n  line-height: 30px;\n  text-align: center;\n  text-decoration: none; }\n.item-list .item-actions-dropdown .item-actions-toggle-btn .active {\n  display: none; }\n.item-list .item-actions-dropdown .item-actions-block {\n  height: 30px;\n  max-width: 0;\n  line-height: 30px;\n  overflow: hidden;\n  position: absolute;\n  top: 0;\n  right: 100%;\n  background-color: #d7dde4;\n  border-radius: 3px;\n  transition: all .15s ease-in-out; }\n.item-list .item-actions-dropdown .item-actions-block.direction-right {\n  right: auto;\n  left: 100%; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list {\n  padding: 0;\n  list-style: none;\n  white-space: nowrap;\n  padding: 0 5px; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list li {\n  display: inline-block;\n  padding: 0; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list a {\n  display: block;\n  padding: 0 5px; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list a.edit {\n  color: #38424c; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list a.check {\n  color: #40b726; }\n.item-list .item-actions-dropdown .item-actions-block .item-actions-list a.remove {\n  color: #db0e1e; }\n.card > .item-list .item > .item-row {\n  padding: 0 15px; }\n@media (min-width: 1200px) {\n  .card > .item-list .item > .item-row {\n    padding: 0 20px; } }\n@media (max-width: 767px) {\n  .card > .item-list .item > .item-row {\n    padding: 0 10px; } }\n.logo {\n  display: inline-block;\n  width: 45px;\n  height: 25px;\n  vertical-align: middle;\n  margin-right: 5px;\n  position: relative; }\n.logo .l {\n  width: 11px;\n  height: 11px;\n  border-radius: 50%;\n  background-color: #52bcd3;\n  position: absolute; }\n.logo .l.l1 {\n  bottom: 0;\n  left: 0; }\n.logo .l.l2 {\n  width: 7px;\n  height: 7px;\n  bottom: 13px;\n  left: 10px; }\n.logo .l.l3 {\n  width: 7px;\n  height: 7px;\n  bottom: 4px;\n  left: 17px; }\n.logo .l.l4 {\n  bottom: 13px;\n  left: 25px; }\n.logo .l.l5 {\n  bottom: 0;\n  left: 34px; }\n.modal-body.modal-tab-container {\n  padding: 0; }\n.modal-body.modal-tab-container .modal-tabs {\n  padding-left: 0;\n  margin-bottom: 0;\n  list-style: none;\n  background-color: #fff;\n  border-bottom: 1px solid #ddd;\n  box-shadow: 0 0 2px rgba(0, 0, 0, 0.3); }\n.modal-body.modal-tab-container .modal-tabs .nav-link {\n  padding: 10px 20px;\n  border: none; }\n.modal-body.modal-tab-container .modal-tabs .nav-link.active, .modal-body.modal-tab-container .modal-tabs .nav-link:hover {\n  color: #52bcd3;\n  border-bottom: 2px solid #52bcd3; }\n.modal-body.modal-tab-container .modal-tabs .nav-link.active {\n  font-weight: 600; }\na:not(.btn) {\n  transition: color .13s;\n  text-decoration: none;\n  color: #52bcd3; }\na:not(.btn):hover {\n  text-decoration: inherit;\n  color: #2c96ad; }\na:not(.btn):hover:before {\n  -webkit-transform: scaleX(1);\n  transform: scaleX(1); }\na:not(.btn):focus {\n  text-decoration: none; }\nspan a {\n  vertical-align: text-bottom; }\n[class*=' nav'] li > a, [class^=nav] li > a {\n  display: block; }\n[class*=' nav'] li > a:before, [class^=nav] li > a:before {\n  display: none; }\n.nav.nav-tabs-bordered {\n  border-color: #52bcd3; }\n.nav.nav-tabs-bordered + .tab-content {\n  border-style: solid;\n  border-width: 0 1px 1px 1px;\n  border-color: #52bcd3;\n  padding: 10px 20px 0; }\n.nav.nav-tabs-bordered .nav-item .nav-link {\n  text-decoration: none; }\n.nav.nav-tabs-bordered .nav-item .nav-link:hover {\n  color: #fff;\n  background-color: #52bcd3;\n  border: 1px solid #52bcd3; }\n.nav.nav-tabs-bordered .nav-item .nav-link.active {\n  border-color: #52bcd3;\n  border-bottom-color: transparent; }\n.nav.nav-tabs-bordered .nav-item .nav-link.active:hover {\n  background-color: #fff;\n  color: inherit; }\n.nav.nav-pills + .tab-content {\n  border: 0;\n  padding: 5px; }\n.nav.nav-pills .nav-item .nav-link {\n  text-decoration: none; }\n.nav.nav-pills .nav-item .nav-link:hover {\n  color: #4f5f6f;\n  background-color: transparent;\n  border: 0; }\n.nav.nav-pills .nav-item .nav-link.active {\n  border-color: #52bcd3;\n  border-bottom-color: transparent;\n  background-color: #52bcd3; }\n.nav.nav-pills .nav-item .nav-link.active:hover {\n  background-color: #52bcd3;\n  color: #fff; }\n#nprogress .bar {\n  background: #52bcd3 !important; }\n#nprogress .bar .peg {\n  box-shadow: 0 0 10px #52bcd3,0 0 5px #52bcd3; }\n#nprogress .spinner {\n  top: 25px !important;\n  right: 23px !important; }\n#nprogress .spinner .spinner-icon {\n  border-top-color: #52bcd3 !important;\n  border-left-color: #52bcd3 !important; }\n.pagination {\n  margin-top: 0; }\n.pagination .page-item .page-link {\n  color: #52bcd3; }\n.pagination .page-item.active .page-link, .pagination .page-item.active .page-link:focus, .pagination .page-item.active .page-link:hover {\n  color: #fff;\n  border-color: #52bcd3;\n  background-color: #52bcd3; }\n::-webkit-scrollbar {\n  width: 7px;\n  height: 7px; }\n::-webkit-scrollbar-track {\n  border-radius: 0; }\n::-webkit-scrollbar-thumb {\n  border-radius: 0;\n  background: #3eb4ce; }\n::-webkit-scrollbar-thumb:window-inactive {\n  background: #52bcd3; }\n.table label {\n  margin-bottom: 0; }\n.table .checkbox + span {\n  margin-bottom: 0; }\n.table .checkbox + span:before {\n  line-height: 20px; }\n.row .col {\n  padding-left: .9375rem;\n  padding-right: .9375rem;\n  float: left; }\n.row-sm {\n  margin-left: -10px;\n  margin-right: -10px; }\n.row-sm [class^=col] {\n  padding-left: 10px;\n  padding-right: 10px; }\n.title-block {\n  padding-bottom: 15px;\n  margin-bottom: 30px;\n  border-bottom: 1px solid #d7dde4; }\n.title-block::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n@media (max-width: 767px) {\n  .title-block {\n    margin-bottom: 20px; } }\n.subtitle-block {\n  padding-bottom: 10px;\n  margin-bottom: 20px;\n  border-bottom: 1px dashed #e0e5ea; }\n.section {\n  display: block;\n  margin-bottom: 30px; }\n.section:last-of-type {\n  margin-bottom: 0; }\n.box-placeholder {\n  margin-bottom: 15px;\n  padding: 20px;\n  border: 1px dashed #ddd;\n  background: #fafafa;\n  color: #444;\n  cursor: pointer; }\n.underline-animation {\n  position: absolute;\n  top: auto;\n  bottom: 1px;\n  left: 0;\n  width: 100%;\n  height: 1px;\n  background-color: #52bcd3;\n  content: '';\n  transition: all .2s;\n  -webkit-backface-visibility: hidden;\n  backface-visibility: hidden;\n  -webkit-transform: scaleX(0);\n  transform: scaleX(0); }\n.stat-chart {\n  border-radius: 50%; }\n.stat {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: inline-block;\n  margin-right: 10px; }\n.stat .value {\n  font-size: 20px;\n  line-height: 24px;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  font-weight: 500; }\n.stat .name {\n  overflow: hidden;\n  text-overflow: ellipsis; }\n.stat.lg .value {\n  font-size: 26px;\n  line-height: 28px; }\n.stat.lg .name {\n  font-size: 16px; }\n.list-icon [class^=col] {\n  cursor: pointer; }\n.list-icon [class^=col] em {\n  font-size: 14px;\n  width: 40px;\n  vertical-align: middle;\n  margin: 0;\n  display: inline-block;\n  text-align: center;\n  transition: all 1s;\n  line-height: 30px; }\n.list-icon [class^=col]:hover em {\n  -webkit-transform: scale(2, 2);\n  transform: scale(2, 2); }\n.well {\n  background-image: none;\n  background-color: #fff; }\n.jumbotron {\n  background-image: none;\n  background-color: #fff;\n  padding: 15px 30px; }\n.jumbotron.jumbotron-fluid {\n  padding-left: 0;\n  padding-right: 0; }\n.rounded {\n  border-radius: .25rem; }\n.rounded-l {\n  border-radius: .3rem; }\n.rounded-s {\n  border-radius: .2rem; }\n.jqstooltip {\n  height: 25px !important;\n  width: auto !important;\n  border-radius: .2rem; }\n.title {\n  font-size: 1.45rem;\n  font-weight: 600;\n  margin-bottom: 0; }\n.title.l {\n  font-size: 1.6rem; }\n.title.s {\n  font-size: 1.4rem; }\n.card .title {\n  font-size: 1.1rem;\n  color: #4f5f6f; }\n.title-description {\n  margin: 0;\n  font-size: .9rem;\n  font-weight: 400;\n  color: #7e8e9f; }\n.title-description.s {\n  font-size: .8rem; }\n@media (max-width: 767px) {\n  .title-description {\n    display: none; } }\n.subtitle {\n  font-size: 1.2rem;\n  margin: 0;\n  color: #7e8e9f; }\n.text-primary {\n  color: #52bcd3; }\n.text-muted {\n  color: #9ba8b5; }\npre {\n  padding: 0;\n  border: none;\n  background: 0 0; }\n.flot-chart {\n  display: block;\n  height: 225px; }\n.flot-chart .flot-chart-content {\n  width: 100%;\n  height: 100%; }\n.flot-chart .flot-chart-pie-content {\n  width: 225px;\n  height: 225px;\n  margin: auto; }\n.dashboard-page #dashboard-downloads-chart, .dashboard-page #dashboard-visits-chart {\n  height: 220px; }\n@media (max-width: 543px) {\n  .dashboard-page .items .card-header {\n    border: none;\n    flex-wrap: wrap; }\n  .dashboard-page .items .card-header .header-block {\n    display: flex;\n    align-items: center;\n    width: 100%;\n    justify-content: space-between;\n    border-bottom: 1px solid #e9edf0; } }\n.dashboard-page .items .card-header .title {\n  padding-right: 0;\n  margin-right: 5px; }\n.dashboard-page .items .card-header .search {\n  margin: 0;\n  vertical-align: middle;\n  display: inline-flex;\n  flex-direction: row;\n  align-items: center; }\n@media (max-width: 543px) {\n  .dashboard-page .items .card-header .search {\n    min-width: 50%; } }\n.dashboard-page .items .card-header .search .search-input {\n  border: none;\n  background-color: inherit;\n  color: #c2ccd6;\n  width: 100px;\n  transition: color .3s ease; }\n.dashboard-page .items .card-header .search .search-input::-webkit-input-placeholder {\n  transition: color .3s ease;\n  color: #c2ccd6; }\n.dashboard-page .items .card-header .search .search-input:-moz-placeholder {\n  transition: color .3s ease;\n  color: #c2ccd6; }\n.dashboard-page .items .card-header .search .search-input::-moz-placeholder {\n  transition: color .3s ease;\n  color: #c2ccd6; }\n.dashboard-page .items .card-header .search .search-input:-ms-input-placeholder {\n  transition: color .3s ease;\n  color: #c2ccd6; }\n@media (max-width: 543px) {\n  .dashboard-page .items .card-header .search .search-input {\n    min-width: 130px; } }\n.dashboard-page .items .card-header .search .search-input:focus {\n  color: #7e8e9f; }\n.dashboard-page .items .card-header .search .search-input:focus::-webkit-input-placeholder {\n  color: #aab4c0; }\n.dashboard-page .items .card-header .search .search-input:focus:-moz-placeholder {\n  color: #aab4c0; }\n.dashboard-page .items .card-header .search .search-input:focus::-moz-placeholder {\n  color: #aab4c0; }\n.dashboard-page .items .card-header .search .search-input:focus:-ms-input-placeholder {\n  color: #aab4c0; }\n.dashboard-page .items .card-header .search .search-input:focus + .search-icon {\n  color: #7e8e9f; }\n.dashboard-page .items .card-header .search .search-icon {\n  color: #c2ccd6;\n  transition: color .3s ease;\n  order: -1;\n  padding-right: 6px; }\n.dashboard-page .items .card-header .pagination {\n  display: inline-block;\n  margin: 0; }\n.dashboard-page .items .item-list .item-col-title {\n  flex-grow: 9; }\n.dashboard-page .items .item-list .item-col-date {\n  text-align: right; }\n@media (min-width: 1200px) {\n  .dashboard-page .items .item-list .item-col-date {\n    flex-grow: 4; } }\n@media (max-width: 767px) {\n  .dashboard-page .items .item-list .item-row {\n    padding: 0; }\n  .dashboard-page .items .item-list .item-col {\n    padding-left: 10px;\n    padding-right: 10px; }\n  .dashboard-page .items .item-list .item-col-img {\n    padding-left: 10px;\n    flex-basis: 60px;\n    padding-right: 0; }\n  .dashboard-page .items .item-list .item-col-stats {\n    text-align: center; } }\n@media (min-width: 544px) and (max-width: 767px) {\n  .dashboard-page .items .item-list .item-col-title {\n    flex-basis: 100%;\n    border-bottom: 1px solid #e9edf0; }\n  .dashboard-page .items .item-list .item-col:not(.item-col-title):not(.item-col-img) {\n    position: relative;\n    padding-top: 35px; }\n  .dashboard-page .items .item-list .item-heading {\n    position: absolute;\n    height: 30px;\n    width: 100%;\n    left: 0;\n    top: 5px;\n    line-height: 30px;\n    padding-left: 10px;\n    padding-right: 10px; } }\n@media (max-width: 543px) {\n  .dashboard-page .items .item-list .item-col {\n    border-bottom: 1px solid #e9edf0; }\n  .dashboard-page .items .item-list .item-col-img {\n    flex-basis: 50px;\n    order: -5; }\n  .dashboard-page .items .item-list .item-col-title {\n    flex-basis: calc(100% - 50px); }\n  .dashboard-page .items .item-list .item-col:not(.item-col-title):not(.item-col-img) {\n    flex-basis: 100%;\n    text-align: left; }\n  .dashboard-page .items .item-list .item-col:not(.item-col-title):not(.item-col-img) .item-heading {\n    text-align: left; }\n  .dashboard-page .items .item-list .item-col-date {\n    border: none; } }\n.dashboard-page .sales-breakdown .dashboard-sales-breakdown-chart {\n  margin: 0 auto;\n  max-width: 250px;\n  max-height: 250px; }\n.dashboard-page #dashboard-sales-map .jqvmap-zoomin, .dashboard-page #dashboard-sales-map .jqvmap-zoomout {\n  background-color: #52bcd3;\n  height: 20px;\n  width: 20px;\n  line-height: 14px; }\n.dashboard-page #dashboard-sales-map .jqvmap-zoomout {\n  top: 32px; }\n.dashboard-page .stats .card-block {\n  padding-bottom: 0; }\n.dashboard-page .stats .stat-col {\n  margin-bottom: 20px;\n  float: left;\n  white-space: nowrap;\n  overflow: hidden; }\n.dashboard-page .stats .stat-icon {\n  color: #52bcd3;\n  display: inline-block;\n  font-size: 26px;\n  text-align: center;\n  vertical-align: middle;\n  width: 50px; }\n.dashboard-page .stats .stat-chart {\n  margin-right: 5px;\n  vertical-align: middle; }\n@media (min-width: 1200px) {\n  .dashboard-page .stats .stat-chart {\n    margin-right: .6vw; } }\n.dashboard-page .stats .stat {\n  vertical-align: middle; }\n@media (min-width: 1200px) {\n  .dashboard-page .stats .stat .value {\n    font-size: 1.3vw; } }\n@media (min-width: 1200px) {\n  .dashboard-page .stats .stat .name {\n    font-size: .9vw; } }\n.dashboard-page .stats .stat-progress {\n  height: 2px;\n  margin: 5px 0;\n  color: #52bcd3; }\n.dashboard-page .stats .stat-progress[value]::-webkit-progress-bar {\n  background-color: #ddd; }\n.dashboard-page .stats .stat-progress[value]::-webkit-progress-value {\n  background-color: #52bcd3; }\n.dashboard-page .stats .stat-progress[value]::-moz-progress-bar {\n  background-color: #ddd; }\n.dashboard-page .tasks {\n  display: flex;\n  flex-direction: column;\n  align-content: stretch; }\n.dashboard-page .tasks .title-block .title {\n  align-items: center;\n  display: flex;\n  justify-content: space-between; }\n.dashboard-page .tasks label {\n  width: 100%;\n  margin-bottom: 0; }\n.dashboard-page .tasks label .checkbox:checked + span {\n  text-decoration: line-through; }\n.dashboard-page .tasks label span {\n  display: inline-block;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  width: 100%; }\n.dashboard-page .tasks .tasks-block {\n  max-height: 400px;\n  overflow-y: scroll;\n  overflow-x: hidden;\n  margin: 0;\n  margin-right: -5px; }\n.dashboard-page .tasks .item-list .item-col {\n  padding-top: 5px;\n  padding-bottom: 5px; }\n.items-list-page .title-search-block {\n  position: relative; }\n@media (max-width: 767px) {\n  .items-list-page .title-block {\n    padding-bottom: 10px;\n    margin-bottom: 13px; } }\n.items-list-page .title-block .action {\n  display: inline; }\n.items-list-page .title-block .action a {\n  padding: 10px 15px; }\n.items-list-page .title-block .action a .icon {\n  margin-right: 5px;\n  text-align: center;\n  width: 16px; }\n@media (max-width: 767px) {\n  .items-list-page .title-block .action {\n    display: none; } }\n.items-list-page .items-search {\n  position: absolute;\n  margin-bottom: 15px;\n  right: 0;\n  top: 0; }\n@media (max-width: 767px) {\n  .items-list-page .items-search {\n    position: static; } }\n.items-list-page .items-search .search-button {\n  margin: 0; }\n.items-list-page .item-list .item-col.item-col-check {\n  text-align: left; }\n.items-list-page .item-list .item-col.item-col-img {\n  text-align: left;\n  width: auto;\n  text-align: center;\n  flex-basis: 70px; }\n@media (min-width: 544px) {\n  .items-list-page .item-list .item-col.item-col-img:not(.item-col-header) {\n    height: 80px; } }\n.items-list-page .item-list .item-col.item-col-title {\n  text-align: left;\n  margin-left: 0 !important;\n  margin-right: auto;\n  flex-basis: 0;\n  flex-grow: 9; }\n.items-list-page .item-list .item-col.item-col-sales {\n  text-align: right;\n  font-weight: 600; }\n.items-list-page .item-list .item-col.item-col-stats {\n  text-align: center; }\n.items-list-page .item-list .item-col.item-col-category {\n  text-align: left;\n  font-weight: 600; }\n.items-list-page .item-list .item-col.item-col-author {\n  text-align: left;\n  flex-grow: 4.5; }\n.items-list-page .item-list .item-col.item-col-date {\n  text-align: right; }\n@media (max-width: 767px) {\n  .items-list-page .card.items {\n    background: 0 0;\n    box-shadow: none; }\n  .items-list-page .item-list .item {\n    border: none;\n    margin-bottom: 10px;\n    background-color: #fff;\n    box-shadow: 1px 1px 5px rgba(126, 142, 159, 0.1); }\n  .items-list-page .item-list .item-row {\n    padding: 0 !important; }\n  .items-list-page .item-list .item-col.item-col-author {\n    flex-grow: 3; } }\n@media (min-width: 544px) and (max-width: 767px) {\n  .items-list-page .item-list .item {\n    background-color: #fff;\n    margin-bottom: 10px;\n    box-shadow: 1px 1px 5px rgba(126, 142, 159, 0.1); }\n  .items-list-page .item-list .item-row {\n    padding: 0; }\n  .items-list-page .item-list .item-heading {\n    width: 100%;\n    display: block;\n    position: absolute;\n    top: 0;\n    width: 100%;\n    left: 0;\n    line-height: 40px;\n    padding-left: 0; }\n  .items-list-page .item-list .item-col.item-col-actions-dropdown, .items-list-page .item-list .item-col.item-col-check, .items-list-page .item-list .item-col.item-col-title {\n    border-bottom: 1px solid #d7dde4; }\n  .items-list-page .item-list .item-col.item-col-actions-dropdown .item-heading, .items-list-page .item-list .item-col.item-col-check .item-heading, .items-list-page .item-list .item-col.item-col-title .item-heading {\n    display: none; }\n  .items-list-page .item-list .item-col.item-col-author, .items-list-page .item-list .item-col.item-col-category, .items-list-page .item-list .item-col.item-col-date, .items-list-page .item-list .item-col.item-col-sales, .items-list-page .item-list .item-col.item-col-stats {\n    padding-top: 40px;\n    position: relative; }\n  .items-list-page .item-list .item-col.item-col-check {\n    display: none; }\n  .items-list-page .item-list .item-col.item-col-title {\n    padding-left: 10px;\n    text-align: left;\n    margin-left: 0 !important;\n    margin-right: auto;\n    flex-basis: calc(100% - 40px); }\n  .items-list-page .item-list .item-col.item-col-img {\n    padding-left: 10px;\n    flex-basis: 79px; }\n  .items-list-page .item-list .item-col.item-col-sales {\n    text-align: left; }\n  .items-list-page .item-list .item-col.item-col-stats {\n    text-align: center; }\n  .items-list-page .item-list .item-col.item-col-category {\n    text-align: center; }\n  .items-list-page .item-list .item-col.item-col-author {\n    text-align: center; }\n  .items-list-page .item-list .item-col.item-col-date {\n    padding-right: 10px;\n    text-align: right;\n    white-space: nowrap;\n    flex-basis: 100px;\n    flex-basis: 0;\n    flex-grow: 3; } }\n@media (max-width: 543px) {\n  .items-list-page .item-list .item {\n    border: none;\n    font-size: .9rem;\n    margin-bottom: 10px;\n    background-color: #fff;\n    box-shadow: 1px 1px 5px rgba(126, 142, 159, 0.1); }\n  .items-list-page .item-list .item .item-col {\n    text-align: right;\n    border-bottom: 1px solid #d7dde4;\n    padding-left: 10px; }\n  .items-list-page .item-list .item .item-col[class^=item-col] {\n    flex-basis: 100%; }\n  .items-list-page .item-list .item .item-col.item-col-check {\n    display: none; }\n  .items-list-page .item-list .item .item-col.item-col-img .item-img {\n    padding-bottom: 65%; }\n  .items-list-page .item-list .item .item-col.item-col-title {\n    text-align: left;\n    padding-bottom: 0;\n    border: none;\n    flex-grow: 1;\n    flex-basis: 0; }\n  .items-list-page .item-list .item .item-col.item-col-title .item-heading {\n    display: none; }\n  .items-list-page .item-list .item .item-col.item-col-title .item-title {\n    font-size: 1rem;\n    line-height: 1.4rem; }\n  .items-list-page .item-list .item .item-col.item-col-actions-dropdown {\n    border: none;\n    padding-bottom: 0; }\n  .items-list-page .item-list .item .item-col.item-col-sales {\n    text-align: left; }\n  .items-list-page .item-list .item .item-col.item-col-stats {\n    text-align: left; }\n  .items-list-page .item-list .item .item-col.item-col-category {\n    text-align: left; }\n  .items-list-page .item-list .item .item-col.item-col-author {\n    text-align: left; }\n  .items-list-page .item-list .item .item-col.item-col-date {\n    text-align: left; } }\n.table-flip-scroll table {\n  width: 100%; }\n@media only screen and (max-width: 800px) {\n  .table-flip-scroll .flip-content:after, .table-flip-scroll .flip-header:after {\n    visibility: hidden;\n    display: block;\n    font-size: 0;\n    content: \" \";\n    clear: both;\n    height: 0; }\n  .table-flip-scroll html .flip-content, .table-flip-scroll html .flip-header {\n    -ms-zoom: 1;\n    zoom: 1; }\n  .table-flip-scroll table {\n    width: 100%;\n    border-collapse: collapse;\n    border-spacing: 0;\n    display: block;\n    position: relative; }\n  .table-flip-scroll td, .table-flip-scroll th {\n    margin: 0;\n    vertical-align: top; }\n  .table-flip-scroll td:last-child, .table-flip-scroll th:last-child {\n    border-bottom: 1px solid #ddd; }\n  .table-flip-scroll th {\n    border: 0 !important;\n    border-right: 1px solid #ddd !important;\n    width: auto !important;\n    display: block;\n    text-align: right; }\n  .table-flip-scroll td {\n    display: block;\n    text-align: left;\n    border: 0 !important;\n    border-bottom: 1px solid #ddd !important; }\n  .table-flip-scroll thead {\n    display: block;\n    float: left; }\n  .table-flip-scroll thead tr {\n    display: block; }\n  .table-flip-scroll tbody {\n    display: block;\n    width: auto;\n    position: relative;\n    overflow-x: auto;\n    white-space: nowrap; }\n  .table-flip-scroll tbody tr {\n    display: inline-block;\n    vertical-align: top;\n    margin-left: -5px;\n    border-left: 1px solid #ddd; } }\n.wyswyg {\n  border: 1px solid #d7dde4; }\n.wyswyg .ql-container {\n  border-top: 1px solid #d7dde4; }\n.wyswyg .toolbar .btn {\n  margin: 0; }\n.wyswyg .ql-container {\n  font-size: 1rem; }\n.wyswyg .ql-container .ql-editor {\n  min-height: 200px; }\n.footer {\n  background-color: #fff;\n  position: absolute;\n  left: 230px;\n  right: 0;\n  bottom: 0;\n  height: 50px;\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n.footer-fixed .footer {\n  position: fixed; }\n.footer .footer-block {\n  vertical-align: middle;\n  margin-left: 20px;\n  margin-right: 20px; }\n.footer .footer-github-btn {\n  vertical-align: middle; }\n@media (max-width: 991px) {\n  .footer {\n    left: 0; } }\n.footer .author > ul {\n  list-style: none;\n  margin: 0;\n  padding: 0; }\n.footer .author > ul li {\n  display: inline-block; }\n.footer .author > ul li:after {\n  content: \"|\"; }\n.footer .author > ul li:last-child:after {\n  content: \"\"; }\n@media (max-width: 991px) {\n  .footer .author > ul li {\n    display: block;\n    text-align: right; }\n  .footer .author > ul li:after {\n    content: \"\"; } }\n@media (max-width: 991px) {\n  .footer .author > ul {\n    display: block; } }\n@media (max-width: 767px) {\n  .footer .author > ul {\n    display: none; } }\n.modal .modal-content {\n  border-radius: 0; }\n.modal .modal-header {\n  background-color: #52bcd3;\n  color: #fff; }\n.modal .modal-footer .btn {\n  margin-bottom: 0; }\n.header {\n  background-color: #d7dde4;\n  height: 70px;\n  position: absolute;\n  left: 230px;\n  right: 0;\n  transition: left .3s ease;\n  z-index: 10;\n  display: flex;\n  align-items: center; }\n@media (max-width: 991px) {\n  .header {\n    left: 0; } }\n@media (max-width: 767px) {\n  .header {\n    left: 0;\n    height: 50px; } }\n.header-fixed .header {\n  position: fixed; }\n.header .header-block {\n  margin-right: 15px; }\n@media (max-width: 767px) {\n  .header .header-block {\n    padding: 5px; } }\n.sidebar {\n  background-color: #3a4651;\n  width: 230px;\n  padding-bottom: 60px;\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  transition: left .3s ease;\n  z-index: 20; }\n@media (max-width: 991px) {\n  .sidebar {\n    position: fixed;\n    left: -230px; } }\n.sidebar-fixed .sidebar {\n  position: fixed; }\n.sidebar-open .sidebar {\n  left: 0; }\n.sidebar .sidebar-container {\n  position: absolute;\n  top: 0;\n  bottom: 51px;\n  width: 100%;\n  left: 0;\n  overflow-y: auto;\n  overflow-x: hidden; }\n.sidebar .sidebar-container::-webkit-scrollbar-track {\n  background-color: #2c353e; }\n.sidebar .nav {\n  font-size: 14px; }\n.open .sidebar .nav li a:focus, .sidebar .nav li a:focus {\n  background-color: inherit; }\n.sidebar .nav ul {\n  padding: 0;\n  height: 0;\n  overflow: hidden; }\n.loaded .sidebar .nav ul {\n  height: auto; }\n.sidebar .nav li.active ul {\n  height: auto; }\n.sidebar .nav li a {\n  color: rgba(255, 255, 255, 0.5);\n  text-decoration: none; }\n.sidebar .nav li a:hover, .sidebar .nav li.open a:hover, .sidebar .nav li.open > a {\n  color: #fff;\n  background-color: #2d363f; }\n.sidebar .nav > li > a {\n  padding-top: 15px;\n  padding-bottom: 15px;\n  padding-left: 20px;\n  padding-right: 10px; }\n.sidebar .nav > li.active > a, .sidebar .nav > li.active > a:hover {\n  background-color: #52bcd3 !important;\n  color: #fff !important; }\n.sidebar .nav > li.open > a {\n  background-color: #333e48; }\n.sidebar .nav > li.open > a i.arrow {\n  -webkit-transform: rotate(90deg);\n  transform: rotate(90deg); }\n.sidebar .nav > li > a i {\n  margin-right: 5px;\n  font-size: 16px; }\n.sidebar .nav > li > a i.arrow {\n  float: right;\n  font-size: 20px;\n  line-height: initial;\n  transition: all .3s ease; }\n.sidebar .nav > li > a i.arrow:before {\n  content: \"\" !important; }\n.sidebar .nav > li > ul > li a {\n  padding-top: 10px;\n  padding-bottom: 10px;\n  padding-left: 50px;\n  background-color: #333e48; }\n.sidebar .nav > li > ul > li.active a {\n  color: #fff; }\n.sidebar-overlay {\n  position: absolute;\n  display: none;\n  left: 200vw;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  z-index: 5;\n  opacity: 0;\n  transition: opacity .3s ease;\n  z-index: 15; }\n@media (max-width: 991px) {\n  .sidebar-overlay {\n    display: block; } }\n@media (max-width: 767px) {\n  .sidebar-overlay {\n    background-color: rgba(0, 0, 0, 0.7); } }\n@media (max-width: 991px) {\n  .sidebar-open .sidebar-overlay {\n    left: 0;\n    opacity: 1; } }\n#modal-media .modal-body {\n  min-height: 250px; }\n#modal-media .modal-tab-content {\n  min-height: 300px; }\n#modal-media .images-container {\n  padding: 15px;\n  text-align: center; }\n#modal-media .images-container .image-container {\n  margin: 0 auto 10px auto;\n  cursor: pointer;\n  transition: all .3s ease;\n  display: inline-block;\n  float: none; }\n#modal-media .images-container .image-container:hover {\n  border-color: rgba(82, 188, 211, 0.5); }\n#modal-media .images-container .image-container.active {\n  border-color: rgba(82, 188, 211, 0.5); }\n#modal-media .upload-container {\n  padding: 15px; }\n#modal-media .upload-container .dropzone {\n  position: relative;\n  border: 2px dashed #52bcd3;\n  height: 270px; }\n#modal-media .upload-container .dropzone .dz-message-block {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translateY(-50%) translateX(-50%);\n  transform: translateY(-50%) translateX(-50%); }\n#modal-media .upload-container .dropzone .dz-message-block .dz-message {\n  margin: 0;\n  font-size: 24px;\n  color: #52bcd3;\n  width: 230px; }\n.header .header-block-buttons {\n  text-align: center;\n  margin-left: auto;\n  margin-right: auto;\n  white-space: nowrap; }\n.header .header-block-buttons .btn.header-btn {\n  background-color: transparent;\n  border: 1px solid #64798d;\n  color: #64798d;\n  margin: 0 5px; }\n.header .header-block-buttons .btn.header-btn:focus, .header .header-block-buttons .btn.header-btn:hover {\n  border: 1px solid #3a4651;\n  color: #3a4651; }\n@media (max-width: 767px) {\n  .header .header-block-buttons {\n    display: none; } }\n.header .header-block-collapse {\n  padding-right: 5px; }\n.header .header-block-collapse .collapse-btn {\n  background: 0 0;\n  border: none;\n  box-shadow: none;\n  color: #52bcd3;\n  font-size: 24px;\n  line-height: 40px;\n  border-radius: 0;\n  outline: 0;\n  padding: 0;\n  padding-left: 10px;\n  padding-right: 10px;\n  vertical-align: initial; }\n.header .header-block-nav {\n  margin-left: auto;\n  white-space: nowrap; }\n.header .header-block-nav::after {\n  content: \"\";\n  display: table;\n  clear: both; }\n.header .header-block-nav a {\n  text-decoration: none; }\n.header .header-block-nav ul {\n  margin: 0;\n  padding: 0;\n  list-style: none; }\n.header .header-block-nav > ul {\n  display: table; }\n.header .header-block-nav > ul > li {\n  display: table-cell;\n  position: relative; }\n.header .header-block-nav > ul > li:before {\n  display: block;\n  content: \" \";\n  width: 1px;\n  height: 24px;\n  top: 50%;\n  margin-top: -12px;\n  background-color: #8b9cb1;\n  position: absolute;\n  left: 0; }\n.header .header-block-nav > ul > li:first-child:before {\n  display: none; }\n.header .header-block-nav > ul > li > a {\n  padding: 0 15px;\n  color: #4f5f6f; }\n.header .header-block-nav > ul > li > a:hover {\n  color: #52bcd3; }\n.header .header-block-nav .dropdown-menu {\n  margin-top: 15px; }\n.header .header-block-search {\n  margin-right: auto;\n  padding-left: 30px; }\n@media (max-width: 767px) {\n  .header .header-block-search {\n    padding-left: 10px; } }\n@media (min-width: 768px) and (max-width: 991px) {\n  .header .header-block-search {\n    padding-left: 20px; } }\n@media (min-width: 992px) and (max-width: 1199px) {\n  .header .header-block-search {\n    padding-left: 30px; } }\n@media (min-width: 1200px) {\n  .header .header-block-search {\n    padding-left: 50px; } }\n.header .header-block-search > form {\n  float: right; }\n@media (max-width: 767px) {\n  .header .header-block-search > form {\n    padding-left: 0; } }\n.header .header-block-search .input-container {\n  position: relative;\n  color: #7e8e9f; }\n.header .header-block-search .input-container i {\n  position: absolute;\n  pointer-events: none;\n  display: block;\n  height: 40px;\n  line-height: 40px;\n  left: 0; }\n.header .header-block-search .input-container input {\n  background-color: transparent;\n  border: none;\n  padding-left: 25px;\n  height: 40px;\n  max-width: 150px; }\n@media (max-width: 767px) {\n  .header .header-block-search .input-container input {\n    max-width: 140px; } }\n.header .header-block-search .input-container input:focus + .underline {\n  -webkit-transform: scaleX(1);\n  transform: scaleX(1); }\n.sidebar-header .brand {\n  color: #fff;\n  text-align: left;\n  padding-left: 25px;\n  line-height: 70px;\n  font-size: 16px; }\n@media (max-width: 767px) {\n  .sidebar-header .brand {\n    line-height: 50px;\n    font-size: 16px; } }\n.customize {\n  width: 100%;\n  color: rgba(255, 255, 255, 0.5);\n  padding: 5px 15px;\n  text-align: center; }\n.customize .customize-header {\n  margin-bottom: 10px; }\n#customize-menu {\n  position: fixed;\n  bottom: 0;\n  left: 0;\n  width: 230px; }\n@media (max-width: 991px) {\n  .sidebar-open #customize-menu {\n    left: 0; } }\n@media (max-width: 991px) {\n  #customize-menu {\n    transition: left .3s ease;\n    left: -230px; } }\n#customize-menu > li > a {\n  background-color: #3a4651;\n  border-top: 1px solid rgba(45, 54, 63, 0.5); }\n#customize-menu > li.open > a, #customize-menu > li > a:hover {\n  background-color: #2d363f; }\n#customize-menu .customize {\n  width: 230px;\n  color: rgba(255, 255, 255, 0.5);\n  background-color: #2d363f;\n  text-align: center;\n  padding: 10px 15px;\n  border-top: 2px solid #52bcd3; }\n#customize-menu .customize .customize-item {\n  margin-bottom: 15px; }\n#customize-menu .customize .customize-item .customize-header {\n  margin-bottom: 10px; }\n#customize-menu .customize .customize-item label {\n  font-weight: 400; }\n#customize-menu .customize .customize-item label.title {\n  font-size: 14px; }\n#customize-menu .customize .customize-item .radio + span {\n  padding: 0;\n  padding-left: 5px; }\n#customize-menu .customize .customize-item .radio + span:before {\n  font-size: 17px;\n  color: #546273;\n  cursor: pointer; }\n#customize-menu .customize .customize-item .radio:checked + span:before {\n  color: #52bcd3; }\n#customize-menu .customize .customize-item .customize-colors {\n  list-style: none; }\n#customize-menu .customize .customize-item .customize-colors li {\n  display: inline-block;\n  margin-left: 5px;\n  margin-right: 5px; }\n#customize-menu .customize .customize-item .customize-colors li .color-item {\n  display: block;\n  height: 20px;\n  width: 20px;\n  border: 1px solid;\n  cursor: pointer; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-red {\n  background-color: #fb494d;\n  border-color: #fb494d; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-orange {\n  background-color: #fe7a0e;\n  border-color: #fe7a0e; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-green {\n  background-color: #8cde33;\n  border-color: #8cde33; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-seagreen {\n  background-color: #4bcf99;\n  border-color: #4bcf99; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-blue {\n  background-color: #52bcd3;\n  border-color: #52bcd3; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.color-purple {\n  background-color: #7867a7;\n  border-color: #7867a7; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.active {\n  position: relative;\n  font-family: FontAwesome;\n  font-size: 17px;\n  line-height: 17px; }\n#customize-menu .customize .customize-item .customize-colors li .color-item.active:before {\n  content: \"\\f00c\";\n  position: absolute;\n  top: 0;\n  left: 0;\n  color: #fff; }\n.header .header-block-nav .notifications {\n  font-size: 16px; }\n.header .header-block-nav .notifications a {\n  padding-right: 10px; }\n.header .header-block-nav .notifications .counter {\n  font-weight: 700;\n  font-size: 14px;\n  position: relative;\n  top: -3px;\n  left: -2px; }\n.header .header-block-nav .notifications.new .counter {\n  color: #52bcd3;\n  font-weight: 700; }\n@media (max-width: 767px) {\n  .header .header-block-nav .notifications {\n    position: static; } }\n.header .header-block-nav .notifications-dropdown-menu {\n  white-space: normal;\n  left: auto;\n  right: 0;\n  min-width: 350px; }\n.header .header-block-nav .notifications-dropdown-menu:before {\n  position: absolute;\n  right: 20px;\n  bottom: 100%;\n  margin-right: -1px; }\n.header .header-block-nav .notifications-dropdown-menu:after {\n  position: absolute;\n  right: 20px;\n  bottom: 100%; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container .notification-item {\n  border-bottom: 1px solid rgba(126, 142, 159, 0.1);\n  padding: 5px; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container .notification-item:hover {\n  background-color: #f5f5f5; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container .img-col {\n  display: table-cell;\n  padding: 5px; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container .body-col {\n  padding: 5px;\n  display: table-cell; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container .img {\n  width: 40px;\n  height: 40px;\n  border-radius: 3px;\n  vertical-align: top;\n  display: inline-block;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container p {\n  color: #4f5f6f;\n  display: inline-block;\n  line-height: 18px;\n  font-size: 13px;\n  margin: 0;\n  vertical-align: top; }\n.header .header-block-nav .notifications-dropdown-menu .notifications-container p .accent {\n  font-weight: 700; }\n.header .header-block-nav .notifications-dropdown-menu footer {\n  text-align: center; }\n.header .header-block-nav .notifications-dropdown-menu footer a {\n  color: #373a3c;\n  transition: none; }\n.header .header-block-nav .notifications-dropdown-menu footer a:hover {\n  background-color: #f5f5f5;\n  color: #52bcd3; }\n@media (max-width: 767px) {\n  .header .header-block-nav .notifications-dropdown-menu {\n    min-width: 100px;\n    width: 100%;\n    margin-top: 5px; }\n  .header .header-block-nav .notifications-dropdown-menu:after, .header .header-block-nav .notifications-dropdown-menu:before {\n    right: 107px; } }\n.header .header-block-nav .profile .img {\n  display: inline-block;\n  width: 30px;\n  height: 30px;\n  line-height: 30px;\n  border-radius: 4px;\n  background-color: #8b9cb1;\n  color: #fff;\n  text-align: center;\n  margin-right: 10px;\n  background-repeat: no-repeat;\n  background-position: center;\n  background-size: cover;\n  vertical-align: middle; }\n.header .header-block-nav .profile .name {\n  display: inline-block;\n  margin-right: 9px;\n  font-weight: 700; }\n@media (max-width: 767px) {\n  .header .header-block-nav .profile .name {\n    display: none; } }\n.header .header-block-nav .profile .arrow {\n  color: #52bcd3; }\n.header .header-block-nav .profile-dropdown-menu {\n  left: auto;\n  right: 0;\n  min-width: 180px;\n  white-space: normal; }\n.header .header-block-nav .profile-dropdown-menu:before {\n  position: absolute;\n  right: 10px;\n  bottom: 100%;\n  margin-right: -1px; }\n.header .header-block-nav .profile-dropdown-menu:after {\n  position: absolute;\n  right: 10px;\n  bottom: 100%; }\n.header .header-block-nav .profile-dropdown-menu a {\n  padding: 10px 15px; }\n.header .header-block-nav .profile-dropdown-menu a .icon {\n  color: #52bcd3;\n  text-align: center;\n  width: 16px; }\n.header .header-block-nav .profile-dropdown-menu a span {\n  display: inline-block;\n  padding-left: 5px;\n  text-align: left;\n  color: #7e8e9f; }\n.header .header-block-nav .profile-dropdown-menu .profile-dropdown-menu-icon {\n  padding: 0; }\n.header .header-block-nav .profile-dropdown-menu .profile-dropdown-menu-topic {\n  color: #7e8e9f;\n  padding: 0; }\n.header .header-block-nav .profile-dropdown-menu .dropdown-divider {\n  margin: 0; }\n.header .header-block-nav .profile-dropdown-menu .logout {\n  border-top: 1px solid rgba(126, 142, 159, 0.1); }\n@media (max-width: 767px) {\n  .header .header-block-nav .profile-dropdown-menu {\n    margin-top: 8px; } }\n.form-group.has-error .form-help-text {\n  color: #fb494d;\n  font-size: 13px; }\n.form-group.has-error .form-control {\n  border-color: #fb494d; }\n.form-group.has-success .form-help-text {\n  color: #8cde33;\n  font-size: 13px; }\n.n-progress .progress-bar {\n  float: left;\n  width: 0;\n  height: 100%;\n  font-size: 12px;\n  line-height: 20px;\n  color: #211f33;\n  text-align: center;\n  background-color: #337ab7;\n  box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);\n  transition: width .6s ease; }\n.n-progress .progress-bar.success {\n  background-color: #5cb85c; }\n.n-progress .progress-bar.danger {\n  background-color: #f44; }\n.n-progress .progress-bar.warning {\n  background-color: #f0ad4e; }\n.n-progress {\n  height: 20px;\n  margin-bottom: 20px;\n  overflow: hidden;\n  background-color: #f5f5f5;\n  border-radius: 4px;\n  box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1); }\n.planning-expenses {\n  margin-right: 8px;\n  margin-top: 6px; }\n.text-center {\n  text-align: center; }\na {\n  cursor: pointer; }\n"

/***/ }),

/***/ "./src/app/client/components/sidebar/sidebar.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/client/components/sidebar/sidebar.component.ts ***!
  \****************************************************************/
/*! exports provided: SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SidebarComponent = /** @class */ (function () {
    function SidebarComponent() {
    }
    SidebarComponent.prototype.ngOnInit = function () {
    };
    SidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidebar',
            template: __webpack_require__(/*! ./sidebar.component.html */ "./src/app/client/components/sidebar/sidebar.component.html"),
            styles: [__webpack_require__(/*! ./sidebar.component.scss */ "./src/app/client/components/sidebar/sidebar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/core/components/home/best-sellers/best-sellers.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/core/components/home/best-sellers/best-sellers.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"best-sellers\" id=\"#best_sellers\">\n  <div class=\"best-sellers__col\">\n    <div class=\"best-sellers-info\">\n      <div class=\"best-sellers-info__year-heading\">2017's</div>\n      <div class=\"best-sellers-info__year\">Best Selling Products</div>\n      <div class=\"best-sellers-info__slider-controls\">\n        <div class=\"slider-controls\">\n          <button class=\"slider-controls__btn slider-controls__prev\">\n            <i class=\"fas fa-chevron-left\"></i>\n          </button>\n          <button class=\"slider-controls__btn slider-controls__next\">\n            <i class=\"fas fa-chevron-right\"></i>\n          </button>\n        </div>\n        <!-- /.slider-controls -->\n      </div>\n      <!-- /.best-sellers-info__slider-controls -->\n      <a class=\"best-sellers-info__link\" href=\"#\">Full Catalog ></a>\n      <!-- /.best-sellers-info__link -->\n    </div>\n  </div>\n  <!-- /.best-sellers__col -->\n  <div class=\"best-sellers__col\">\n    <div class=\"best-sellers-slides\">\n      <div class=\"best-sellers-slides__slide\">\n        <div class=\"best-sellers-slide\">\n          <img class=\"best-sellers-slide__img\" src=\"assets/img/audio_white.png\" alt=\"\">\n          <a href=\"\" class=\"best-sellers-slide__text\">audio</a>\n          <!-- /.best-sellers-slide__text -->\n        </div>\n        <!-- /.best-sellers-slide -->\n        <div class=\"best-sellers-slide\">\n          <img class=\"best-sellers-slide__img\" src=\"assets/img/tv_premium.png\" alt=\"\">\n          <a href=\"\" class=\"best-sellers-slide__text\">Premium tv</a>\n          <!-- /.best-sellers-slide__text -->\n        </div>\n        <!-- /.best-sellers-slide -->\n        <div class=\"best-sellers-slide\">\n          <img class=\"best-sellers-slide__img\" src=\"assets/img/quadcopter.png\" alt=\"\">\n          <a href=\"\" class=\"best-sellers-slide__text\">gadgets</a>\n          <!-- /.best-sellers-slide__text -->\n        </div>\n        <!-- /.best-sellers-slide -->\n        <div class=\"best-sellers-slide\">\n          <img class=\"best-sellers-slide__img\" src=\"assets/img/notebook.png\" alt=\"\">\n          <a href=\"\" class=\"best-sellers-slide__text\">tablets</a>\n          <!-- /.best-sellers-slide__text -->\n        </div>\n        <!-- /.best-sellers-slide -->\n        <div class=\"best-sellers-slide\">\n          <img class=\"best-sellers-slide__img\" src=\"assets/img/headphones.png\" alt=\"\">\n          <a href=\"\" class=\"best-sellers-slide__text\">Headphones</a>\n          <!-- /.best-sellers-slide__text -->\n        </div>\n        <!-- /.best-sellers-slide -->\n      </div>\n      <!-- /.best-sellers-slides__slide -->\n    </div>\n    <!-- /.best-sellers-slides -->\n  </div>\n  <!-- /.best-sellers__col -->\n</div>\n<!-- /.best-sellers -->\n"

/***/ }),

/***/ "./src/app/core/components/home/best-sellers/best-sellers.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/core/components/home/best-sellers/best-sellers.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".best-sellers {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  height: 370px;\n  padding-top: 30px;\n  width: 95%;\n  max-width: 1760px;\n  margin: 0 auto; }\n  .best-sellers .best-sellers__col {\n    display: flex; }\n  .best-sellers .best-sellers__col .best-sellers-info {\n      display: flex;\n      flex-direction: column;\n      color: #000; }\n  .best-sellers .best-sellers__col .best-sellers-info__year-heading {\n        font-size: 36px;\n        font-family: Poppins;\n        font-weight: 600; }\n  .best-sellers .best-sellers__col .best-sellers-info__year {\n        font-size: 34px;\n        font-family: Poppins;\n        font-weight: 500; }\n  .best-sellers .best-sellers__col .best-sellers-info__slider-controls .slider-controls {\n        display: flex;\n        flex-direction: row;\n        justify-content: flex-start; }\n  .best-sellers .best-sellers__col .best-sellers-info__slider-controls .slider-controls__btn {\n          display: flex;\n          justify-content: center;\n          align-items: center;\n          height: 60px;\n          width: 60px;\n          border-radius: 50%;\n          background-color: #f5f5f5;\n          margin-top: 25px;\n          border: 2px solid #eeeeee;\n          font-size: 22px;\n          color: #a2a2a2;\n          cursor: pointer; }\n  .best-sellers .best-sellers__col .best-sellers-info__slider-controls .slider-controls__btn:active, .best-sellers .best-sellers__col .best-sellers-info__slider-controls .slider-controls__btn:hover, .best-sellers .best-sellers__col .best-sellers-info__slider-controls .slider-controls__btn:focus {\n          outline: 0;\n          outline-offset: 0;\n          background-color: #eeeeee; }\n  .best-sellers .best-sellers__col .best-sellers-info__slider-controls .slider-controls__next {\n          margin-left: 10px; }\n  .best-sellers .best-sellers__col .best-sellers-info__link {\n        font-size: 24px;\n        font-family: Poppins;\n        font-weight: 500;\n        color: #007aff;\n        text-transform: uppercase;\n        margin-top: 30px;\n        text-decoration: none; }\n  .best-sellers .best-sellers__col .best-sellers-slides {\n      display: flex; }\n  .best-sellers .best-sellers__col .best-sellers-slides .best-sellers-slides__slide {\n        display: flex;\n        flex-direction: row;\n        flex-wrap: nowrap;\n        justify-content: space-between; }\n  .best-sellers .best-sellers__col .best-sellers-slides .best-sellers-slides__slide .best-sellers-slide {\n          display: flex;\n          flex-direction: column;\n          justify-content: start;\n          align-items: center;\n          margin: 0 30px 20px; }\n  .best-sellers .best-sellers__col .best-sellers-slides .best-sellers-slides__slide .best-sellers-slide__img {\n            height: 230px; }\n  .best-sellers .best-sellers__col .best-sellers-slides .best-sellers-slides__slide .best-sellers-slide__text {\n            font-size: 24px;\n            font-family: Poppins;\n            font-weight: 600;\n            text-transform: uppercase;\n            color: #000;\n            margin-top: 40px;\n            text-decoration: none; }\n"

/***/ }),

/***/ "./src/app/core/components/home/best-sellers/best-sellers.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/core/components/home/best-sellers/best-sellers.component.ts ***!
  \*****************************************************************************/
/*! exports provided: BestSellersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BestSellersComponent", function() { return BestSellersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BestSellersComponent = /** @class */ (function () {
    function BestSellersComponent() {
    }
    BestSellersComponent.prototype.ngOnInit = function () {
    };
    BestSellersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-best-sellers',
            template: __webpack_require__(/*! ./best-sellers.component.html */ "./src/app/core/components/home/best-sellers/best-sellers.component.html"),
            styles: [__webpack_require__(/*! ./best-sellers.component.scss */ "./src/app/core/components/home/best-sellers/best-sellers.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BestSellersComponent);
    return BestSellersComponent;
}());



/***/ }),

/***/ "./src/app/core/components/home/container-specials/container-specials.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/core/components/home/container-specials/container-specials.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"specials\">\n  <div class=\"specials-item\">\n    <div class=\"special-item__heading\">dji drone Reality Capture</div>\n    <a class=\"special-item__link\" href=\"#\">Discover Now</a>\n    <img class=\"special-item__img\" src=\"assets/img/drone.png\" alt=\"\">\n  </div>\n  <!-- /.specials-item -->\n  <div class=\"specials-item\">\n    <div class=\"special-item__heading\">osmo mobile Beyond Smart</div>\n    <a class=\"special-item__link\" href=\"#\">Discover Now</a>\n    <img class=\"special-item__img\" src=\"assets/img/osmo.png\" alt=\"\">\n  </div>\n  <!-- /.specials-item -->\n  <div class=\"specials-item\">\n    <div class=\"special-item__heading\">s3 flexible Home Speaker</div>\n    <a class=\"special-item__link\" href=\"#\">Discover Now</a>\n    <img class=\"special-item__img\" src=\"assets/img/flexible.png\" alt=\"\">\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/core/components/home/container-specials/container-specials.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/core/components/home/container-specials/container-specials.component.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".specials {\n  width: 95%;\n  max-width: 1760px;\n  margin: 0 auto;\n  height: 435px;\n  background-color: white;\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n  .specials-item {\n    display: flex;\n    flex-direction: column;\n    width: 30vw;\n    justify-content: center; }\n  .specials-item .special-item__heading {\n      margin-left: 40px;\n      display: block;\n      width: 15%;\n      color: #fff;\n      position: absolute;\n      font-size: 28px;\n      font-family: Poppins;\n      font-weight: 300;\n      text-transform: uppercase;\n      z-index: 1; }\n  .specials-item .special-item__link {\n      margin-left: 40px;\n      margin-top: 80px;\n      color: #fff;\n      position: absolute;\n      font-size: 18px;\n      font-family: Poppins;\n      font-weight: 500;\n      text-decoration: underline;\n      text-transform: uppercase;\n      z-index: 1; }\n  .specials-item .special-item__img {\n      position: relative;\n      width: 30vw;\n      height: 33vh; }\n"

/***/ }),

/***/ "./src/app/core/components/home/container-specials/container-specials.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/core/components/home/container-specials/container-specials.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: ContainerSpecialsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContainerSpecialsComponent", function() { return ContainerSpecialsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ContainerSpecialsComponent = /** @class */ (function () {
    function ContainerSpecialsComponent() {
    }
    ContainerSpecialsComponent.prototype.ngOnInit = function () {
    };
    ContainerSpecialsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-container-specials',
            template: __webpack_require__(/*! ./container-specials.component.html */ "./src/app/core/components/home/container-specials/container-specials.component.html"),
            styles: [__webpack_require__(/*! ./container-specials.component.scss */ "./src/app/core/components/home/container-specials/container-specials.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ContainerSpecialsComponent);
    return ContainerSpecialsComponent;
}());



/***/ }),

/***/ "./src/app/core/components/home/home.component.html":
/*!**********************************************************!*\
  !*** ./src/app/core/components/home/home.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-main\">\r\n<app-header></app-header>\r\n  <div class=\"container__slider\">\r\n    <app-slider-big></app-slider-big>\r\n  </div>\r\n  <div class=\"container__specials\">\r\n <app-container-specials></app-container-specials>\r\n  </div>\r\n  <div class=\"container__best-sellers\">\r\n <app-best-sellers></app-best-sellers>\r\n  </div>\r\n  <div class=\"container__month-deals\">\r\n    <app-month-deals></app-month-deals>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/core/components/home/home.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/core/components/home/home.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container__slider {\n  background-color: #eeeeee; }\n"

/***/ }),

/***/ "./src/app/core/components/home/home.component.ts":
/*!********************************************************!*\
  !*** ./src/app/core/components/home/home.component.ts ***!
  \********************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = /** @class */ (function () {
    // tslint:disable-next-line
    function HomeComponent() {
    }
    // tslint:disable-next-line
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/core/components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/core/components/home/home.component.scss")],
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/core/components/home/month-deals/month-deals.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/core/components/home/month-deals/month-deals.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"month-deals\">\n  <div class=\"month-deals__heading\">\n    <div class=\"month-deals__heading-links\">\n      <a href=\"#\" class=\"month-deals__link\">New Arrivals</a>\n      <a href=\"#\" class=\"month-deals__link\">On Sale</a>\n      <a href=\"#\" class=\"month-deals__link\">Best Rated</a>\n      <a href=\"#\" class=\"month-deals__link\">Popular Products</a>\n    </div>\n    <div class=\"month-deals__controls\">\n      <div class=\"month-deals-controls mm-slider\">\n        <div class=\"mm-slider__btn\"><i class=\"fas fa-chevron-left mm-slider__btn_prev\"></i></div>\n        <div class=\"mm-slider__btn\"><i class=\"fas fa-chevron-right mm-slider__btn_next\"></i></div>\n      </div>\n    </div>\n  </div>\n  <div class=\"month-deals__slide month-deals__slide_shown\">\n    <div class=\"month-deals-slide\" *ngFor=\"let p of products\">\n      <div class=\"month-deals-slide__label\">\n        <div class=\"mm-slider__btn\"><i class=\"fas fa-chevron-left mm-slider__btn_prev\"></i></div>\n        <h2 class=\"month-deals-slide__label-head\">Deals of the month</h2>\n        <div class=\"mm-slider__btn\"><i class=\"fas fa-chevron-right mm-slider__btn_next\"></i></div>\n      </div>\n      <img src=\"{{ getImagePath(p.image) }}\" class=\"month-deals-slide__img\" alt=\"\">\n      <div class=\"month-deals-slide__prices\">\n        <div class=\"month-deals-slide__price_new\">${{p.cost}}</div>\n        <div class=\"month-deals-slide__price_old\">$269.00</div>\n      </div>\n      <div class=\"month-deals-slide__heading\">{{p.model}} {{ p.name }}</div>\n      <div class=\"month-deals-slide__avail\">\n        <div class=\"month-deals-slide__available\">\n          <div class=\"month-deals-slide__qty\">Available: </div>\n          <div class=\"month-deals-slide__qty_avail\">{{ p.quantity - p.sold}}</div>\n        </div>\n        <div class=\"month-deals-slide__sold\">\n          <div class=\"month-deals-slide__qty\">Already Sold:</div>\n          <div class=\"month-deals-slide__qty_sold\">{{ p.sold }}</div>\n        </div>\n      </div>\n      <div class=\"progress_sm\">\n        <progress max=\"100\" value={{getPercents(p)}}></progress>\n        <div class=\"progress-value\"></div>\n        <div class=\"progress-bg\">\n          <div class=\"progress-bar\"></div>\n        </div>\n      </div>\n    </div>\n  \n  </div>\n  <div class=\"month-deals__main-indicator\">\n    here circle blue indicator\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/core/components/home/month-deals/month-deals.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/core/components/home/month-deals/month-deals.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".month-deals {\n  width: 95%;\n  max-width: 1760px;\n  margin: 0 auto; }\n  .month-deals__heading {\n    display: flex; }\n  .month-deals__heading-links {\n    display: flex;\n    justify-content: center;\n    flex-grow: 1; }\n  .month-deals__link {\n    display: flex;\n    font-size: 24px;\n    font-family: Poppins;\n    font-weight: 500;\n    color: #a2a2a2;\n    text-decoration: none;\n    padding: 5px 45px 5px 0; }\n  .month-deals__link:hover {\n    color: #007aff; }\n  .month-deals__controls {\n    display: flex;\n    justify-content: flex-end; }\n  .month-deals .month-deals-controls {\n    display: flex; }\n  .month-deals .mm-slider__btn {\n    font-size: 24px;\n    font-family: Poppins;\n    font-weight: 500;\n    color: #a2a2a2;\n    width: 25px; }\n  .month-deals .mm-slider__btn:hover {\n    color: #007aff; }\n  .month-deals__slide {\n    display: flex;\n    flex-direction: row;\n    flex-wrap: wrap;\n    justify-content: space-between;\n    align-items: center;\n    align-content: center;\n    margin-top: 15px;\n    border-top: 2px solid #eeeeee; }\n  .month-deals__slide .month-deals-slide {\n      display: flex;\n      flex-direction: column;\n      justify-content: center;\n      align-items: center;\n      align-content: center;\n      width: 21%;\n      margin-top: 30px;\n      padding: 30px;\n      border-right: 2px solid #eeeeee; }\n  .month-deals__slide .month-deals-slide__label {\n        width: 90%;\n        background-color: white;\n        display: flex;\n        flex-direction: row;\n        flex-wrap: nowrap;\n        align-items: center;\n        align-content: center;\n        justify-content: space-between;\n        font-size: 22px;\n        font-family: Poppins;\n        font-weight: 300;\n        color: #b4b4b4;\n        display: none; }\n  .month-deals__slide .month-deals-slide__label .month-deals-slide__label-head {\n          font-size: 20px;\n          font-family: Poppins;\n          font-weight: 500;\n          color: #000; }\n  .month-deals__slide .month-deals-slide__img {\n        width: 240px;\n        height: 240px; }\n  .month-deals__slide .month-deals-slide__prices {\n        font-size: 18px;\n        font-family: Poppins;\n        font-weight: 500;\n        display: flex; }\n  .month-deals__slide .month-deals-slide__prices .month-deals-slide__price_new {\n          color: #007aff; }\n  .month-deals__slide .month-deals-slide__prices .month-deals-slide__price_old {\n          color: #b4b4b4;\n          display: none; }\n  .month-deals__slide .month-deals-slide__heading {\n        width: 70%;\n        margin: 10px auto 10px;\n        font-size: 20px;\n        font-family: Poppins;\n        font-weight: 500;\n        text-align: center;\n        color: #b4b4b4; }\n  .month-deals__slide .month-deals-slide__avail {\n        width: 98%;\n        display: flex;\n        justify-content: space-between;\n        font-size: 18px;\n        font-family: Poppins;\n        font-weight: 500;\n        color: #b4b4b4; }\n  .month-deals__slide .month-deals-slide__available {\n        display: flex;\n        justify-content: space-between;\n        width: 37%; }\n  .month-deals__slide .month-deals-slide__qty_avail {\n        color: #42D65A;\n        font-size: 18px;\n        font-family: Poppins;\n        font-weight: 500; }\n  .month-deals__slide .month-deals-slide__sold {\n        display: flex;\n        justify-content: space-between;\n        width: 45%; }\n  .month-deals__slide .month-deals-slide__qty_sold {\n        color: #FD3F33;\n        font-size: 18px;\n        font-family: Poppins;\n        font-weight: 500; }\n  .month-deals__slide .month-deals-slide:nth-child(4n) {\n      border-right: none; }\n  .progress_sm progress[value=\"0\"] ~ .progress-bg .progress-bar {\n  width: 0%; }\n  .progress_sm progress[value=\"1\"] ~ .progress-bg .progress-bar {\n  width: 1%; }\n  .progress_sm progress[value=\"2\"] ~ .progress-bg .progress-bar {\n  width: 2%; }\n  .progress_sm progress[value=\"3\"] ~ .progress-bg .progress-bar {\n  width: 3%; }\n  .progress_sm progress[value=\"4\"] ~ .progress-bg .progress-bar {\n  width: 4%; }\n  .progress_sm progress[value=\"5\"] ~ .progress-bg .progress-bar {\n  width: 5%; }\n  .progress_sm progress[value=\"6\"] ~ .progress-bg .progress-bar {\n  width: 6%; }\n  .progress_sm progress[value=\"7\"] ~ .progress-bg .progress-bar {\n  width: 7%; }\n  .progress_sm progress[value=\"8\"] ~ .progress-bg .progress-bar {\n  width: 8%; }\n  .progress_sm progress[value=\"9\"] ~ .progress-bg .progress-bar {\n  width: 9%; }\n  .progress_sm progress[value=\"10\"] ~ .progress-bg .progress-bar {\n  width: 10%; }\n  .progress_sm progress[value=\"11\"] ~ .progress-bg .progress-bar {\n  width: 11%; }\n  .progress_sm progress[value=\"12\"] ~ .progress-bg .progress-bar {\n  width: 12%; }\n  .progress_sm progress[value=\"13\"] ~ .progress-bg .progress-bar {\n  width: 13%; }\n  .progress_sm progress[value=\"14\"] ~ .progress-bg .progress-bar {\n  width: 14%; }\n  .progress_sm progress[value=\"15\"] ~ .progress-bg .progress-bar {\n  width: 15%; }\n  .progress_sm progress[value=\"16\"] ~ .progress-bg .progress-bar {\n  width: 16%; }\n  .progress_sm progress[value=\"17\"] ~ .progress-bg .progress-bar {\n  width: 17%; }\n  .progress_sm progress[value=\"18\"] ~ .progress-bg .progress-bar {\n  width: 18%; }\n  .progress_sm progress[value=\"19\"] ~ .progress-bg .progress-bar {\n  width: 19%; }\n  .progress_sm progress[value=\"20\"] ~ .progress-bg .progress-bar {\n  width: 20%; }\n  .progress_sm progress[value=\"21\"] ~ .progress-bg .progress-bar {\n  width: 21%; }\n  .progress_sm progress[value=\"22\"] ~ .progress-bg .progress-bar {\n  width: 22%; }\n  .progress_sm progress[value=\"23\"] ~ .progress-bg .progress-bar {\n  width: 23%; }\n  .progress_sm progress[value=\"24\"] ~ .progress-bg .progress-bar {\n  width: 24%; }\n  .progress_sm progress[value=\"25\"] ~ .progress-bg .progress-bar {\n  width: 25%; }\n  .progress_sm progress[value=\"26\"] ~ .progress-bg .progress-bar {\n  width: 26%; }\n  .progress_sm progress[value=\"27\"] ~ .progress-bg .progress-bar {\n  width: 27%; }\n  .progress_sm progress[value=\"28\"] ~ .progress-bg .progress-bar {\n  width: 28%; }\n  .progress_sm progress[value=\"29\"] ~ .progress-bg .progress-bar {\n  width: 29%; }\n  .progress_sm progress[value=\"30\"] ~ .progress-bg .progress-bar {\n  width: 30%; }\n  .progress_sm progress[value=\"31\"] ~ .progress-bg .progress-bar {\n  width: 31%; }\n  .progress_sm progress[value=\"32\"] ~ .progress-bg .progress-bar {\n  width: 32%; }\n  .progress_sm progress[value=\"33\"] ~ .progress-bg .progress-bar {\n  width: 33%; }\n  .progress_sm progress[value=\"34\"] ~ .progress-bg .progress-bar {\n  width: 34%; }\n  .progress_sm progress[value=\"35\"] ~ .progress-bg .progress-bar {\n  width: 35%; }\n  .progress_sm progress[value=\"36\"] ~ .progress-bg .progress-bar {\n  width: 36%; }\n  .progress_sm progress[value=\"37\"] ~ .progress-bg .progress-bar {\n  width: 37%; }\n  .progress_sm progress[value=\"38\"] ~ .progress-bg .progress-bar {\n  width: 38%; }\n  .progress_sm progress[value=\"39\"] ~ .progress-bg .progress-bar {\n  width: 39%; }\n  .progress_sm progress[value=\"40\"] ~ .progress-bg .progress-bar {\n  width: 40%; }\n  .progress_sm progress[value=\"41\"] ~ .progress-bg .progress-bar {\n  width: 41%; }\n  .progress_sm progress[value=\"42\"] ~ .progress-bg .progress-bar {\n  width: 42%; }\n  .progress_sm progress[value=\"43\"] ~ .progress-bg .progress-bar {\n  width: 43%; }\n  .progress_sm progress[value=\"44\"] ~ .progress-bg .progress-bar {\n  width: 44%; }\n  .progress_sm progress[value=\"45\"] ~ .progress-bg .progress-bar {\n  width: 45%; }\n  .progress_sm progress[value=\"46\"] ~ .progress-bg .progress-bar {\n  width: 46%; }\n  .progress_sm progress[value=\"47\"] ~ .progress-bg .progress-bar {\n  width: 47%; }\n  .progress_sm progress[value=\"48\"] ~ .progress-bg .progress-bar {\n  width: 48%; }\n  .progress_sm progress[value=\"49\"] ~ .progress-bg .progress-bar {\n  width: 49%; }\n  .progress_sm progress[value=\"50\"] ~ .progress-bg .progress-bar {\n  width: 50%; }\n  .progress_sm progress[value=\"51\"] ~ .progress-bg .progress-bar {\n  width: 51%; }\n  .progress_sm progress[value=\"52\"] ~ .progress-bg .progress-bar {\n  width: 52%; }\n  .progress_sm progress[value=\"53\"] ~ .progress-bg .progress-bar {\n  width: 53%; }\n  .progress_sm progress[value=\"54\"] ~ .progress-bg .progress-bar {\n  width: 54%; }\n  .progress_sm progress[value=\"55\"] ~ .progress-bg .progress-bar {\n  width: 55%; }\n  .progress_sm progress[value=\"56\"] ~ .progress-bg .progress-bar {\n  width: 56%; }\n  .progress_sm progress[value=\"57\"] ~ .progress-bg .progress-bar {\n  width: 57%; }\n  .progress_sm progress[value=\"58\"] ~ .progress-bg .progress-bar {\n  width: 58%; }\n  .progress_sm progress[value=\"59\"] ~ .progress-bg .progress-bar {\n  width: 59%; }\n  .progress_sm progress[value=\"60\"] ~ .progress-bg .progress-bar {\n  width: 60%; }\n  .progress_sm progress[value=\"61\"] ~ .progress-bg .progress-bar {\n  width: 61%; }\n  .progress_sm progress[value=\"62\"] ~ .progress-bg .progress-bar {\n  width: 62%; }\n  .progress_sm progress[value=\"63\"] ~ .progress-bg .progress-bar {\n  width: 63%; }\n  .progress_sm progress[value=\"64\"] ~ .progress-bg .progress-bar {\n  width: 64%; }\n  .progress_sm progress[value=\"65\"] ~ .progress-bg .progress-bar {\n  width: 65%; }\n  .progress_sm progress[value=\"66\"] ~ .progress-bg .progress-bar {\n  width: 66%; }\n  .progress_sm progress[value=\"67\"] ~ .progress-bg .progress-bar {\n  width: 67%; }\n  .progress_sm progress[value=\"68\"] ~ .progress-bg .progress-bar {\n  width: 68%; }\n  .progress_sm progress[value=\"69\"] ~ .progress-bg .progress-bar {\n  width: 69%; }\n  .progress_sm progress[value=\"70\"] ~ .progress-bg .progress-bar {\n  width: 70%; }\n  .progress_sm progress[value=\"71\"] ~ .progress-bg .progress-bar {\n  width: 71%; }\n  .progress_sm progress[value=\"72\"] ~ .progress-bg .progress-bar {\n  width: 72%; }\n  .progress_sm progress[value=\"73\"] ~ .progress-bg .progress-bar {\n  width: 73%; }\n  .progress_sm progress[value=\"74\"] ~ .progress-bg .progress-bar {\n  width: 74%; }\n  .progress_sm progress[value=\"75\"] ~ .progress-bg .progress-bar {\n  width: 75%; }\n  .progress_sm progress[value=\"76\"] ~ .progress-bg .progress-bar {\n  width: 76%; }\n  .progress_sm progress[value=\"77\"] ~ .progress-bg .progress-bar {\n  width: 77%; }\n  .progress_sm progress[value=\"78\"] ~ .progress-bg .progress-bar {\n  width: 78%; }\n  .progress_sm progress[value=\"79\"] ~ .progress-bg .progress-bar {\n  width: 79%; }\n  .progress_sm progress[value=\"80\"] ~ .progress-bg .progress-bar {\n  width: 80%; }\n  .progress_sm progress[value=\"81\"] ~ .progress-bg .progress-bar {\n  width: 81%; }\n  .progress_sm progress[value=\"82\"] ~ .progress-bg .progress-bar {\n  width: 82%; }\n  .progress_sm progress[value=\"83\"] ~ .progress-bg .progress-bar {\n  width: 83%; }\n  .progress_sm progress[value=\"84\"] ~ .progress-bg .progress-bar {\n  width: 84%; }\n  .progress_sm progress[value=\"85\"] ~ .progress-bg .progress-bar {\n  width: 85%; }\n  .progress_sm progress[value=\"86\"] ~ .progress-bg .progress-bar {\n  width: 86%; }\n  .progress_sm progress[value=\"87\"] ~ .progress-bg .progress-bar {\n  width: 87%; }\n  .progress_sm progress[value=\"88\"] ~ .progress-bg .progress-bar {\n  width: 88%; }\n  .progress_sm progress[value=\"89\"] ~ .progress-bg .progress-bar {\n  width: 89%; }\n  .progress_sm progress[value=\"90\"] ~ .progress-bg .progress-bar {\n  width: 90%; }\n  .progress_sm progress[value=\"91\"] ~ .progress-bg .progress-bar {\n  width: 91%; }\n  .progress_sm progress[value=\"92\"] ~ .progress-bg .progress-bar {\n  width: 92%; }\n  .progress_sm progress[value=\"93\"] ~ .progress-bg .progress-bar {\n  width: 93%; }\n  .progress_sm progress[value=\"94\"] ~ .progress-bg .progress-bar {\n  width: 94%; }\n  .progress_sm progress[value=\"95\"] ~ .progress-bg .progress-bar {\n  width: 95%; }\n  .progress_sm progress[value=\"96\"] ~ .progress-bg .progress-bar {\n  width: 96%; }\n  .progress_sm progress[value=\"97\"] ~ .progress-bg .progress-bar {\n  width: 97%; }\n  .progress_sm progress[value=\"98\"] ~ .progress-bg .progress-bar {\n  width: 98%; }\n  .progress_sm progress[value=\"99\"] ~ .progress-bg .progress-bar {\n  width: 99%; }\n  .progress_sm progress[value=\"100\"] ~ .progress-bg .progress-bar {\n  width: 100%; }\n  .progress_sm {\n  font: 12px Arial, Tahoma, sans-serif;\n  position: relative;\n  overflow: hidden;\n  width: 98%;\n  margin-top: 15px; }\n  .progress_sm progress {\n    position: absolute;\n    width: 0;\n    height: 0;\n    overflow: hidden;\n    left: -777px; }\n  .progress-value {\n  color: #333;\n  display: block;\n  line-height: 21px;\n  text-align: center; }\n  .progress-bg {\n  background: #e6e9ed;\n  position: relative;\n  height: 8px;\n  border-radius: 5px;\n  overflow: hidden; }\n  @-webkit-keyframes progress_bar {\n  0% {\n    background-position: 0 0; }\n  100% {\n    background-position: -40px 0; } }\n  @keyframes progress_bar {\n  0% {\n    background-position: 0 0; }\n  100% {\n    background-position: -40px 0; } }\n  .progress-bar {\n  overflow: hidden;\n  background: #56ec44;\n  height: 100%;\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 0;\n  transition: width 1s linear; }\n  .progress-bar:after {\n    -webkit-animation: progress_bar 0.8s linear infinite;\n    animation: progress_bar 0.8s linear infinite;\n    background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);\n    background-size: 40px 40px;\n    position: absolute;\n    content: '';\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%; }\n"

/***/ }),

/***/ "./src/app/core/components/home/month-deals/month-deals.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/core/components/home/month-deals/month-deals.component.ts ***!
  \***************************************************************************/
/*! exports provided: MonthDealsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MonthDealsComponent", function() { return MonthDealsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_product_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../shared/services/product.service */ "./src/app/shared/services/product.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MonthDealsComponent = /** @class */ (function () {
    function MonthDealsComponent(productService) {
        this.productService = productService;
        this.products = [];
        this.percents = 0;
        this.imagePath = 'assets/img/';
    }
    MonthDealsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscrbeProduct = this.productService.getProducts()
            .subscribe(function (data) {
            _this.products = data;
            console.log('this.products:', _this.products);
        });
    };
    MonthDealsComponent.prototype.getImagePath = function (imageName) {
        console.log(this.imagePath + imageName);
        return this.imagePath + imageName;
    };
    MonthDealsComponent.prototype.getPercents = function (data) {
        return this.percents = 100 - Math.round(100 * data.sold / data.quantity);
    };
    MonthDealsComponent.prototype.ngOnDestroy = function () {
        if (this.products) {
            this.subscrbeProduct.unsubscribe();
        }
    };
    MonthDealsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-month-deals',
            template: __webpack_require__(/*! ./month-deals.component.html */ "./src/app/core/components/home/month-deals/month-deals.component.html"),
            styles: [__webpack_require__(/*! ./month-deals.component.scss */ "./src/app/core/components/home/month-deals/month-deals.component.scss")],
        }),
        __metadata("design:paramtypes", [_shared_services_product_service__WEBPACK_IMPORTED_MODULE_1__["ProductService"]])
    ], MonthDealsComponent);
    return MonthDealsComponent;
}());

// ngOnInit() {
//   this.s1 = Observable.combineLatest(
//     this.categoriesService.getCategories(),
//     this.eventService.getEvents()
//   ).subscribe((data: [Category[], WFMEvent[]]) => {
//     this.categories = data[0];
//     this.events = data[1];
//
//     this.setOriginalEvents();
//     this.calculateChartData();
//
//     this.isLoaded = true;
//   });
// }


/***/ }),

/***/ "./src/app/core/components/home/slider-big/slider-big.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/core/components/home/slider-big/slider-big.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"slider\">\n  <div class=\"slider__slides\">\n    <div class=\"slides__slide\">\n      <div class=\"slide slide_active\">\n        <div class=\"slide__goods-info\">\n          <div class=\"goods-info\">\n            <div class=\"goods-info__category\">\n              Audio and video\n            </div>\n            <!-- /.goods-info__category -->\n            <div class=\"goods-info__heading\">\n              <div class=\"goods-info__title\">Devialet Gold</div>\n              <!-- /.goods-info__title -->\n              <div class=\"goods-info__brand\">Phantom</div>\n              <!-- /.goods-info__brand -->\n            </div>\n            <!-- /.goods-info__heading -->\n            <div class=\"goods-info__type\">Wireless Speaker</div>\n            <!-- /.goods-info__type -->\n            <button class=\"goods-info__btn\">Discover now</button>\n            <!-- /.goods-info__btn -->\n          </div>\n          <!-- /.goods-info -->\n          <div class=\"slide__goods-img\"><img src=\"assets/img/phantom.png\" alt=\"\"></div>\n          <!-- /.slide__goods-img -->\n        </div>\n        <!-- /.slide__goods-info -->\n      </div>\n      <!-- /.slide slide_active -->\n    </div>\n    <!-- /.slides__slide -->\n  </div>\n  <!-- /.slider__slides -->\n  <div class=\"slider__controls\">\n    <div class=\"slider-controls\">\n      <div class=\"slider_control slider_control--active\"></div>\n      <!-- /.slider slider_control -->\n    </div>\n    <!-- /.slider-controls -->\n  </div>\n  <!-- /.slider__controls -->\n</div>\n<!-- /.slider -->\n"

/***/ }),

/***/ "./src/app/core/components/home/slider-big/slider-big.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/core/components/home/slider-big/slider-big.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".slider {\n  width: 95%;\n  max-width: 1760px;\n  margin: 0 auto;\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  height: 965px; }\n  .slider__slides {\n    width: 90%; }\n  .slider__slides .slides__slide .slide__goods-info {\n      display: flex;\n      flex-direction: row;\n      justify-content: space-between;\n      align-items: center; }\n  .slider__slides .slides__slide .slide__goods-info .goods-info {\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        height: 69%; }\n  .slider__slides .slides__slide .slide__goods-info .goods-info__category {\n          opacity: 0.5;\n          color: #000;\n          font-size: 20px;\n          font-family: Poppins;\n          font-weight: 500;\n          text-transform: uppercase; }\n  .slider__slides .slides__slide .slide__goods-info .goods-info__heading {\n          display: flex; }\n  .slider__slides .slides__slide .slide__goods-info .goods-info__heading .goods-info__title {\n            color: #000;\n            font-size: 48px;\n            font-family: Poppins;\n            font-weight: 500;\n            text-transform: uppercase; }\n  .slider__slides .slides__slide .slide__goods-info .goods-info__heading .goods-info__brand {\n            margin-left: 15px;\n            color: #000;\n            font-size: 48px;\n            font-family: Poppins;\n            font-weight: 300;\n            text-transform: uppercase; }\n  .slider__slides .slides__slide .slide__goods-info .goods-info__type {\n          color: #000;\n          font-size: 42px;\n          font-family: Poppins;\n          font-weight: 300; }\n  .slider__slides .slides__slide .slide__goods-info .goods-info__btn {\n          margin-top: 3%;\n          height: 70px;\n          width: 39%;\n          background: transparent;\n          border: 3px solid #007aff;\n          font-size: 24px;\n          font-family: Poppins;\n          font-weight: 500;\n          color: #007aff;\n          border-radius: 4px;\n          outline: none;\n          cursor: pointer; }\n  .slider__controls {\n    height: 10%; }\n  .slider__controls .slider_control {\n      width: 15px;\n      height: 15px;\n      border: 2px solid #007aff;\n      border-radius: 50%;\n      position: relative;\n      cursor: pointer; }\n  .slider__controls .slider_control:before,\n    .slider__controls .slider_control:after {\n      content: \"\";\n      position: absolute;\n      width: 100%;\n      height: 100%;\n      right: -2px;\n      background-color: inherit;\n      border-radius: inherit;\n      border: inherit; }\n  .slider__controls .slider_control:before {\n      top: 180%; }\n  .slider__controls .slider_control:after {\n      top: -220%; }\n"

/***/ }),

/***/ "./src/app/core/components/home/slider-big/slider-big.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/core/components/home/slider-big/slider-big.component.ts ***!
  \*************************************************************************/
/*! exports provided: SliderBigComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SliderBigComponent", function() { return SliderBigComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SliderBigComponent = /** @class */ (function () {
    function SliderBigComponent() {
    }
    SliderBigComponent.prototype.ngOnInit = function () {
    };
    SliderBigComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-slider-big',
            template: __webpack_require__(/*! ./slider-big.component.html */ "./src/app/core/components/home/slider-big/slider-big.component.html"),
            styles: [__webpack_require__(/*! ./slider-big.component.scss */ "./src/app/core/components/home/slider-big/slider-big.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SliderBigComponent);
    return SliderBigComponent;
}());



/***/ }),

/***/ "./src/app/core/core.module.ts":
/*!*************************************!*\
  !*** ./src/app/core/core.module.ts ***!
  \*************************************/
/*! exports provided: CoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoreModule", function() { return CoreModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_auth_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth/auth.module */ "./src/app/auth/auth.module.ts");
/* harmony import */ var _auth_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../auth/services/auth.service */ "./src/app/auth/services/auth.service.ts");
/* harmony import */ var _client_client_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../client/client.module */ "./src/app/client/client.module.ts");
/* harmony import */ var _shared_app_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared/app-shared.module */ "./src/app/shared/app-shared.module.ts");
/* harmony import */ var _shared_services_product_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared/services/product.service */ "./src/app/shared/services/product.service.ts");
/* harmony import */ var _shared_services_user_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared/services/user.service */ "./src/app/shared/services/user.service.ts");
/* harmony import */ var _shared_components_not_found_page_not_found_page_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./../shared/components/not-found-page/not-found-page.component */ "./src/app/shared/components/not-found-page/not-found-page.component.ts");
/* harmony import */ var _components_home_best_sellers_best_sellers_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/home/best-sellers/best-sellers.component */ "./src/app/core/components/home/best-sellers/best-sellers.component.ts");
/* harmony import */ var _components_home_container_specials_container_specials_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/home/container-specials/container-specials.component */ "./src/app/core/components/home/container-specials/container-specials.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/core/components/home/home.component.ts");
/* harmony import */ var _components_home_month_deals_month_deals_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/home/month-deals/month-deals.component */ "./src/app/core/components/home/month-deals/month-deals.component.ts");
/* harmony import */ var _components_home_slider_big_slider_big_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/home/slider-big/slider-big.component */ "./src/app/core/components/home/slider-big/slider-big.component.ts");
/* harmony import */ var _shared_components_header_currency_currency_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./shared/components/header/currency/currency.component */ "./src/app/core/shared/components/header/currency/currency.component.ts");
/* harmony import */ var _shared_components_header_dropdown_dropdown_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./shared/components/header/dropdown/dropdown.component */ "./src/app/core/shared/components/header/dropdown/dropdown.component.ts");
/* harmony import */ var _shared_components_header_header_cart_header_cart_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./shared/components/header/header-cart/header-cart.component */ "./src/app/core/shared/components/header/header-cart/header-cart.component.ts");
/* harmony import */ var _shared_components_header_header_search_header_search_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./shared/components/header/header-search/header-search.component */ "./src/app/core/shared/components/header/header-search/header-search.component.ts");
/* harmony import */ var _shared_components_header_header_wish_list_header_wish_list_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./shared/components/header/header-wish-list/header-wish-list.component */ "./src/app/core/shared/components/header/header-wish-list/header-wish-list.component.ts");
/* harmony import */ var _shared_components_header_header_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./shared/components/header/header.component */ "./src/app/core/shared/components/header/header.component.ts");
/* harmony import */ var _shared_components_header_langs_langs_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./shared/components/header/langs/langs.component */ "./src/app/core/shared/components/header/langs/langs.component.ts");
/* harmony import */ var _shared_components_header_menu_about_menu_about_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./shared/components/header/menu-about/menu-about.component */ "./src/app/core/shared/components/header/menu-about/menu-about.component.ts");
/* harmony import */ var _shared_components_header_menu_navigation_menu_navigation_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./shared/components/header/menu-navigation/menu-navigation.component */ "./src/app/core/shared/components/header/menu-navigation/menu-navigation.component.ts");
/* harmony import */ var _shared_components_header_shop_info_shop_info_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./shared/components/header/shop-info/shop-info.component */ "./src/app/core/shared/components/header/shop-info/shop-info.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


























/* tslint:disable */
var CoreModule = /** @class */ (function () {
    // tslint:disable-next-line
    function CoreModule(parentModule) {
        if (parentModule) {
            throw new Error('CoreModule is already loaded. Import it in the AppModule only');
        }
    }
    CoreModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClientModule"],
                _shared_app_shared_module__WEBPACK_IMPORTED_MODULE_7__["AppSharedModule"],
                _auth_auth_module__WEBPACK_IMPORTED_MODULE_4__["AuthModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
                _client_client_module__WEBPACK_IMPORTED_MODULE_6__["ClientModule"]
            ],
            declarations: [
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_13__["HomeComponent"],
                _shared_components_header_header_component__WEBPACK_IMPORTED_MODULE_21__["HeaderComponent"],
                _shared_components_header_menu_about_menu_about_component__WEBPACK_IMPORTED_MODULE_23__["MenuAboutComponent"],
                _shared_components_header_header_search_header_search_component__WEBPACK_IMPORTED_MODULE_19__["HeaderSearchComponent"],
                _shared_components_header_header_wish_list_header_wish_list_component__WEBPACK_IMPORTED_MODULE_20__["HeaderWishListComponent"],
                _shared_components_header_header_cart_header_cart_component__WEBPACK_IMPORTED_MODULE_18__["HeaderCartComponent"],
                _shared_components_header_menu_navigation_menu_navigation_component__WEBPACK_IMPORTED_MODULE_24__["MenuNavigationComponent"],
                _shared_components_header_dropdown_dropdown_component__WEBPACK_IMPORTED_MODULE_17__["DropdownComponent"],
                _shared_components_header_currency_currency_component__WEBPACK_IMPORTED_MODULE_16__["CurrencyComponent"],
                _shared_components_header_langs_langs_component__WEBPACK_IMPORTED_MODULE_22__["LangsComponent"],
                _shared_components_header_shop_info_shop_info_component__WEBPACK_IMPORTED_MODULE_25__["ShopInfoComponent"],
                _components_home_container_specials_container_specials_component__WEBPACK_IMPORTED_MODULE_12__["ContainerSpecialsComponent"],
                _components_home_slider_big_slider_big_component__WEBPACK_IMPORTED_MODULE_15__["SliderBigComponent"],
                _components_home_best_sellers_best_sellers_component__WEBPACK_IMPORTED_MODULE_11__["BestSellersComponent"],
                _components_home_month_deals_month_deals_component__WEBPACK_IMPORTED_MODULE_14__["MonthDealsComponent"],
                _shared_components_not_found_page_not_found_page_component__WEBPACK_IMPORTED_MODULE_10__["NotFoundPageComponent"],
            ],
            providers: [
                _shared_services_user_service__WEBPACK_IMPORTED_MODULE_9__["UserService"],
                _auth_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"],
                _shared_services_product_service__WEBPACK_IMPORTED_MODULE_8__["ProductService"]
            ],
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Optional"])()),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["SkipSelf"])()),
        __metadata("design:paramtypes", [CoreModule])
    ], CoreModule);
    return CoreModule;
}());



/***/ }),

/***/ "./src/app/core/shared/components/header/currency/currency.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/core/shared/components/header/currency/currency.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"selector__currency\">\n  <select name=\"currency\">\n    <option value=\"usd\">usd</option>\n    <option value=\"euro\">euro</option>\n    <option value=\"uah\">uah</option>\n  </select>\n</div>\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/currency/currency.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/core/shared/components/header/currency/currency.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".selector__currency {\n  position: relative; }\n  .selector__currency :active, .selector__currency :hover, .selector__currency :focus {\n    border: none;\n    outline: 0;\n    outline-offset: 0; }\n  .selector__currency select {\n    opacity: 0.7;\n    color: #000000;\n    font-family: Poppins;\n    font-size: 20px;\n    font-weight: 500;\n    line-height: 26px;\n    text-transform: uppercase;\n    -webkit-appearance: none;\n    -moz-appearance: none;\n    appearance: none;\n    border: none;\n    background-color: transparent;\n    position: relative;\n    width: 65px;\n    margin-left: 25px; }\n  .selector__currency:after {\n  content: \"\";\n  border-style: solid;\n  opacity: 0.7;\n  border-width: 6px 6px 0 6px;\n  border-color: #333 transparent transparent transparent;\n  pointer-events: none;\n  position: absolute;\n  top: 50%;\n  right: -6%;\n  z-index: 1;\n  -webkit-transform: translate3d(-50%, -50%, 0);\n          transform: translate3d(-50%, -50%, 0); }\n  .selector__currency select:hover {\n  cursor: pointer; }\n  .selector__currency select > option {\n  font-size: 18px;\n  outline: none;\n  background-color: #eeeeee; }\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/currency/currency.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/core/shared/components/header/currency/currency.component.ts ***!
  \******************************************************************************/
/*! exports provided: CurrencyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrencyComponent", function() { return CurrencyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CurrencyComponent = /** @class */ (function () {
    function CurrencyComponent() {
    }
    CurrencyComponent.prototype.ngOnInit = function () {
    };
    CurrencyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-currency',
            template: __webpack_require__(/*! ./currency.component.html */ "./src/app/core/shared/components/header/currency/currency.component.html"),
            styles: [__webpack_require__(/*! ./currency.component.scss */ "./src/app/core/shared/components/header/currency/currency.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CurrencyComponent);
    return CurrencyComponent;
}());



/***/ }),

/***/ "./src/app/core/shared/components/header/dropdown/dropdown.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/core/shared/components/header/dropdown/dropdown.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"menu-list\">\n  <select name=\"categories-menu\" id=\"#\">\n    <option selected>Shop by Category</option>\n    <option value=\"1\">One</option>\n    <option value=\"2\">Two</option>\n    <option value=\"3\">Three</option>\n  </select>\n</div>\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/dropdown/dropdown.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/core/shared/components/header/dropdown/dropdown.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".menu-list {\n  height: 70px;\n  width: 370px;\n  border-radius: 4px;\n  background-color: #007aff;\n  cursor: pointer;\n  position: relative; }\n\n.menu-list:after {\n  content: \"\";\n  border-style: solid;\n  border-width: 7px 7px 0 7px;\n  border-color: #fff transparent transparent transparent;\n  pointer-events: none;\n  position: absolute;\n  top: 50%;\n  right: 30px;\n  z-index: 1;\n  margin-top: -2px; }\n\n.menu-list select {\n  color: #fff;\n  font-size: 21px;\n  font-family: Poppins;\n  font-weight: 500;\n  text-transform: uppercase;\n  -webkit-appearance: none;\n  -moz-appearance: none;\n  appearance: none;\n  height: 100%;\n  width: 100%;\n  padding: 0 30px;\n  background: none;\n  border: none;\n  cursor: pointer; }\n\n.menu-list select option {\n    background-color: #eeeeee;\n    color: #494949; }\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/dropdown/dropdown.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/core/shared/components/header/dropdown/dropdown.component.ts ***!
  \******************************************************************************/
/*! exports provided: DropdownComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DropdownComponent", function() { return DropdownComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DropdownComponent = /** @class */ (function () {
    function DropdownComponent() {
    }
    DropdownComponent.prototype.ngOnInit = function () {
    };
    DropdownComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dropdown',
            template: __webpack_require__(/*! ./dropdown.component.html */ "./src/app/core/shared/components/header/dropdown/dropdown.component.html"),
            styles: [__webpack_require__(/*! ./dropdown.component.scss */ "./src/app/core/shared/components/header/dropdown/dropdown.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DropdownComponent);
    return DropdownComponent;
}());



/***/ }),

/***/ "./src/app/core/shared/components/header/header-cart/header-cart.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/core/shared/components/header/header-cart/header-cart.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  header-cart works!\n</p>\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/header-cart/header-cart.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/core/shared/components/header/header-cart/header-cart.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/core/shared/components/header/header-cart/header-cart.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/core/shared/components/header/header-cart/header-cart.component.ts ***!
  \************************************************************************************/
/*! exports provided: HeaderCartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderCartComponent", function() { return HeaderCartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HeaderCartComponent = /** @class */ (function () {
    function HeaderCartComponent() {
    }
    HeaderCartComponent.prototype.ngOnInit = function () {
    };
    HeaderCartComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header-cart',
            template: __webpack_require__(/*! ./header-cart.component.html */ "./src/app/core/shared/components/header/header-cart/header-cart.component.html"),
            styles: [__webpack_require__(/*! ./header-cart.component.scss */ "./src/app/core/shared/components/header/header-cart/header-cart.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], HeaderCartComponent);
    return HeaderCartComponent;
}());



/***/ }),

/***/ "./src/app/core/shared/components/header/header-search/header-search.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/core/shared/components/header/header-search/header-search.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"search\">\n  <input type=\"text\" class=\"search__textfield\" placeholder=\"Search for products...\"\n         aria-label=\"search_for_products\" aria-describedby=\"search__textfield\">\n  <div class=\"search__select-block\">\n    <select class=\"search__select\" id=\"inputGroupSelect04\">\n      <option selected>All?Categories</option>\n      <option value=\"1\">One</option>\n      <option value=\"2\">Two</option>\n      <option value=\"3\">Three</option>\n    </select>\n  </div>\n  <button class=\"search__btn\" type=\"button\"><i class=\"fas fa-search\"></i></button>\n</div>\n\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/header-search/header-search.component.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/core/shared/components/header/header-search/header-search.component.scss ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".search {\n  display: flex;\n  justify-content: space-between;\n  height: 100%;\n  width: 100%;\n  border: 2px solid #007aff;\n  border-radius: 4px 0 0 4px;\n  width: 200%; }\n  .search :active, .search :hover, .search :focus {\n    border: none;\n    outline: 0;\n    outline-offset: 0; }\n  .search__textfield {\n    height: 98%;\n    width: 60%;\n    margin: 0;\n    border: 0;\n    border: none;\n    padding-left: 22px;\n    opacity: 0.3;\n    color: #000000;\n    font-family: Poppins;\n    font-size: 20px;\n    font-weight: 500;\n    line-height: 26px; }\n  .search__select-block {\n    display: flex;\n    align-items: center;\n    position: relative;\n    height: 100%;\n    width: 26%;\n    border: none;\n    margin-right: -35px;\n    border: 0;\n    padding: 0; }\n  .search__select-block select {\n      height: 100%;\n      width: 100%;\n      opacity: 1;\n      color: #000000;\n      font-family: Poppins;\n      font-size: 20px;\n      font-weight: 500;\n      line-height: 26px;\n      -webkit-appearance: none;\n      -moz-appearance: none;\n      appearance: none;\n      border: none;\n      background-color: transparent;\n      position: relative;\n      cursor: pointer; }\n  .search__select-block select:after {\n      content: \"//\";\n      color: black;\n      height: 10px;\n      width: 10px;\n      position: absolute;\n      display: block;\n      pointer-events: none; }\n  .search__btn {\n    height: 106%;\n    width: 9%;\n    border: none;\n    margin: -2px -10px 0 0;\n    border: 0;\n    padding: 0;\n    background-color: #007aff;\n    color: white;\n    font-size: 30px;\n    border-radius: 0 4px 4px 0;\n    cursor: pointer; }\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/header-search/header-search.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/core/shared/components/header/header-search/header-search.component.ts ***!
  \****************************************************************************************/
/*! exports provided: HeaderSearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderSearchComponent", function() { return HeaderSearchComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HeaderSearchComponent = /** @class */ (function () {
    function HeaderSearchComponent() {
    }
    HeaderSearchComponent.prototype.ngOnInit = function () {
    };
    HeaderSearchComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header-search',
            template: __webpack_require__(/*! ./header-search.component.html */ "./src/app/core/shared/components/header/header-search/header-search.component.html"),
            styles: [__webpack_require__(/*! ./header-search.component.scss */ "./src/app/core/shared/components/header/header-search/header-search.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], HeaderSearchComponent);
    return HeaderSearchComponent;
}());



/***/ }),

/***/ "./src/app/core/shared/components/header/header-wish-list/header-wish-list.component.html":
/*!************************************************************************************************!*\
  !*** ./src/app/core/shared/components/header/header-wish-list/header-wish-list.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  header-wish-list works!\n</p>\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/header-wish-list/header-wish-list.component.scss":
/*!************************************************************************************************!*\
  !*** ./src/app/core/shared/components/header/header-wish-list/header-wish-list.component.scss ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/core/shared/components/header/header-wish-list/header-wish-list.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/core/shared/components/header/header-wish-list/header-wish-list.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: HeaderWishListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderWishListComponent", function() { return HeaderWishListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HeaderWishListComponent = /** @class */ (function () {
    function HeaderWishListComponent() {
    }
    HeaderWishListComponent.prototype.ngOnInit = function () {
    };
    HeaderWishListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header-wish-list',
            template: __webpack_require__(/*! ./header-wish-list.component.html */ "./src/app/core/shared/components/header/header-wish-list/header-wish-list.component.html"),
            styles: [__webpack_require__(/*! ./header-wish-list.component.scss */ "./src/app/core/shared/components/header/header-wish-list/header-wish-list.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], HeaderWishListComponent);
    return HeaderWishListComponent;
}());



/***/ }),

/***/ "./src/app/core/shared/components/header/header.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/core/shared/components/header/header.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container__header\">\n  <div class=\"header\">\n    <div class=\"header__meta-menu\">\n      <div class=\"meta-menu\">\n        <div class=\"meta-menu__selectors\">\n          <app-langs></app-langs>\n          <app-currency></app-currency>\n        </div>\n        <div class=\"meta-menu__menu-about\">\n          <app-menu-about></app-menu-about>\n        </div>\n      </div>\n    </div>\n    <div class=\"header__info\">\n      <div class=\"header-info\">\n        <div class=\"header-info__row\">\n          <div class=\"header-row\">\n            <div class=\"header-row__logo\">\n              <div class=\"logo\">\n                <a href=\"#\" class=\"logo__link\">\n                  <span class=\"logo__bold-text\">Media</span>\n                  <span class=\"logo__plain-text\">market</span>\n                </a>\n              </div>\n              <!-- /.logo -->\n            </div>\n            <!-- /.header-row__logo -->\n            <div class=\"header-row__search\">\n            <app-header-search></app-header-search>\n            </div>\n            <!-- /.header-row__search -->\n            <div class=\"header-row__shop-info\">\n            <app-shop-info></app-shop-info>\n            </div>\n          </div>\n          <!-- /.header-row -->\n        </div>\n        <!-- /.header-info__row -->\n       <!--//////////////////-->\n        <div class=\"header-info__row\">\n          <div class=\"header-row\">\n            <div class=\"header-row__categories\">\n              \n              <app-dropdown></app-dropdown>\n              <!-- /.menu-list -->\n            </div>\n            <!-- /.header-row__categories -->\n            <div class=\"header-row__menu\">\n              <app-menu-navigation></app-menu-navigation>\n            </div>\n            <!-- /.header-row__menu -->\n            <div class=\"header-row__cont\">\n              <div class=\"cont\">\n        \n              </div>\n              <!-- /.cont -->\n            </div>\n            <!-- /.header-row__cont -->\n          </div>\n          <!-- /.header-row -->\n  \n        </div>\n        <!-- /.header-info__row -->\n       <!--//////////////////-->\n      </div>\n      <!-- /.header-info -->\n    </div>\n    <!--/.header__info -->\n  </div>\n  <!-- /.header -->\n</div>\n<!-- /.container__header -->\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/header.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/core/shared/components/header/header.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header__meta-menu {\n  background-color: #eeeeee; }\n  .header__meta-menu .meta-menu {\n    height: 68px;\n    width: 95%;\n    max-width: 1760px;\n    margin: 0 auto;\n    display: flex;\n    justify-content: space-between; }\n  .header__meta-menu .meta-menu__selectors {\n      display: flex;\n      align-items: center; }\n  .header__meta-menu .meta-menu__selectors :active, .header__meta-menu .meta-menu__selectors :hover, .header__meta-menu .meta-menu__selectors :focus {\n        border: none;\n        outline: 0;\n        outline-offset: 0; }\n  .header__info .header-info__row .header-row {\n  height: 97px;\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  width: 95%;\n  max-width: 1760px;\n  margin: 0 auto; }\n  .header__info .header-info__row .header-row__logo .logo {\n    cursor: pointer; }\n  .header__info .header-info__row .header-row__logo .logo__link {\n      text-decoration: none;\n      font-size: 32px;\n      font-family: Poppins;\n      font-weight: 500;\n      color: #000;\n      display: block;\n      padding: 29px 0; }\n  .header__info .header-info__row .header-row__logo .logo__bold-text {\n      font-weight: 600; }\n  .header__info .header-info__row .header-row__logo .logo__plain-text {\n      font-weight: 400; }\n  .header__info .header-info__row .header-row__search {\n    display: flex;\n    justify-content: space-between;\n    width: 50vw;\n    height: 67px;\n    margin-bottom: -10px; }\n  .header__info .header-info__row .header-row__shop-info {\n    width: 20%; }\n  .header__info .header-info__row .header-row__menu {\n    width: 73%; }\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/header.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/core/shared/components/header/header.component.ts ***!
  \*******************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/core/shared/components/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/core/shared/components/header/header.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/core/shared/components/header/langs/langs.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/core/shared/components/header/langs/langs.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"selector__langs\">\n  <select name=\"langs\">\n    <option value=\"en\">en</option>\n    <option value=\"ru\">ru</option>\n    <option value=\"ua\">ua</option>\n  </select>\n</div>\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/langs/langs.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/core/shared/components/header/langs/langs.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".selector__langs {\n  position: relative; }\n  .selector__langs :active, .selector__langs :hover, .selector__langs :focus {\n    border: none;\n    outline: 0;\n    outline-offset: 0; }\n  .selector__langs select {\n    opacity: 0.7;\n    color: #000000;\n    font-family: Poppins;\n    font-size: 20px;\n    font-weight: 500;\n    line-height: 26px;\n    text-transform: uppercase;\n    -webkit-appearance: none;\n    -moz-appearance: none;\n    appearance: none;\n    border: none;\n    background-color: transparent;\n    position: relative;\n    width: 50px; }\n  .selector__langs:after {\n  content: \"\";\n  border-style: solid;\n  opacity: 0.7;\n  border-width: 6px 6px 0 6px;\n  border-color: #333 transparent transparent transparent;\n  pointer-events: none;\n  position: absolute;\n  top: 50%;\n  right: -6%;\n  z-index: 1;\n  -webkit-transform: translate3d(-50%, -50%, 0);\n          transform: translate3d(-50%, -50%, 0); }\n  .selector__langs select:hover {\n  cursor: pointer; }\n  .selector__langs select > option {\n  font-size: 18px;\n  outline: none;\n  background-color: #eeeeee; }\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/langs/langs.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/core/shared/components/header/langs/langs.component.ts ***!
  \************************************************************************/
/*! exports provided: LangsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LangsComponent", function() { return LangsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LangsComponent = /** @class */ (function () {
    function LangsComponent() {
    }
    LangsComponent.prototype.ngOnInit = function () {
    };
    LangsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-langs',
            template: __webpack_require__(/*! ./langs.component.html */ "./src/app/core/shared/components/header/langs/langs.component.html"),
            styles: [__webpack_require__(/*! ./langs.component.scss */ "./src/app/core/shared/components/header/langs/langs.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], LangsComponent);
    return LangsComponent;
}());



/***/ }),

/***/ "./src/app/core/shared/components/header/menu-about/menu-about.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/core/shared/components/header/menu-about/menu-about.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <div class=\"menu-about\">\n    <ul class=\"menu-about__list\">\n        <li class=\"menu-about__item\">\n            <a routerLink=\"/\" class=\"menu-about__link\">Home</a>\n        </li>\n        <li class=\"menu-about__item\">\n            <a href=\"#\" class=\"menu-about__link\">Help & Contact</a>\n        </li>\n        <li class=\"menu-about__item\">\n            <a href=\"#\" class=\"menu-about__link\">Order Status</a>\n        </li>\n        <li class=\"menu-about__item\">\n        <a [routerLink]=\"['/auth','login']\" class=\"menu-about__link\">My Account </a>\n        </li>\n    </ul>\n  </div>\n\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/menu-about/menu-about.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/core/shared/components/header/menu-about/menu-about.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".menu-about {\n  display: flex; }\n  .menu-about__list {\n    display: flex;\n    margin: 0;\n    padding: 0;\n    list-style: none; }\n  .menu-about__item {\n    margin-left: 48px; }\n  .menu-about__link {\n    display: block;\n    opacity: 0.7;\n    color: #000000;\n    font-family: Poppins;\n    font-size: 20px;\n    font-weight: 500;\n    line-height: 26px;\n    padding: 17px 0 25px;\n    text-decoration: none; }\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/menu-about/menu-about.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/core/shared/components/header/menu-about/menu-about.component.ts ***!
  \**********************************************************************************/
/*! exports provided: MenuAboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuAboutComponent", function() { return MenuAboutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MenuAboutComponent = /** @class */ (function () {
    function MenuAboutComponent() {
    }
    MenuAboutComponent.prototype.ngOnInit = function () {
    };
    MenuAboutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-menu-about',
            template: __webpack_require__(/*! ./menu-about.component.html */ "./src/app/core/shared/components/header/menu-about/menu-about.component.html"),
            styles: [__webpack_require__(/*! ./menu-about.component.scss */ "./src/app/core/shared/components/header/menu-about/menu-about.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MenuAboutComponent);
    return MenuAboutComponent;
}());



/***/ }),

/***/ "./src/app/core/shared/components/header/menu-navigation/menu-navigation.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/core/shared/components/header/menu-navigation/menu-navigation.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"menu\">\n  <ul class=\"menu__topmenu\">\n    <li class=\"menu-item\"><a class=\"menu-item__link\" href=\"#\">Home\n    </a>\n      <i class=\"fas fa-angle-down menu-item_fa-angle-down\"></i>\n      <ul class=\"menu-item__submenu\">\n        <li class=\"submenu-item\"><a class=\"submenu-item__link\"\n                                    href=\"#\">Home_1</a>\n        </li>\n        <li class=\"submenu-item\"><a class=\"submenu-item__link\"\n                                    href=\"#\">Home_2</a>\n        </li>\n        <li class=\"submenu-item\"><a class=\"submenu-item__link\"\n                                    href=\"#\">Home_3</a>\n        </li>\n      </ul>\n    </li>\n    <li class=\"menu-item\"><a class=\"menu-item__link\" href=\"#\">Shop\n    </a>\n      <i class=\"fas fa-angle-down menu-item_fa-angle-down\"></i>\n      \n      <ul class=\"menu-item__submenu\">\n        <li class=\"submenu-item\"><a class=\"submenu-item__link\"\n                                    href=\"#\">Shop_1</a></li>\n        <li class=\"submenu-item\"><a class=\"submenu-item__link\"\n                                    href=\"#\">Shop_2</a></li>\n        <li class=\"submenu-item\"><a class=\"submenu-item__link\"\n                                    href=\"#\">Shop_3</a></li>\n      </ul>\n    </li>\n    <li class=\"menu-item\"><a class=\"menu-item__link\" href=\"#\">Blog</a>\n      <i class=\"fas fa-angle-down menu-item_fa-angle-down\"></i>\n      <ul class=\"menu-item__submenu\">\n        <li class=\"submenu-item\"><a class=\"submenu-item__link\"\n                                    href=\"#\">Blog_1</a>\n        </li>\n        <li class=\"submenu-item\"><a class=\"submenu-item__link\"\n                                    href=\"#\">Blog_2</a>\n        </li>\n        <li class=\"submenu-item\"><a class=\"submenu-item__link\"\n                                    href=\"#\">Blog_3</a>\n        </li>\n      </ul>\n    </li>\n    <li class=\"menu-item\"><a class=\"menu-item__link\" href=\"#\">Pages</a>\n      <i class=\"fas fa-angle-down menu-item_fa-angle-down\"></i>\n      <ul class=\"menu-item__submenu\">\n        <li class=\"submenu-item\"><a class=\"submenu-item__link\"\n                                    href=\"#\">Pages_1</a>\n        </li>\n        <li class=\"submenu-item\"><a class=\"submenu-item__link\"\n                                    href=\"#\">Pages_2</a>\n        </li>\n        <li class=\"submenu-item\"><a class=\"submenu-item__link\"\n                                    href=\"#\">Pages_3</a>\n        </li>\n      </ul>\n    </li>\n    <li class=\"menu-item\"><a class=\"menu-item__link\" href=\"#\">Products</a></li>\n    <li class=\"menu-item\"><a class=\"menu-item__link\" href=\"#\">Brands</a></li>\n    <li class=\"menu-item\"><a class=\"menu-item__link\" href=\"#\">Today's Deals</a></li>\n    <li class=\"menu-item\"><a class=\"menu-item__link\" href=\"#\">new arrivals</a></li>\n  </ul>\n</nav>\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/menu-navigation/menu-navigation.component.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/core/shared/components/header/menu-navigation/menu-navigation.component.scss ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".menu__topmenu {\n  display: flex;\n  justify-content: space-between;\n  margin: 0;\n  padding: 0;\n  list-style: none; }\n  .menu__topmenu .menu-item {\n    position: relative; }\n  .menu__topmenu .menu-item__link {\n      display: block;\n      text-decoration: none;\n      text-transform: uppercase;\n      font-size: 22px;\n      font-family: Poppins;\n      font-weight: 600;\n      color: #494949;\n      padding: 5px;\n      margin-left: 5px; }\n  .menu__topmenu .menu-item__link:hover,\n    .menu__topmenu .menu-item .menu-item_fa-angle-down:hover {\n      color: #007aff; }\n  .menu__topmenu .menu-item .menu-item_fa-angle-down {\n      color: #494949;\n      position: absolute;\n      right: -15px;\n      top: 30%;\n      cursor: pointer; }\n  .menu__topmenu .menu-item__submenu {\n      margin: 0;\n      padding: 0;\n      list-style: none;\n      background: #ffffff;\n      position: absolute;\n      left: 0;\n      top: 100%;\n      z-index: 5;\n      width: 100%;\n      opacity: 0;\n      -webkit-transform: scaleY(0);\n              transform: scaleY(0);\n      -webkit-transform-origin: 0 0;\n              transform-origin: 0 0;\n      transition: .3s ease-in-out; }\n  .menu__topmenu .menu-item__submenu .submenu-item__link {\n        text-align: left;\n        padding: 10px;\n        text-decoration: none;\n        font-size: 18px;\n        font-family: Poppins;\n        font-weight: 500;\n        color: #494949;\n        border-bottom: 1px solid rgba(255, 255, 255, 0.1); }\n  .menu__topmenu .menu-item__submenu .submenu-item__link:hover {\n        color: #007aff; }\n  .menu__topmenu .menu-item:hover .menu-item__submenu {\n    opacity: 1;\n    -webkit-transform: scaleY(1);\n            transform: scaleY(1); }\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/menu-navigation/menu-navigation.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/core/shared/components/header/menu-navigation/menu-navigation.component.ts ***!
  \********************************************************************************************/
/*! exports provided: MenuNavigationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuNavigationComponent", function() { return MenuNavigationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MenuNavigationComponent = /** @class */ (function () {
    function MenuNavigationComponent() {
    }
    MenuNavigationComponent.prototype.ngOnInit = function () {
    };
    MenuNavigationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-menu-navigation',
            template: __webpack_require__(/*! ./menu-navigation.component.html */ "./src/app/core/shared/components/header/menu-navigation/menu-navigation.component.html"),
            styles: [__webpack_require__(/*! ./menu-navigation.component.scss */ "./src/app/core/shared/components/header/menu-navigation/menu-navigation.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MenuNavigationComponent);
    return MenuNavigationComponent;
}());



/***/ }),

/***/ "./src/app/core/shared/components/header/shop-info/shop-info.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/core/shared/components/header/shop-info/shop-info.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"shop-info\">\n  <!-- /.header-row__info-shop -->\n  <div class=\"shop-info__wishlist\">\n    <div class=\"notification wishlist\">\n      <i class=\"far fa-heart notification__icon\"></i>\n      <div class=\"notification__label\">\n        <span class=\"notification_notice\">0</span>\n        <!-- /.notice -->\n      </div>\n    </div>\n  </div>\n  <!-- /.header__wishlist -->\n  <div class=\"shop-info__cart\">\n    <div class=\"cart-info\">\n      <div class=\"cart-info__cart\">\n        <div class=\"notification cart\">\n          <img class=\"notification__icon\" src=\"assets/fonts/svg/icon-bag.svg\" alt=\"\">\n          <div class=\"notification__label\">\n            <span class=\"notification_notice\">2</span>\n          </div>\n        </div>\n      </div>\n    \n    </div>\n  </div>\n  <!-- /.header__cart -->\n  <div class=\"shop-info__total\">\n    <div class=\"cart__total\">\n      <div class=\"cart-total\">$0.00</div>\n    </div>\n  </div>\n  <!-- /.shop-info__total -->\n</div>\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/shop-info/shop-info.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/core/shared/components/header/shop-info/shop-info.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".shop-info {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n  .shop-info .notification {\n    position: relative;\n    cursor: pointer; }\n  .shop-info .notification__icon {\n      height: 32px;\n      width: 32px;\n      font-size: 32px; }\n  .shop-info .notification__label {\n      height: 32px;\n      width: 32px;\n      font-size: 32px;\n      background-color: #eeeeee;\n      border-radius: 50%;\n      position: absolute;\n      top: 50%;\n      left: 70%;\n      font-size: 18px;\n      font-family: Poppins;\n      font-weight: 500;\n      color: #494949;\n      display: flex;\n      justify-content: center;\n      align-items: center; }\n  .shop-info__total .cart-total {\n    font-size: 24px;\n    font-family: Poppins;\n    font-weight: 500;\n    color: #007aff; }\n"

/***/ }),

/***/ "./src/app/core/shared/components/header/shop-info/shop-info.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/core/shared/components/header/shop-info/shop-info.component.ts ***!
  \********************************************************************************/
/*! exports provided: ShopInfoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShopInfoComponent", function() { return ShopInfoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ShopInfoComponent = /** @class */ (function () {
    function ShopInfoComponent() {
    }
    ShopInfoComponent.prototype.ngOnInit = function () {
    };
    ShopInfoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-shop-info',
            template: __webpack_require__(/*! ./shop-info.component.html */ "./src/app/core/shared/components/header/shop-info/shop-info.component.html"),
            styles: [__webpack_require__(/*! ./shop-info.component.scss */ "./src/app/core/shared/components/header/shop-info/shop-info.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ShopInfoComponent);
    return ShopInfoComponent;
}());



/***/ }),

/***/ "./src/app/shared/app-shared.module.ts":
/*!*********************************************!*\
  !*** ./src/app/shared/app-shared.module.ts ***!
  \*********************************************/
/*! exports provided: AppSharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppSharedModule", function() { return AppSharedModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppSharedModule = /** @class */ (function () {
    function AppSharedModule() {
    }
    AppSharedModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
            ],
            exports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
            ],
            declarations: [],
            providers: [],
        })
    ], AppSharedModule);
    return AppSharedModule;
}());



/***/ }),

/***/ "./src/app/shared/components/not-found-page/not-found-page.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/shared/components/not-found-page/not-found-page.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"not-found\">\n    <div class=\"text-center\">\n        <h1>404</h1>\n        <h1>Страница не найдена</h1>\n        <!--<a routerLink=\"/\" >Перейти на главную</a>-->\n        <a routerLink=\"/\">перейти на главную</a>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/shared/components/not-found-page/not-found-page.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/shared/components/not-found-page/not-found-page.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".not-found {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 100vw;\n  height: 100vh; }\n  .not-found h1 {\n    color: red; }\n"

/***/ }),

/***/ "./src/app/shared/components/not-found-page/not-found-page.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/shared/components/not-found-page/not-found-page.component.ts ***!
  \******************************************************************************/
/*! exports provided: NotFoundPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundPageComponent", function() { return NotFoundPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var NotFoundPageComponent = /** @class */ (function () {
    function NotFoundPageComponent() {
    }
    NotFoundPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-not-found-page',
            template: __webpack_require__(/*! ./not-found-page.component.html */ "./src/app/shared/components/not-found-page/not-found-page.component.html"),
            styles: [__webpack_require__(/*! ./not-found-page.component.scss */ "./src/app/shared/components/not-found-page/not-found-page.component.scss")],
        })
    ], NotFoundPageComponent);
    return NotFoundPageComponent;
}());



/***/ }),

/***/ "./src/app/shared/constants.ts":
/*!*************************************!*\
  !*** ./src/app/shared/constants.ts ***!
  \*************************************/
/*! exports provided: MIN_LENGTH_PASSWORD */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MIN_LENGTH_PASSWORD", function() { return MIN_LENGTH_PASSWORD; });
/*
* Put here all general labels for your app
* for example:
* export const REQUIRED_FIELD_LABEL = 'Required field';
* export const INCORRECT_EMAIL_LABEL = 'Incorrect email';
*/
var MIN_LENGTH_PASSWORD = 6;


/***/ }),

/***/ "./src/app/shared/core/base-api.ts":
/*!*****************************************!*\
  !*** ./src/app/shared/core/base-api.ts ***!
  \*****************************************/
/*! exports provided: BaseApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseApi", function() { return BaseApi; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BaseApi = /** @class */ (function () {
    function BaseApi(http) {
        this.http = http;
        this.BASE_URL = 'http://localhost:3000/';
    }
    BaseApi.prototype.get = function (url) {
        if (url === void 0) { url = ''; }
        return this.http.get(this.getUrl(url));
    };
    BaseApi.prototype.post = function (url, data) {
        if (url === void 0) { url = ''; }
        if (data === void 0) { data = {}; }
        return this.http.post(this.getUrl(url), data);
    };
    BaseApi.prototype.put = function (url, data) {
        if (url === void 0) { url = ''; }
        if (data === void 0) { data = {}; }
        return this.http.put(this.getUrl(url), data);
    };
    BaseApi.prototype.getUrl = function (url) {
        if (url === void 0) { url = ''; }
        return this.BASE_URL + url;
    };
    BaseApi = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]])
    ], BaseApi);
    return BaseApi;
}());



/***/ }),

/***/ "./src/app/shared/models/message.model.ts":
/*!************************************************!*\
  !*** ./src/app/shared/models/message.model.ts ***!
  \************************************************/
/*! exports provided: Message */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Message", function() { return Message; });
var Message = /** @class */ (function () {
    function Message(type, text) {
        this.type = type;
        this.text = text;
    }
    return Message;
}());



/***/ }),

/***/ "./src/app/shared/models/user.model.ts":
/*!*********************************************!*\
  !*** ./src/app/shared/models/user.model.ts ***!
  \*********************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User(email, password, name, id) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.id = id;
    }
    return User;
}());



/***/ }),

/***/ "./src/app/shared/services/product.service.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/services/product.service.ts ***!
  \****************************************************/
/*! exports provided: ProductService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductService", function() { return ProductService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_base_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../core/base-api */ "./src/app/shared/core/base-api.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductService = /** @class */ (function (_super) {
    __extends(ProductService, _super);
    function ProductService(http) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        return _this;
    }
    ProductService.prototype.getProducts = function () {
        return this.get('products');
    };
    ProductService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]])
    ], ProductService);
    return ProductService;
}(_core_base_api__WEBPACK_IMPORTED_MODULE_2__["BaseApi"]));



/***/ }),

/***/ "./src/app/shared/services/user.service.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/services/user.service.ts ***!
  \*************************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _core_base_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/base-api */ "./src/app/shared/core/base-api.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserService = /** @class */ (function (_super) {
    __extends(UserService, _super);
    function UserService(httpClient) {
        var _this = _super.call(this, httpClient) || this;
        _this.httpClient = httpClient;
        return _this;
    }
    // public getUserByEmail(email: string): Observable<User> {
    //   return this.httpClient.get<User[]>(`http://localhost:3000/users?email=${email}`)
    //     .pipe(map((user: User[]) => user[0] ? user[0] : undefined));
    // }
    UserService.prototype.getUserByEmail = function (email) {
        return this.get("users?email=" + email)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (user) { return user[0] ? user[0] : undefined; }));
    };
    // public createNewUser(user: User): Observable<User> {
    //   return this.httpClient.post<User>(`http://localhost:3000/users`, user);
    //
    // }
    UserService.prototype.createNewUser = function (user) {
        return this.post("users", user);
    };
    UserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]])
    ], UserService);
    return UserService;
}(_core_base_api__WEBPACK_IMPORTED_MODULE_3__["BaseApi"]));



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    backEndUrl: 'http://example.dev.com/api/',
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* tslint:disable */




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])()
    .bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\Git\shopmediamarket\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map