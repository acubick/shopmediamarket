import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {AppSharedModule} from '../shared/app-shared.module';
import {ClientComponent} from './client.component';
import {ClientRoutingModule} from './client.routing';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';

@NgModule({
  imports: [CommonModule, ClientRoutingModule, AppSharedModule],
  declarations: [ClientComponent, HeaderComponent, SidebarComponent],
  providers: [],
})
export class ClientModule {
}
