export class Product {
  constructor(
    public name: string,
    public category: string,
    public cost: number,
    public quantity: number,
    public sold: number,
    public model: string,
    public image: string,
    public id?: number,
  ) {
  }
}
