import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {BaseApi} from '../core/base-api';

import {User} from '../models/user.model';

@Injectable()
export class UserService extends BaseApi {
  constructor(public httpClient: HttpClient) {
    super(httpClient);
  }

  // public getUserByEmail(email: string): Observable<User> {
  //   return this.httpClient.get<User[]>(`http://localhost:3000/users?email=${email}`)
  //     .pipe(map((user: User[]) => user[0] ? user[0] : undefined));
  // }

  public getUserByEmail(email: string): Observable<User> {
    return this.get(`users?email=${email}`)
      .pipe(map((user: User[]) => user[0] ? user[0] : undefined));
  }

  // public createNewUser(user: User): Observable<User> {
  //   return this.httpClient.post<User>(`http://localhost:3000/users`, user);
  //
  // }
  public createNewUser(user: User): Observable<User> {
    return this.post(`users`, user);

  }
}
