import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {BaseApi} from '../core/base-api';
import {Product} from '../models/product.model';

@Injectable()
export class ProductService extends BaseApi {


  constructor(public http: HttpClient) {
    super(http);
  }


  public getProducts(): Observable<Product[]> {
    return this.get('products');
  }

}
