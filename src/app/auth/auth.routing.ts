﻿import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
/* tslint:disable */
import {AuthComponent} from './auth.component';
import {LoginComponent} from './components/login/login.component';
import {RegistrationComponent} from './components/registration/registration.component';

const AUTH_ROUTES: Routes = [
  // {path: 'login', component: LoginComponent},
  // {path: 'registration', component: RegistrationComponent}
  {
    path: '', component: AuthComponent, children: [
      {path: '', redirectTo: 'login'},
      {path: 'login', component: LoginComponent},
      {path: 'registration', component: RegistrationComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(AUTH_ROUTES)],
  exports: [RouterModule],
})
export class AuthRoutingModule {
}
