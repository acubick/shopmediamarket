﻿import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {AppSharedModule} from '../shared/app-shared.module';
import {AuthComponent} from './auth.component';
import {AuthRoutingModule} from './auth.routing';
import {LoginComponent} from './components/login/login.component';
import {RegistrationComponent} from './components/registration/registration.component';

/* tslint:disable */
@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    AppSharedModule
  ],
  declarations: [AuthComponent, LoginComponent, RegistrationComponent],
  providers: [],
})
export class AuthModule {
}
