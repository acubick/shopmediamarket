import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';

import {MIN_LENGTH_PASSWORD} from '../../../shared/constants';
import {asyncExample} from '../../../validators/async-example.validator';
import {onlyLetters} from '../../../validators/only-letters.validators';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {
  
  public registrationForm: FormGroup;


  constructor(private formBuilder: FormBuilder) {
    this.buildForm();
  }


  public ngOnInit(): void {
  }

  public onSubmit() {
    console.log(this.registrationForm);
  }

  public get username(): AbstractControl {
    console.log((this.registrationForm as FormGroup).controls.username);
    return (this.registrationForm as FormGroup).controls.username;
  }

  private buildForm() {
    this.registrationForm = this.formBuilder.group({
      username: ['yourName', [Validators.required, onlyLetters], asyncExample],
      email: ['your.email@gmail.com',
        {
          updateOn: 'blur',
          validators: [Validators.required, Validators.email],
          asyncValidators: null,
        },
      ],
      password: ['', [Validators.required, Validators.minLength(MIN_LENGTH_PASSWORD)]],
      agree: ['', Validators.requiredTrue],
    });
    // console.table(this.registrationForm);
  }

}
