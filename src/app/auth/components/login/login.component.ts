import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {MIN_LENGTH_PASSWORD} from '../../../shared/constants';

import {Message} from '../../../shared/models/message.model';
import {User} from '../../../shared/models/user.model';
import {UserService} from '../../../shared/services/user.service';
import {AuthService} from '../../../core/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public message: Message;
  public form: FormGroup;

  constructor(
    private userService: UserService,
    private authServise: AuthService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
  }

  public ngOnInit() {
    this.message = new Message('danger', ''),
      this.route.queryParams
        .subscribe(
          (params: Params) => {
            console.log('few', params);
            if (params.nowCanLogin) {
              this.showMessage({
                text: 'Теперь вы можуте войти в систему',
                type: 'success',
              });
            }
          }),

      this.form = new FormGroup({
        email: new FormControl(null, [Validators.required, Validators.email]),
        password: new FormControl(null, [Validators.required, Validators.minLength(MIN_LENGTH_PASSWORD)]),
      });
  }

  public onSubmit() {
    // console.log('this form value:', this.form.value)
    const formData = this.form.value;
    this.userService.getUserByEmail(formData.email)
      .subscribe((user: User) => {
        if (user) {
          if (user.password === formData.password) {
            this.message.text = '';
            window.localStorage.setItem('user', JSON.stringify(user));
            this.authServise.login();
            // this.router.navigate(['']);
            console.log('вы залогинились');
          } else {
            this.showMessage({
              text: 'Пароль неверный',
              type: 'danger',
            });
          }
        } else {
          this.showMessage({
            text: 'Такого пользователя не существует',
            type: 'danger',
          });
        }
      });
  }

  private showMessage(message: Message) {
    // console.log(this, type, text);
    this.message = message;
    window.setTimeout(() => {
      this.message.text = '';
    }, 3000);
  }
}
