import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

import {MIN_LENGTH_PASSWORD} from '../../../shared/constants';
import {User} from '../../../shared/models/user.model';
import {UserService} from '../../../shared/services/user.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {

  public form: FormGroup;

  constructor(
    private userService: UserService,
    private router: Router,
  ) {
  }

  public ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmails.bind(this)),
      password: new FormControl(null, [Validators.required, Validators.minLength(MIN_LENGTH_PASSWORD)]),
      name: new FormControl(null, [Validators.required]),
      agree: new FormControl(false, [Validators.requiredTrue]),
    });
  }

  public onSubmit() {
    const {email, password, name} = this.form.value;
    const user = new User(email, password, name);

    // console.log(this.form);
    this.userService.createNewUser(user)
      .subscribe(() => {
        // console.log(user);
        this.router.navigate(['login'], {
          queryParams: {
            nowCanLogin: true,
          },
        });
      });
  }

  public forbiddenEmails(control: FormControl): Promise<any> {
    return new Promise((resolve, reject) => {
      this.userService.getUserByEmail(control.value)
        .subscribe((user: User) => {
          if (user) {
            resolve({forbiddenEmail: true});
          } else {
            resolve(null);
          }
        });
    });
  }

}
