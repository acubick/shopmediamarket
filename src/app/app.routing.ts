﻿import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClientComponent} from './client/client.component';

import {HomeComponent} from './core/components/home/home.component';
import {NotFoundPageComponent} from './shared/components/not-found-page/not-found-page.component';
/* tslint:disable */

const routes: Routes = [
{path: '', component: HomeComponent},
{path: 'client', component: ClientComponent},
{path: 'auth', loadChildren: 'app/auth/auth.module#AuthModule'},
{path: 'not-found-page', component: NotFoundPageComponent},
{path: '**', redirectTo: '/not-found-page'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
