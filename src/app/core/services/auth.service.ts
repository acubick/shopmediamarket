import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private isAuthentificated: boolean;

  public isLoggedIn(): boolean {
    return this.isAuthentificated;
  }

  public login() {
    this.isAuthentificated = true;
  }

  public logout() {
    this.isAuthentificated = false;
    window.localStorage.clear();
  }
}
