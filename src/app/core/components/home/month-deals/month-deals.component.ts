import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {Product} from '../../../../shared/models/product.model';
import {ProductService} from '../../../../shared/services/product.service';

@Component({
  selector: 'app-month-deals',
  templateUrl: './month-deals.component.html',
  styleUrls: ['./month-deals.component.scss'],
})
export class MonthDealsComponent implements OnInit, OnDestroy {
  public subscribeProduct: Subscription;
  public products: Product[] = [];
  public percents = 0;
  private imagePath = 'assets/img/';

  constructor(private productService: ProductService) {
  }

  public ngOnInit() {
    this.subscribeProduct = this.productService.getProducts()
      .subscribe((data: Product[]) => {
        this.products = data;
        // console.log('this.products:', this.products);
      });
  }
  public getImagePath(imageName: string): string {
    // console.log(this.imagePath + imageName);
    return this.imagePath + imageName;
  }

  public getPercents(data: Product){

    return this.percents = 100 - Math.round(100 * data.sold / data.quantity);
  }

  public ngOnDestroy() {
    if (this.products) {
      this.subscribeProduct.unsubscribe();
    }
  }
}

// ngOnInit() {
//   this.s1 = Observable.combineLatest(
//     this.categoriesService.getCategories(),
//     this.eventService.getEvents()
//   ).subscribe((data: [Category[], WFMEvent[]]) => {
//     this.categories = data[0];
//     this.events = data[1];
//
//     this.setOriginalEvents();
//     this.calculateChartData();
//
//     this.isLoaded = true;
//   });
// }
