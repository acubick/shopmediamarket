import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {AppSharedModule} from '../shared/app-shared.module';
import {AdministratorComponent} from './administrator.component';
import {AdministratorRoutingModule} from './administrator.routing';

@NgModule({
  declarations: [AdministratorComponent],
  imports: [CommonModule, AppSharedModule, AdministratorRoutingModule],
  providers: [],
})
export class AdministratorModule {
}
