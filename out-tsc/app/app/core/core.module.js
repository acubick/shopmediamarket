var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ClientModule } from '../client/client.module';
import { AppSharedModule } from '../shared/app-shared.module';
import { NotFoundPageComponent } from '../shared/components/not-found-page/not-found-page.component';
import { ProductService } from '../shared/services/product.service';
import { UserService } from '../shared/services/user.service';
import { BestSellersComponent } from './components/home/best-sellers/best-sellers.component';
import { ContainerSpecialsComponent } from './components/home/container-specials/container-specials.component';
import { HomeComponent } from './components/home/home.component';
import { MonthDealsComponent } from './components/home/month-deals/month-deals.component';
import { SliderBigComponent } from './components/home/slider-big/slider-big.component';
import { CurrencyComponent } from './shared/components/header/currency/currency.component';
import { DropdownComponent } from './shared/components/header/dropdown/dropdown.component';
import { HeaderCartComponent } from './shared/components/header/header-cart/header-cart.component';
import { HeaderSearchComponent } from './shared/components/header/header-search/header-search.component';
import { HeaderWishListComponent } from './shared/components/header/header-wish-list/header-wish-list.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { LangsComponent } from './shared/components/header/langs/langs.component';
import { MenuAboutComponent } from './shared/components/header/menu-about/menu-about.component';
import { MenuNavigationComponent } from './shared/components/header/menu-navigation/menu-navigation.component';
import { ShopInfoComponent } from './shared/components/header/shop-info/shop-info.component';
/* tslint:disable */
var CoreModule = /** @class */ (function () {
    // tslint:disable-next-line
    function CoreModule(parentModule) {
        if (parentModule) {
            throw new Error('CoreModule is already loaded. Import it in the AppModule only');
        }
    }
    CoreModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                HttpClientModule,
                ReactiveFormsModule,
                AppSharedModule,
                RouterModule,
                ClientModule,
            ],
            declarations: [
                HomeComponent,
                HeaderComponent,
                MenuAboutComponent,
                HeaderSearchComponent,
                HeaderWishListComponent,
                HeaderCartComponent,
                MenuNavigationComponent,
                DropdownComponent,
                CurrencyComponent,
                LangsComponent,
                ShopInfoComponent,
                ContainerSpecialsComponent,
                SliderBigComponent,
                BestSellersComponent,
                MonthDealsComponent,
                NotFoundPageComponent,
            ],
            providers: [
                UserService,
                ProductService
            ],
        }),
        __param(0, Optional()),
        __param(0, SkipSelf()),
        __metadata("design:paramtypes", [CoreModule])
    ], CoreModule);
    return CoreModule;
}());
export { CoreModule };
//# sourceMappingURL=core.module.js.map