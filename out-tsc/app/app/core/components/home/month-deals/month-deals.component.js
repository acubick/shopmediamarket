var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ProductService } from '../../../../shared/services/product.service';
var MonthDealsComponent = /** @class */ (function () {
    function MonthDealsComponent(productService) {
        this.productService = productService;
        this.products = [];
        this.percents = 0;
        this.imagePath = 'assets/img/';
    }
    MonthDealsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscribeProduct = this.productService.getProducts()
            .subscribe(function (data) {
            _this.products = data;
            // console.log('this.products:', this.products);
        });
    };
    MonthDealsComponent.prototype.getImagePath = function (imageName) {
        // console.log(this.imagePath + imageName);
        return this.imagePath + imageName;
    };
    MonthDealsComponent.prototype.getPercents = function (data) {
        return this.percents = 100 - Math.round(100 * data.sold / data.quantity);
    };
    MonthDealsComponent.prototype.ngOnDestroy = function () {
        if (this.products) {
            this.subscribeProduct.unsubscribe();
        }
    };
    MonthDealsComponent = __decorate([
        Component({
            selector: 'app-month-deals',
            templateUrl: './month-deals.component.html',
            styleUrls: ['./month-deals.component.scss'],
        }),
        __metadata("design:paramtypes", [ProductService])
    ], MonthDealsComponent);
    return MonthDealsComponent;
}());
export { MonthDealsComponent };
// ngOnInit() {
//   this.s1 = Observable.combineLatest(
//     this.categoriesService.getCategories(),
//     this.eventService.getEvents()
//   ).subscribe((data: [Category[], WFMEvent[]]) => {
//     this.categories = data[0];
//     this.events = data[1];
//
//     this.setOriginalEvents();
//     this.calculateChartData();
//
//     this.isLoaded = true;
//   });
// }
//# sourceMappingURL=month-deals.component.js.map