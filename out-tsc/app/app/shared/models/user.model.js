var User = /** @class */ (function () {
    function User(email, password, name, id) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.id = id;
    }
    return User;
}());
export { User };
//# sourceMappingURL=user.model.js.map