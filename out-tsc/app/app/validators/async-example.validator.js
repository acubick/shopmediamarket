export function asyncExample(control) {
    // console.log('HI SERVER!');
    var promise = new Promise(function (resolve) {
        setTimeout(function () {
            if (control.value === 'yourName') {
                resolve({
                    asyncExample: true,
                });
            }
            else {
                resolve(null);
            }
        }, 2000);
    });
    return promise;
}
//# sourceMappingURL=async-example.validator.js.map