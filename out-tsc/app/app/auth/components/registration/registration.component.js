var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MIN_LENGTH_PASSWORD } from '../../../shared/constants';
import { asyncExample } from '../../../validators/async-example.validator';
import { onlyLetters } from '../../../validators/only-letters.validators';
var RegistrationComponent = /** @class */ (function () {
    function RegistrationComponent(formBuilder) {
        this.formBuilder = formBuilder;
        this.buildForm();
    }
    RegistrationComponent.prototype.ngOnInit = function () {
    };
    RegistrationComponent.prototype.onSubmit = function () {
        console.log(this.registrationForm);
    };
    Object.defineProperty(RegistrationComponent.prototype, "username", {
        get: function () {
            console.log(this.registrationForm.controls.username);
            return this.registrationForm.controls.username;
        },
        enumerable: true,
        configurable: true
    });
    RegistrationComponent.prototype.buildForm = function () {
        this.registrationForm = this.formBuilder.group({
            username: ['yourName', [Validators.required, onlyLetters], asyncExample],
            email: ['your.email@gmail.com',
                {
                    updateOn: 'blur',
                    validators: [Validators.required, Validators.email],
                    asyncValidators: null,
                },
            ],
            password: ['', [Validators.required, Validators.minLength(MIN_LENGTH_PASSWORD)]],
            agree: ['', Validators.requiredTrue],
        });
        // console.table(this.registrationForm);
    };
    RegistrationComponent = __decorate([
        Component({
            selector: 'app-registration',
            templateUrl: './registration.component.html',
            styleUrls: ['./registration.component.scss'],
        }),
        __metadata("design:paramtypes", [FormBuilder])
    ], RegistrationComponent);
    return RegistrationComponent;
}());
export { RegistrationComponent };
//# sourceMappingURL=registration.component.js.map