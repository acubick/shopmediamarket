var AuthService = /** @class */ (function () {
    function AuthService() {
    }
    AuthService.prototype.isLoggedIn = function () {
        return this.isAuthentificated;
    };
    AuthService.prototype.login = function () {
        this.isAuthentificated = true;
    };
    AuthService.prototype.logout = function () {
        this.isAuthentificated = false;
        window.localStorage.clear();
    };
    return AuthService;
}());
export { AuthService };
//# sourceMappingURL=auth.service.js.map